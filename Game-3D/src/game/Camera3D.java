package game;

import engine.basics.EngineCore;
import engine.basics.ICamera;
import engine.devices.DeviceMouse;
import engine.space.Frustum;
import engine.toolbox.Maths;
import engine.toolbox.MousePicker;
import engine.toolbox.vector.Matrix4f;
import engine.toolbox.vector.Vector3f;
import engine.visual.fxDrivers.SmoothFloat;

/**
 * Represents the in-game camera and handles all of the camera movement and controls.
 */
public class Camera3D implements ICamera {
	private static final float ZOOM_AGILITY = 8f;
	private static final float ROTATE_AGILITY = 6f;
	private static final float PITCH_AGILITY = 8f;
	private static final float FIELD_OF_VIEW = 70f;
	private static final float NEAR_PLANE = 0.1f;
	private static final float FAR_PLANE = 3200f;
	private final static float CAMERA_AIM_OFFSET = 10.0f;
	private final static float INFLUENCE_OF_MOUSEDY = -7200.0f;
	private final static float INFLUENCE_OF_MOUSEDX = INFLUENCE_OF_MOUSEDY * 92.0f;
	private final static float INFLUENCE_OF_MOUSE_WHEEL = 12.5f;
	private final static float MAX_ANGLE_OF_ELEVATION = 1.5f;
	private final static float PITCH_OFFSET = 3f;
	private final static float MINIMUM_ZOOM = 8;
	private final static float MAXIMUM_ZOOM = 300;
	private final static float MAX_HORIZONTAL_CHANGE = 500;
	private final static float MAX_VERTICAL_CHANGE = 5;
	private static final float NORMAL_ZOOM = 32f;

	public static int toggleMouseMoveKey;
	private Matrix4f viewMatrix;
	private Vector3f position;

	private float horizontalDistanceFromFocus;
	private float verticalDistanceFromFocus;
	private float pitch;
	private float yaw;

	private SmoothFloat aimDistance;
	private MousePicker cameraAimer;

	private Vector3f targetPosition;
	private Vector3f targetRotation;
	private float actualDistanceFromPoint;
	private float targetZoom = actualDistanceFromPoint;
	private float targetElevation;
	private float angleOfElevation;
	private float targetRotationAngle;
	private float angleAroundPlayer;

	public Camera3D() {
		toggleMouseMoveKey = DeviceMouse.MOUSE_BUTTON_RIGHT;
		viewMatrix = new Matrix4f();
		position = new Vector3f();
		targetPosition = new Vector3f(0, 0, 0);
		targetRotation = new Vector3f(0, 0, 0);
		actualDistanceFromPoint = NORMAL_ZOOM;
		targetZoom = actualDistanceFromPoint;
		targetElevation = 0.37f;
		angleOfElevation = targetElevation;
		targetRotationAngle = 0;
		angleAroundPlayer = targetRotationAngle;

		aimDistance = new SmoothFloat(1400.0f, 2.0f);
		cameraAimer = new MousePicker(this, true);

		calculateDistances();
	}

	private static void createViewMatrix(Matrix4f viewMatrix, Vector3f position, float pitch, float yaw) {
		viewMatrix.setIdentity();
		Vector3f cameraPos = new Vector3f(-position.x, -position.y, -position.z);
		Matrix4f.rotate(viewMatrix, new Vector3f(1, 0, 0), Maths.degreesToRadians(pitch), viewMatrix);
		Matrix4f.rotate(viewMatrix, new Vector3f(0, 1, 0), Maths.degreesToRadians(-yaw), viewMatrix);
		Matrix4f.translate(viewMatrix, cameraPos, viewMatrix);
	}

	private void calculateDistances() {
		horizontalDistanceFromFocus = (float) (actualDistanceFromPoint * Math.cos(angleOfElevation));
		verticalDistanceFromFocus = (float) (actualDistanceFromPoint * Math.sin(angleOfElevation));
	}

	@Override
	public float getNearPlane() {
		return NEAR_PLANE;
	}

	@Override
	public float getFarPlane() {
		return FAR_PLANE;
	}

	@Override
	public float getFOV() {
		return FIELD_OF_VIEW;
	}

	@Override
	public void moveCamera(Vector3f focusPosition, Vector3f focusRotation, boolean gamePaused) {
		calculateHorizontalAngle(gamePaused);
		calculateVerticalAngle(gamePaused);
		calculateZoom(gamePaused);

		targetPosition = focusPosition;
		targetRotation = focusRotation;

		updateActualZoom();
		updateHorizontalAngle();
		updatePitchAngle();
		calculateDistances();
		calculatePosition();
		createViewMatrix(viewMatrix, position, pitch, yaw);

		cameraAimer.update();
		updateAimDistance();
	}

	private void calculateHorizontalAngle(boolean gamePaused) {
		float delta = EngineCore.getDeltaSeconds();
		float angleChange = 0; // Maths.normalizeAngle(yaw) - Maths.normalizeAngle(entity.getRotation().getY())

		if (DeviceMouse.getMouse(toggleMouseMoveKey) && !gamePaused) { // && !DeviceInput.isActiveInGUI()
			angleChange = (float) DeviceMouse.getMouseDeltaX() * INFLUENCE_OF_MOUSEDX;
		}

		if (angleChange > MAX_HORIZONTAL_CHANGE * delta) {
			angleChange = MAX_HORIZONTAL_CHANGE * delta;
		} else if (angleChange < -MAX_HORIZONTAL_CHANGE * delta) {
			angleChange = -MAX_HORIZONTAL_CHANGE * delta;
		}

		targetRotationAngle -= angleChange;

		if (targetRotationAngle >= Maths.DEGREES_IN_HALF_CIRCLE) {
			targetRotationAngle -= Maths.DEGREES_IN_CIRCLE;
		} else if (targetRotationAngle <= -Maths.DEGREES_IN_HALF_CIRCLE) {
			targetRotationAngle += Maths.DEGREES_IN_CIRCLE;
		}
	}

	private void calculateVerticalAngle(boolean gamePaused) {
		float delta = EngineCore.getDeltaSeconds();
		float angleChange = 0;

		if (DeviceMouse.getMouse(toggleMouseMoveKey) && !gamePaused) { // && !DeviceMouse.isActiveInGUI()
			angleChange = -(float) DeviceMouse.getMouseDeltaY() * INFLUENCE_OF_MOUSEDY;
		}

		if (angleChange > MAX_VERTICAL_CHANGE * delta) {
			angleChange = MAX_VERTICAL_CHANGE * delta;
		} else if (angleChange < -MAX_VERTICAL_CHANGE * delta) {
			angleChange = -MAX_VERTICAL_CHANGE * delta;
		}

		targetElevation -= angleChange;

		//if (targetElevation >= MAX_ANGLE_OF_ELEVATION) {
		//	targetElevation = MAX_ANGLE_OF_ELEVATION;
		//} else if (targetElevation <= 0) {
		//	targetElevation = 0;
		//}
	}

	private void calculateZoom(boolean gamePaused) {
		float zoomLevel = gamePaused ? 0 : (float) (DeviceMouse.getMouseDeltaWheel() / INFLUENCE_OF_MOUSE_WHEEL);
		// float zoomLevel = 0;

		if (zoomLevel != 0) {
			targetZoom -= zoomLevel;

			//if (targetZoom < MINIMUM_ZOOM) {
			//	targetZoom = MINIMUM_ZOOM;
			//} else if (targetZoom > MAXIMUM_ZOOM) {
			//	targetZoom = MAXIMUM_ZOOM;
			//}
		}
	}

	private void updateActualZoom() {
		float offset = targetZoom - actualDistanceFromPoint;
		float change = offset * EngineCore.getDeltaSeconds() * ZOOM_AGILITY;
		actualDistanceFromPoint += change;
	}

	private void updateHorizontalAngle() {
		float offset = targetRotationAngle - angleAroundPlayer;

		if (Math.abs(offset) > Maths.DEGREES_IN_HALF_CIRCLE) {
			if (offset < 0) {
				offset = targetRotationAngle + Maths.DEGREES_IN_CIRCLE - angleAroundPlayer;
			} else {
				offset = targetRotationAngle - Maths.DEGREES_IN_CIRCLE - angleAroundPlayer;
			}
		}

		float change = offset * EngineCore.getDeltaSeconds() * ROTATE_AGILITY;
		angleAroundPlayer += change;

		if (angleAroundPlayer >= Maths.DEGREES_IN_HALF_CIRCLE) {
			angleAroundPlayer -= Maths.DEGREES_IN_CIRCLE;
		} else if (angleAroundPlayer <= -Maths.DEGREES_IN_HALF_CIRCLE) {
			angleAroundPlayer += Maths.DEGREES_IN_CIRCLE;
		}
	}

	private void updatePitchAngle() {
		float offset = targetElevation - angleOfElevation;
		float change = offset * EngineCore.getDeltaSeconds() * PITCH_AGILITY;
		angleOfElevation += change;
	}

	private void calculatePosition() {
		float theta = targetRotation.y + angleAroundPlayer;
		position.x = targetPosition.x - (float) (horizontalDistanceFromFocus * Math.sin(Math.toRadians(theta)));
		position.z = targetPosition.z - (float) (horizontalDistanceFromFocus * Math.cos(Math.toRadians(theta)));
		position.y = targetPosition.y + (verticalDistanceFromFocus + CAMERA_AIM_OFFSET);

		yaw = targetRotation.y + Maths.DEGREES_IN_HALF_CIRCLE + angleAroundPlayer;
		pitch = (float) Math.toDegrees(angleOfElevation) - PITCH_OFFSET;
	}

	private void updateAimDistance() {
		Vector3f aimPoint = cameraAimer.getCurrentTerrainPoint();

		if (aimPoint != null) {
			aimDistance.set(Vector3f.subtract(aimPoint, position, null).length());
		} else {
			aimDistance.set(actualDistanceFromPoint);
		}

		aimDistance.update();
	}

	@Override
	public Matrix4f getViewMatrix() {
		return viewMatrix;
	}

	@Override
	public Frustum getViewFrustum() {
		return Frustum.getFrustum(EngineCore.getModule().getRendererMaster().getProjectionMatrix(), viewMatrix);
	}

	@Override
	public Matrix4f getReflectionViewMatrix(float planeHeight) {
		Matrix4f reflectionMatrix = new Matrix4f();
		Vector3f camPosition = new Vector3f(position);
		camPosition.y -= 2 * (position.y - planeHeight);
		float reflectedPitch = -pitch;
		createViewMatrix(reflectionMatrix, camPosition, reflectedPitch, yaw);
		return reflectionMatrix;
	}

	@Override
	public void reflect(float waterHeight) {
		position.y -= 2 * (position.y - waterHeight);
		pitch = -pitch;
		createViewMatrix(viewMatrix, position, pitch, yaw);
	}

	@Override
	public Vector3f getPosition() {
		return position;
	}

	@Override
	public float getPitch() {
		return pitch;
	}

	@Override
	public float getYaw() {
		return yaw;
	}

	@Override
	public float getAimDistance() {
		return aimDistance.get();
	}
}
