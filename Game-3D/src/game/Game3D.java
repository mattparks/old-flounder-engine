package game;

import engine.EngineNetwork;
import engine.basics.IGame;
import engine.basics.IModule;
import engine.basics.IRendererMaster;
import engine.basics.MasterRenderer3D;
import engine.basics.options.OptionsGraphics;
import engine.devices.DeviceDisplay;
import engine.devices.DeviceKeyboard;
import engine.entities.Entity;
import engine.inputs.ControllerKey;
import engine.inputs.KeyButton;
import engine.terrains.Terrain;
import engine.terrains.TerrainTexturePack;
import engine.textures.Texture;
import engine.toolbox.Colour;
import engine.toolbox.Logger;
import engine.toolbox.OpenglUtils;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Vector3f;
import engine.waters.Water;
import engine.world.Environment;
import engine.world.Fog;
import engine.world.World;
import game.entities.EntityBarrel;
import game.entities.EntityBoulder;
import game.entities.EntityFern;
import game.entities.EntityLamp;
import game.entities.EntityPalm;
import game.entities.EntityPerson;
import game.entities.EntityPine;
import game.entities.EntitySun;

import java.io.IOException;
import java.util.Random;

public class Game3D extends IGame {
	public static final boolean SAVE_LOAD_ENTITY_FILE = false;
	public static EngineNetwork network;
	private static Camera3D camera;
	private static IRendererMaster rendererMaster;
	private static IModule module;
	private Entity player;

	public Game3D() {
		super(null, module);
	}

	public static void main(String[] args) {
		camera = new Camera3D();
		rendererMaster = new MasterRenderer3D();
		network = new EngineNetwork();
		module = new IModule(rendererMaster, camera);
		new Game3D(); // Creates the game!
	}

	private static void generateWorld(boolean addEntities) throws IOException {
		TerrainTexturePack terrainTexturePack = new TerrainTexturePack(Texture.newTexture(new MyFile(Terrain.TERRAINS_LOC, "grass.png")).create(), Texture.newTexture(new MyFile(Terrain.TERRAINS_LOC, "mud.png")).create(), Texture.newTexture(new MyFile(Terrain.TERRAINS_LOC, "flowers.png")).create(), Texture.newTexture(new MyFile(Terrain.TERRAINS_LOC, "path.png")).create());
		World.getTerrainTree().add(new Terrain(0, 0, terrainTexturePack));
		World.getWaterTree().add(new Water(World.getTerrainTree().get(0).getPosition().getX() + World.TERRAIN_SIZE / 2, World.getTerrainTree().get(0).getPosition().getZ() + World.TERRAIN_SIZE / 2));

		if (!addEntities) {
			EntityBarrel.createEntity(World.getTerrainTree().get(0).getEntityQuadtree(), new Vector3f(), new Vector3f());
			EntityBoulder.createEntity(World.getTerrainTree().get(0).getEntityQuadtree(), new Vector3f(), new Vector3f(), 1);
			EntityFern.createEntity(World.getTerrainTree().get(0).getEntityQuadtree(), new Vector3f(), new Vector3f(), 1);
			EntityLamp.createEntity(World.getTerrainTree().get(0).getEntityQuadtree(), new Vector3f(), new Vector3f());
			EntityPalm.createEntity(World.getTerrainTree().get(0).getEntityQuadtree(), new Vector3f(), new Vector3f(), 1);
			EntityPerson.createEntity(World.getTerrainTree().get(0).getEntityQuadtree(), new Vector3f(), new Vector3f());
			EntityPine.createEntity(World.getTerrainTree().get(0).getEntityQuadtree(), new Vector3f(), new Vector3f(), 1);
			EntitySun.createEntity(World.getTerrainTree().get(0).getEntityQuadtree(), new Vector3f());
			return;
		}

		for (int j = 0; j < World.getTerrainTree().getSize(); j++) { // TODO TEMP ENTITY GENERATION!
			Random random = new Random(5775); // TODO: Generate with noise!
			Terrain t = World.getTerrainTree().get(j);

			for (int i = 0; i < 1000; i++) {
				float x = random.nextFloat() * World.TERRAIN_SIZE + t.getPosition().getX();
				float z = random.nextFloat() * World.TERRAIN_SIZE + t.getPosition().getZ();
				float y = World.getTerrainHeight(x, z);

				if (i % 11 == 0) {
					if (y > World.getWaterHeight(x, z)) {
						EntityBoulder.createEntity(t.getEntityQuadtree(), new Vector3f(x, y, z), new Vector3f(random.nextFloat() * 360, random.nextFloat() * 360, random.nextFloat() * 360), random.nextFloat() * 1.0f + 0.5f);
					}
				}

				if (i % 4 == 0) {
					if (y > World.getWaterHeight(x, z)) {
						EntityPine.createEntity(t.getEntityQuadtree(), new Vector3f(x, y, z), new Vector3f(0, random.nextFloat() * 360, 0), random.nextFloat() * 0.6f + 0.8f);
					}
				}

				if (i % 7 == 0) {
					if (y > World.getWaterHeight(x, z)) {
						EntityLamp.createEntity(t.getEntityQuadtree(), new Vector3f(x, y, z), new Vector3f(0, random.nextFloat() * 360, 0));
					}
				}

				if (i % 6 == 0) {
					if (y > World.getWaterHeight(x, z)) {
						EntityFern.createEntity(t.getEntityQuadtree(), new Vector3f(x, y, z), new Vector3f(0, random.nextFloat() * 360, 0), random.nextInt(3));
					}
				}

				// if (i % 12 == 0) {
				// if (y > Water.getWaterHeight(x, z, t.getWaterQuadtree())) {
				// EntityLamp.createEntity(t.getEntityQuadtree(), new Vector3f(x, y, z), new Vector3f(0, random.nextFloat() * 360, 0));
				// }
				// }
			}
		}
	}

	@Override
	public void init() throws IOException {
		GameKeyManager.init();
		GameGuis.init();

		/* SOUNDS & MUSIC. */
		//Sound music = Sound.loadSoundNow(new MyFile(DeviceSound.SOUND_FOLDER, "song.wav"), 0.2f);
		//Sound tweeting = Sound.loadSoundNow(new MyFile(DeviceSound.SOUND_FOLDER, "birds.wav"), 0.5f);
		//Playlist playlist = new Playlist();
		//playlist.addMusic(music);
		//ArrayList<Sound> ambientSounds = new ArrayList<>();
		//ambientSounds.add(tweeting);
		//AmbientNode node = new AmbientNode(new Vector3f(1400.0f, 0.0f, -1400.0f), 1000.0f, 1400.0f, ambientSounds);
		//MusicPlayer musicPlayer = DeviceSound.getMusicPlayer();
		//musicPlayer.playMusicPlaylist(playlist, true);

		/* GAME OBJECTS. */
		player = EntityPerson.createEntity(Environment.setGenericEntityTree(), new Vector3f((World.TERRAIN_SIZE / 2), 0, (World.TERRAIN_SIZE / 2)), new Vector3f(0, 0, 0)); // , "Player" + (int) (Math.random() * 10000)

		Environment.init(new Fog(new Colour(0.15f, 0.16f, 0.18f), 0.001f, 2.0f, 0.0f, 500.0f), EntitySun.createEntity(Environment.setGenericEntityTree(), new Vector3f(10000, 10000, -10000)));

		/* PARTICLE SYSTEMS. */
		//Texture particleSystemTexture = Texture.newTexture(new MyFile(ParticleRenderer.PARTICLES_LOC, "coins.png")).create();
		//particleSystemTexture.setNumberOfRows(3);
		//SystemPlayer particleSystem = new SystemPlayer(particleSystemTexture, 40, 10, 0.1f, 1, 1.6f);
		//particleSystem.setSpeedError(0.25f);
		//particleSystem.randomizeRotation();
		//particleSystem.setDirection(new Vector3f(0, 1, 0), 0.5f);
		//particleSystem.setSystemCenter(tempMove.position);
		// particleSystem.setSystemCenter(player.getPosition());
		// Environment.getParticleSystems().add(particleSystem);

		/* WORLD GENERATION. */
		try {
			generateWorld(true);
		} catch (IOException e) {
			Logger.error("World could not be generated!");
			e.printStackTrace();
		}

		network.startServer();
		network.startClient();
	}

	@Override
	public void update() {
		Environment.update();

		//if (!GameGuis.isMenuOpen()) {
		//	node.update();
		//}

		GameKeyManager.update();
		GameGuis.update();

		super.updateGame(player.getPosition(), player.getRotation(), GameGuis.isMenuOpen(), GameGuis.getBlurFactor());
	}

	@Override
	public void cleanUp() {
		network.cleanUp();
	}

	private static class GameKeyManager {
		private static ControllerKey screenshotKey;
		private static ControllerKey fullscreenKey;
		private static ControllerKey polygonKey;

		private static boolean polygonMode;

		public static void init() {
			screenshotKey = new ControllerKey(new KeyButton(DeviceKeyboard.KEY_F2));
			fullscreenKey = new ControllerKey(new KeyButton(DeviceKeyboard.KEY_F11));
			polygonKey = new ControllerKey(new KeyButton(DeviceKeyboard.KEY_P));
			polygonMode = false;
		}

		public static void update() {
			if (screenshotKey.wasPressed()) {
				DeviceDisplay.screenshot();
			}

			if (fullscreenKey.wasPressed()) {
				OptionsGraphics.FULLSCREEN = !OptionsGraphics.FULLSCREEN;
			}

			if (polygonKey.wasPressed()) {
				polygonMode = !polygonMode;

				if (polygonMode) {
					OpenglUtils.goWireframe(true);
				} else {
					OpenglUtils.goWireframe(false);
				}
			}
		}
	}
}
