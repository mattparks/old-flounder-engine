package game.uis;

import engine.fonts.Text;
import engine.guis.GuiComponent;
import engine.guis.GuiTextButton;
import engine.guis.GuiTexture;
import engine.guis.Listener;

import java.util.List;

public class ResetScreen extends GuiComponent {
	private static final float FONT_SIZE = 2;
	private static final float BUTTONS_X_POS = 0.3f;
	private static final float BUTTONS_Y_SIZE = 0.2f;
	private static final float BUTTONS_X_WIDTH = 1f - BUTTONS_X_POS * 2f;

	private GameMenu gameMenu;

	protected ResetScreen(GameMenu menu) {
		gameMenu = menu;
		createResetOption(0.3f);
		createBackOption(1.0f);
	}

	private void createResetOption(float yPos) {
		final Text text = Text.newText("Reset").center().setFontSize(FONT_SIZE * 1.5f).create();
		text.setColour(GameMenu.TEXT_COLOUR);
		super.addText(text, 0, yPos, 1);
	}

	private void createBackOption(float yPos) {
		final Text text = Text.newText("Back").center().setFontSize(FONT_SIZE).create();
		text.setColour(GameMenu.TEXT_COLOUR);
		GuiTextButton button = new GuiTextButton(text);

		Listener listener = gameMenu::closeSecondaryScreen;

		button.addLeftListener(listener);
		addComponent(button, BUTTONS_X_POS, yPos, BUTTONS_X_WIDTH, BUTTONS_Y_SIZE);
	}

	@Override
	protected void updateSelf() {
	}

	@Override
	protected void getGuiTextures(List<GuiTexture> guiTextures) {
	}
}
