package game.uis;

import engine.fonts.Text;
import engine.guis.GuiComponent;
import engine.guis.GuiTextButton;
import engine.guis.GuiTexture;
import engine.guis.Listener;
import engine.profiling.EngineProfiler;

import java.util.List;

public class OptionScreenDeveloper extends GuiComponent {
	private GameMenu gameMenu;

	protected OptionScreenDeveloper(GameMenu menu) {
		gameMenu = menu;

		createProfilerToggleOption(OptionScreen.BUTTONS_X_CENTER_POS, 0.0f);

		createBackOption(OptionScreen.BUTTONS_X_CENTER_POS, 0.9f);
	}

	private void createProfilerToggleOption(float xPos, float yPos) {
		final String profilerText = "Engine Profiler: ";
		final Text text = Text.newText(profilerText + (EngineProfiler.isOpen() ? "Enabled" : "Disabled")).center().setFontSize(OptionScreen.FONT_SIZE).create();
		text.setColour(GameMenu.TEXT_COLOUR);
		GuiTextButton button = new GuiTextButton(text);

		Listener listener = () -> {
			EngineProfiler.toggle(!EngineProfiler.isOpen());

			text.setText(profilerText + (EngineProfiler.isOpen() ? "Enabled" : "Disabled"));
		};

		button.addLeftListener(listener);
		addComponent(button, xPos, yPos, OptionScreen.BUTTONS_X_WIDTH, OptionScreen.BUTTONS_Y_SIZE);
	}

	private void createBackOption(float xPos, float yPos) {
		final Text text = Text.newText("Back").center().setFontSize(OptionScreen.FONT_SIZE).create();
		text.setColour(GameMenu.TEXT_COLOUR);
		GuiTextButton button = new GuiTextButton(text);

		Listener listener = () -> gameMenu.setNewSecondaryScreen(new OptionScreen(gameMenu));

		button.addLeftListener(listener);
		addComponent(button, xPos, yPos, OptionScreen.BUTTONS_X_WIDTH, OptionScreen.BUTTONS_Y_SIZE);
	}

	@Override
	protected void updateSelf() {
	}

	@Override
	protected void getGuiTextures(List<GuiTexture> guiTextures) {
	}
}
