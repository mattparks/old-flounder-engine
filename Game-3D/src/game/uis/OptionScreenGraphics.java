package game.uis;

import engine.basics.options.OptionsGraphics;
import engine.fonts.Text;
import engine.guis.GuiComponent;
import engine.guis.GuiTextButton;
import engine.guis.GuiTexture;
import engine.guis.Listener;

import java.util.List;

public class OptionScreenGraphics extends GuiComponent {
	private GameMenu gameMenu;

	protected OptionScreenGraphics(GameMenu menu) {
		gameMenu = menu;

		createFullscreenOption(OptionScreen.BUTTONS_X_LEFT_POS, 0.0f);
		createAntialiasOption(OptionScreen.BUTTONS_X_LEFT_POS, 0.2f);
		createVSyncOption(OptionScreen.BUTTONS_X_LEFT_POS, 0.4f);

		createShadowsOption(OptionScreen.BUTTONS_X_RIGHT_POS, 0.0f);
		createPostOption(OptionScreen.BUTTONS_X_RIGHT_POS, 0.2f);
		// createPixelRatioOption(OptionScreen.BUTTONS_X_RIGHT_POS, 0.5f);

		createBackOption(OptionScreen.BUTTONS_X_CENTER_POS, 0.9f);
	}

	private void createFullscreenOption(float xPos, float yPos) {
		final String fullscreenText = "Fullscreen: ";
		final Text text = Text.newText(fullscreenText + (OptionsGraphics.FULLSCREEN ? "On" : "Off")).center().setFontSize(OptionScreen.FONT_SIZE).create();
		text.setColour(GameMenu.TEXT_COLOUR);
		GuiTextButton button = new GuiTextButton(text);

		Listener listener = () -> {
			OptionsGraphics.FULLSCREEN = !OptionsGraphics.FULLSCREEN;
			text.setText(fullscreenText + (OptionsGraphics.FULLSCREEN ? "On" : "Off"));
		};

		button.addLeftListener(listener);
		addComponent(button, xPos, yPos, OptionScreen.BUTTONS_X_WIDTH, OptionScreen.BUTTONS_Y_SIZE);
	}

	private void createAntialiasOption(float xPos, float yPos) {
		final String msaaText = "Antialiasing: ";
		final Text text = Text.newText(msaaText + (OptionsGraphics.ANTI_ALIASING ? "On" : "Off")).center().setFontSize(OptionScreen.FONT_SIZE).create();
		text.setColour(GameMenu.TEXT_COLOUR);
		GuiTextButton button = new GuiTextButton(text);

		Listener listener = () -> {
			OptionsGraphics.ANTI_ALIASING = !OptionsGraphics.ANTI_ALIASING;
			text.setText(msaaText + (OptionsGraphics.ANTI_ALIASING ? "On" : "Off"));
		};

		button.addLeftListener(listener);
		addComponent(button, xPos, yPos, OptionScreen.BUTTONS_X_WIDTH, OptionScreen.BUTTONS_Y_SIZE);
	}

	private void createVSyncOption(float xPos, float yPos) {
		final String shadowText = "VSync: ";
		final Text text = Text.newText(shadowText + (OptionsGraphics.V_SYNC ? "On" : "Off")).center().setFontSize(OptionScreen.FONT_SIZE).create();
		text.setColour(GameMenu.TEXT_COLOUR);
		GuiTextButton button = new GuiTextButton(text);

		Listener listener = () -> {
			OptionsGraphics.V_SYNC = !OptionsGraphics.V_SYNC;
			text.setText(shadowText + (OptionsGraphics.V_SYNC ? "On" : "Off"));
		};

		button.addLeftListener(listener);
		addComponent(button, xPos, yPos, OptionScreen.BUTTONS_X_WIDTH, OptionScreen.BUTTONS_Y_SIZE);
	}

	private void createShadowsOption(float xPos, float yPos) {
		final String shadowText = "Shadows: ";
		final Text text = Text.newText(shadowText + (OptionsGraphics.SHADOWS ? "On" : "Off")).center().setFontSize(OptionScreen.FONT_SIZE).create();
		text.setColour(GameMenu.TEXT_COLOUR);
		GuiTextButton button = new GuiTextButton(text);

		Listener listener = () -> {
			OptionsGraphics.SHADOWS = !OptionsGraphics.SHADOWS;
			text.setText(shadowText + (OptionsGraphics.SHADOWS ? "On" : "Off"));
		};

		button.addLeftListener(listener);
		addComponent(button, xPos, yPos, OptionScreen.BUTTONS_X_WIDTH, OptionScreen.BUTTONS_Y_SIZE);
	}

	private void createPostOption(float xPos, float yPos) {
		final String postTextPre = "Post Effect: ";
		final Text text = Text.newText(postTextPre + OptionsGraphics.POST_EFFECT).center().setFontSize(OptionScreen.FONT_SIZE).create();
		text.setColour(GameMenu.TEXT_COLOUR);
		GuiTextButton button = new GuiTextButton(text);

		Listener listenerUp = () -> {
			OptionsGraphics.POST_EFFECT += 1;

			if (OptionsGraphics.POST_EFFECT > OptionsGraphics.POST_EFFECT_MAX) {
				OptionsGraphics.POST_EFFECT = OptionsGraphics.POST_EFFECT_MAX;
			}

			text.setText(postTextPre + OptionsGraphics.POST_EFFECT);
		};

		Listener listenerDown = () -> {
			OptionsGraphics.POST_EFFECT -= 1;

			if (OptionsGraphics.POST_EFFECT < 0) {
				OptionsGraphics.POST_EFFECT = 0;
			}

			text.setText(postTextPre + OptionsGraphics.POST_EFFECT);
		};

		button.addLeftListener(listenerUp);
		button.addRightListener(listenerDown);
		addComponent(button, xPos, yPos, OptionScreen.BUTTONS_X_WIDTH, OptionScreen.BUTTONS_Y_SIZE);
	}

	private void createPixelRatioOption(float xPos, float yPos) {
		final String pixelTextPre = "Pixel Ratio: ";
		final Text text = Text.newText(pixelTextPre + OptionsGraphics.DISPLAY_PIXEL_RATIO).center().setFontSize(OptionScreen.FONT_SIZE).create();
		text.setColour(GameMenu.TEXT_COLOUR);
		GuiTextButton button = new GuiTextButton(text);

		Listener listenerUp = () -> {
			OptionsGraphics.DISPLAY_PIXEL_RATIO += 0.01f;

			// TODO: Pixel -> Screen ratio!

			text.setText(pixelTextPre + OptionsGraphics.DISPLAY_PIXEL_RATIO);
		};

		Listener listenerDown = () -> {
			OptionsGraphics.DISPLAY_PIXEL_RATIO -= 0.01f;

			text.setText(pixelTextPre + OptionsGraphics.DISPLAY_PIXEL_RATIO);
		};

		button.addLeftListener(listenerUp);
		button.addRightListener(listenerDown);
		addComponent(button, xPos, yPos, OptionScreen.BUTTONS_X_WIDTH, OptionScreen.BUTTONS_Y_SIZE);
	}

	private void createBackOption(float xPos, float yPos) {
		final Text text = Text.newText("Back").center().setFontSize(OptionScreen.FONT_SIZE).create();
		text.setColour(GameMenu.TEXT_COLOUR);
		GuiTextButton button = new GuiTextButton(text);

		Listener listener = () -> gameMenu.setNewSecondaryScreen(new OptionScreen(gameMenu));

		button.addLeftListener(listener);
		addComponent(button, xPos, yPos, OptionScreen.BUTTONS_X_WIDTH, OptionScreen.BUTTONS_Y_SIZE);
	}

	@Override
	protected void updateSelf() {
	}

	@Override
	protected void getGuiTextures(List<GuiTexture> guiTextures) {
	}
}
