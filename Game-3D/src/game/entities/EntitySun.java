package game.entities;

import engine.entities.Entity;
import engine.entities.EntityFileLoader;
import engine.entities.EntityFileSaver;
import engine.entities.EntityFileTemplate;
import engine.entities.components.LightComponent;
import engine.entities.components.SunComponent;
import engine.lights.Light;
import engine.space.ISpatialStructure;
import engine.toolbox.Colour;
import engine.toolbox.vector.Vector3f;
import game.Game3D;

import java.io.IOException;

public class EntitySun {
	public static EntityFileTemplate TEMPLATE = null;

	public static Entity createEntity(ISpatialStructure<Entity> structure, Vector3f position) throws IOException {
		//	if (TEMPLATE == null) {
		//		if (Game3D.SAVE_LOAD_ENTITY_FILE || (TEMPLATE = EntityFileLoader.load("sun")) == null) {
		return new EntitySunEntity(structure, position);
		//		}
		//	}

		//	return TEMPLATE.createEntity(structure, position, new Vector3f());
	}

	public static class EntitySunEntity extends Entity {
		public EntitySunEntity(ISpatialStructure<Entity> structure, Vector3f position) throws IOException {
			super(structure, position, new Vector3f(0, 0, 0));
			new LightComponent(this, new Vector3f(0, 0, 0), new Light(new Colour(0.6f, 0.6f, 0.6f), new Vector3f(0, 0, 0))); // At sunrise sunset = 182, 126, 91 :: Noon = 192, 191, 173 :: Clouds / haze = 189, 190, 192 :: Overcast = 174, 183, 190
			new SunComponent(this);

			if (Game3D.SAVE_LOAD_ENTITY_FILE) {
				EntityFileSaver.save(this, "sun");
				TEMPLATE = EntityFileLoader.load("sun");
			}
		}
	}
}
