package game.entities;

import engine.entities.Entity;
import engine.entities.EntityFileLoader;
import engine.entities.EntityFileSaver;
import engine.entities.EntityFileTemplate;
import engine.entities.components.ColliderComponent;
import engine.entities.components.CollisionComponent;
import engine.entities.components.ModelComponent;
import engine.models.LoaderOBJ;
import engine.models.ModelTextured;
import engine.space.ISpatialStructure;
import engine.textures.Texture;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Vector3f;
import game.Game3D;

import java.io.IOException;

public class EntityPine {
	public static EntityFileTemplate TEMPLATE = null;

	public static Entity createEntity(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation, float scale) throws IOException {
		//	if (TEMPLATE == null) {
		//		if (Game3D.SAVE_LOAD_ENTITY_FILE || (TEMPLATE = EntityFileLoader.load("pine")) == null) {
		return new EntityPineEntity(structure, position, rotation, scale);
		//		}
		//	}

		//	return TEMPLATE.createEntity(structure, position, rotation);
	}

	public static class EntityPineEntity extends Entity {
		public EntityPineEntity(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation, float scale) throws IOException {
			super(structure, position, rotation);
			ModelTextured model = new ModelTextured(LoaderOBJ.loadOBJ(new MyFile(MyFile.RES_FOLDER, "pine.obj")), Texture.newTexture(new MyFile(MyFile.RES_FOLDER, "pine.png")).create());
			model.getModelTexture().setHasTransparency(true);
			new ColliderComponent(this);
			new CollisionComponent(this);
			new ModelComponent(this, model, scale);

			if (Game3D.SAVE_LOAD_ENTITY_FILE) {
				EntityFileSaver.save(this, "pine");
				TEMPLATE = EntityFileLoader.load("pine");
			}
		}
	}
}
