package game.entities;

import engine.entities.Entity;
import engine.entities.EntityFileLoader;
import engine.entities.EntityFileSaver;
import engine.entities.EntityFileTemplate;
import engine.entities.components.ColliderComponent;
import engine.entities.components.CollisionComponent;
import engine.entities.components.ModelComponent;
import engine.models.LoaderOBJ;
import engine.models.ModelTextured;
import engine.space.ISpatialStructure;
import engine.textures.Texture;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Vector3f;
import game.Game3D;

import java.io.IOException;

public class EntityBoulder {
	public static EntityFileTemplate TEMPLATE = null;

	public static Entity createEntity(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation, float scale) throws IOException {
		//	if (TEMPLATE == null) {
		//		if (Game3D.SAVE_LOAD_ENTITY_FILE || (TEMPLATE = EntityFileLoader.load("boulder")) == null) {
		return new EntityBoulderEntity(structure, position, rotation, scale);
		//		}
		//	}

		//	return TEMPLATE.createEntity(structure, position, rotation);
	}

	public static class EntityBoulderEntity extends Entity {
		public EntityBoulderEntity(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation, float scale) throws IOException {
			super(structure, position, rotation);
			ModelTextured model = new ModelTextured(LoaderOBJ.loadOBJ(new MyFile(MyFile.RES_FOLDER, "boulder.obj")), Texture.newTexture(new MyFile(MyFile.RES_FOLDER, "boulder.png")).create());
			model.setNormalTexture(Texture.newTexture(new MyFile(MyFile.RES_FOLDER, "boulderNormal.png")).create());
			model.setShineDamper(10.0f);
			model.setReflectivity(0.5f);
			new ColliderComponent(this);
			new CollisionComponent(this);
			new ModelComponent(this, model, scale);

			if (Game3D.SAVE_LOAD_ENTITY_FILE) {
				EntityFileSaver.save(this, "boulder");
				TEMPLATE = EntityFileLoader.load("boulder");
			}
		}
	}
}
