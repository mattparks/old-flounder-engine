package game.entities;

import engine.entities.Entity;
import engine.entities.EntityFileLoader;
import engine.entities.EntityFileSaver;
import engine.entities.EntityFileTemplate;
import engine.entities.components.ColliderComponent;
import engine.entities.components.CollisionComponent;
import engine.entities.components.ModelComponent;
import engine.entities.components.PlayerComponent;
import engine.models.LoaderOBJ;
import engine.models.ModelTextured;
import engine.space.ISpatialStructure;
import engine.textures.Texture;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Vector3f;
import game.Game3D;

import java.io.IOException;

public class EntityPerson {
	public static EntityFileTemplate TEMPLATE = null;

	public static Entity createEntity(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation) throws IOException {
		//	if (Game3D.SAVE_LOAD_ENTITY_FILE && TEMPLATE == null) {
		//		if (Game3D.SAVE_LOAD_ENTITY_FILE || (TEMPLATE = EntityFileLoader.load("person")) == null) {
		return new EntityPlayerEntity(structure, position, rotation);
		//		}
		//	}

		//	return TEMPLATE.createEntity(structure, position, rotation);
	}

	public static class EntityPlayerEntity extends Entity {
		public EntityPlayerEntity(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation) throws IOException {
			super(structure, position, rotation);
			ModelTextured model = new ModelTextured(LoaderOBJ.loadOBJ(new MyFile(MyFile.RES_FOLDER, "person.obj")), Texture.newTexture(new MyFile(MyFile.RES_FOLDER, "person.png")).create());
			model.setShineDamper(10.0f);
			model.setReflectivity(0.2f);
			new ColliderComponent(this);
			new CollisionComponent(this);
			new PlayerComponent(this, "Player666");
			new ModelComponent(this, model, 1.0f);

			if (Game3D.SAVE_LOAD_ENTITY_FILE) {
				EntityFileSaver.save(this, "person");
				TEMPLATE = EntityFileLoader.load("person");
			}
		}
	}
}
