package game.entities;

import engine.entities.Entity;
import engine.entities.EntityFileLoader;
import engine.entities.EntityFileSaver;
import engine.entities.EntityFileTemplate;
import engine.entities.components.ColliderComponent;
import engine.entities.components.CollisionComponent;
import engine.entities.components.ModelComponent;
import engine.models.LoaderOBJ;
import engine.models.ModelTextured;
import engine.space.ISpatialStructure;
import engine.textures.Texture;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Vector3f;
import game.Game3D;

import java.io.IOException;

public class EntityBarrel {
	public static EntityFileTemplate TEMPLATE = null;

	public static Entity createEntity(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation) throws IOException {
		//	if (TEMPLATE == null) {
		//		if ((TEMPLATE = EntityFileLoader.load("barrel")) == null) {
		return new EntityBarrelEntity(structure, position, rotation);
		//		}
		//	}

		//	return TEMPLATE.createEntity(structure, position, rotation);
	}

	public static class EntityBarrelEntity extends Entity {
		public EntityBarrelEntity(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation) throws IOException {
			super(structure, position, rotation);
			ModelTextured model = new ModelTextured(LoaderOBJ.loadOBJ(new MyFile(MyFile.RES_FOLDER, "barrel.obj")), Texture.newTexture(new MyFile(MyFile.RES_FOLDER, "barrel.png")).create());
			model.setNormalTexture(Texture.newTexture(new MyFile(MyFile.RES_FOLDER, "barrelNormal.png")).create());
			model.setShineDamper(10.0f);
			model.setReflectivity(0.5f);
			new ColliderComponent(this);
			new CollisionComponent(this);
			new ModelComponent(this, model, 1.0f);

			if (Game3D.SAVE_LOAD_ENTITY_FILE) {
				EntityFileSaver.save(this, "barrel");
				TEMPLATE = EntityFileLoader.load("barrel");
			}
		}
	}
}
