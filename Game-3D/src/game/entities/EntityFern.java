package game.entities;

import engine.entities.Entity;
import engine.entities.EntityFileLoader;
import engine.entities.EntityFileSaver;
import engine.entities.EntityFileTemplate;
import engine.entities.components.ColliderComponent;
import engine.entities.components.CollisionComponent;
import engine.entities.components.ModelComponent;
import engine.models.LoaderOBJ;
import engine.models.ModelTextured;
import engine.space.ISpatialStructure;
import engine.textures.Texture;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Vector3f;
import game.Game3D;

import java.io.IOException;

public class EntityFern {
	public static EntityFileTemplate TEMPLATE = null;

	public static Entity createEntity(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation, int textureIndex) throws IOException {
		//	if (TEMPLATE == null) {
		//		if (Game3D.SAVE_LOAD_ENTITY_FILE || (TEMPLATE = EntityFileLoader.load("fern")) == null) {
		return new EntityFernEntity(structure, position, rotation);
		//		}
		//	}

		//	Entity entity = TEMPLATE.createEntity(structure, position, rotation);
		//	((ModelComponent) entity.getComponent(ModelComponent.ID)).setTextureIndex(textureIndex);
		//	return entity;
	}

	public static class EntityFernEntity extends Entity {
		public EntityFernEntity(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation) throws IOException {
			super(structure, position, rotation);
			ModelTextured model = new ModelTextured(LoaderOBJ.loadOBJ(new MyFile(MyFile.RES_FOLDER, "fern.obj")), Texture.newTexture(new MyFile(MyFile.RES_FOLDER, "fern.png")).create());
			model.getModelTexture().setHasTransparency(true);
			model.getModelTexture().setNumberOfRows(2);
			new ColliderComponent(this);
			new CollisionComponent(this);
			new ModelComponent(this, model, 0.9f);

			if (Game3D.SAVE_LOAD_ENTITY_FILE) {
				EntityFileSaver.save(this, "fern");
				TEMPLATE = EntityFileLoader.load("fern");
			}
		}
	}
}
