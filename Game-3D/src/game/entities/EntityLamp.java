package game.entities;

import engine.entities.Entity;
import engine.entities.EntityFileLoader;
import engine.entities.EntityFileSaver;
import engine.entities.EntityFileTemplate;
import engine.entities.components.ColliderComponent;
import engine.entities.components.CollisionComponent;
import engine.entities.components.LightComponent;
import engine.entities.components.ModelComponent;
import engine.lights.Attenuation;
import engine.lights.Light;
import engine.models.LoaderOBJ;
import engine.models.ModelTextured;
import engine.space.ISpatialStructure;
import engine.textures.Texture;
import engine.toolbox.Colour;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Vector3f;
import game.Game3D;

import java.io.IOException;

public class EntityLamp {
	public static EntityFileTemplate TEMPLATE = null;

	public static Entity createEntity(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation) throws IOException {
		//	if (TEMPLATE == null) {
		//		if (Game3D.SAVE_LOAD_ENTITY_FILE || (TEMPLATE = EntityFileLoader.load("lamp")) == null) {
		return new EntityLampEntity(structure, position, rotation);
		//		}
		//	}

		//	return TEMPLATE.createEntity(structure, position, rotation);
	}

	public static class EntityLampEntity extends Entity {
		public EntityLampEntity(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation) throws IOException {
			super(structure, position, rotation);
			ModelTextured model = new ModelTextured(LoaderOBJ.loadOBJ(new MyFile(MyFile.RES_FOLDER, "lamp.obj")), Texture.newTexture(new MyFile(MyFile.RES_FOLDER, "lamp.png")).create());
			new ColliderComponent(this);
			new CollisionComponent(this);
			new ModelComponent(this, model, 1.0f);
			new LightComponent(this, new Vector3f(0, 14, 0), new Light(new Colour(1, 1, 1), new Vector3f(0, 0, 0), new Attenuation(1, 0.01f, 0.002f)));

			if (Game3D.SAVE_LOAD_ENTITY_FILE) {
				EntityFileSaver.save(this, "lamp");
				TEMPLATE = EntityFileLoader.load("lamp");
			}
		}
	}
}
