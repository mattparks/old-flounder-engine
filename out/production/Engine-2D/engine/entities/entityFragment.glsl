#version 130

out vec4 out_colour;

varying vec2 pass_textureCoords;

layout(binding = 0) uniform sampler2D texture;

void main() {
    out_colour = texture(texture, pass_textureCoords);
}
