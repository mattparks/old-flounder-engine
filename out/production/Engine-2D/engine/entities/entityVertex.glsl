#version 130

layout(location = 0) in vec2 position;

varying vec2 pass_textureCoords;

uniform mat4 modelMatrix;
uniform mat4 orthographicMatrix;
uniform mat4 viewMatrix;

void main() {
	vec4 worldPosition = modelMatrix * vec4(position, 0.0, 1.0);
	pass_textureCoords = vec2(position.x, 1.0 - position.y);
	gl_Position = orthographicMatrix * viewMatrix * worldPosition;
}
