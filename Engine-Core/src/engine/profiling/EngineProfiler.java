package engine.profiling;

import engine.basics.IRenderer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;

public class EngineProfiler {
	private static JFrame frame;
	private static boolean open;

	public static void init() {
		frame = new JFrame("Engine Profiler");
		frame.setSize(420, 720);
		frame.setResizable(false);
		frame.setLayout(new FlowLayout());
		toggle(false);

		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				toggle(false);
			}
		});
	}

	public static void update(Map<String, IRenderer> renderers) { // TODO
		int yLocation = 320;

		for (Map.Entry<String, IRenderer> entry : renderers.entrySet()) {
			JLabel label = new JLabel();
			label.setText(entry.getKey());
			label.setLocation(210, (yLocation += 50));
			frame.add(label);
		}

		frame.pack();
	}

	public static void toggle(boolean open) {
		frame.setVisible(EngineProfiler.open = open);
	}

	public static boolean isOpen() {
		return open;
	}

	public static void cleanUp() {
		frame.dispose();
	}
}
