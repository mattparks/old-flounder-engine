/**
 * Contains classes for profiling many parts of the engine.
 */
package engine.profiling;