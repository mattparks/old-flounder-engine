package engine.profiling;

import engine.toolbox.Logger;

/**
 * Can be used to record various timings within the engine,
 */
public class ProfileTimer {
	private int invocations;
	private double totalTime;
	private double startTime;

	public ProfileTimer() {
		invocations = 0;
		totalTime = 0;
		startTime = 0;
	}

	/**
	 * Starts a new invocation/
	 */
	public void startInvocation() {
		startTime = System.nanoTime();
	}

	/**
	 * Stops the current Invocation.
	 */
	public void stopInvocation() {
		if (startTime == 0) {
			Logger.error("Stop Invocation called without matching start invocation!");
			assert (startTime != 0); // Stops from running faulty data.
		}

		invocations++;
		totalTime += System.nanoTime() - startTime;
		startTime = 0;
	}

	/**
	 * @return Returns the total time taken in ms, and resets the timer.
	 */
	public double reset() {
		double timeMs = ((totalTime / 1000000.0) / ((double) invocations));
		invocations = 0;
		totalTime = 0;
		startTime = 0;
		return timeMs;
	}
}
