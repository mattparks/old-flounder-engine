package engine.fonts;

import engine.basics.ICamera;
import engine.basics.IRenderer;
import engine.basics.options.OptionsGraphics;
import engine.devices.DeviceDisplay;
import engine.guis.GuiManager;
import engine.toolbox.Colour;
import engine.toolbox.OpenglUtils;
import engine.toolbox.vector.Vector2f;
import engine.toolbox.vector.Vector4f;
import org.lwjgl.opengl.GL11;

import java.util.List;
import java.util.Map;

public class FontRenderer extends IRenderer {
	private FontShader shader;

	public FontRenderer() {
		shader = new FontShader();
	}

	@Override
	public void renderObjects(Vector4f clipPlane, ICamera camera) {
		prepareRendering();
		Map<FontType, List<Text>> texts = GuiManager.getTexts();

		for (FontType font : texts.keySet()) {
			texts.get(font).forEach(this::renderText);
		}

		endRendering();
	}

	private void prepareRendering() {
		OpenglUtils.antialias(OptionsGraphics.ANTI_ALIASING);
		OpenglUtils.cullBackFaces(true);
		OpenglUtils.enableAlphaBlending();
		OpenglUtils.disableDepthTesting();
		shader.start();
		shader.aspectRatio.loadFloat(DeviceDisplay.getAspectRatio());
	}

	private void renderText(Text text) {
		OpenglUtils.bindVAO(text.getMesh(), 0, 1);
		OpenglUtils.bindTextureToBank(text.getFontType().getTextureAtlas(), 0);
		Vector2f textPosition = text.getPosition();
		Colour textColour = text.getColour();
		shader.transform.loadVec3(textPosition.x, textPosition.y, text.getScale());
		shader.colour.loadVec4(textColour.getR(), textColour.getG(), textColour.getB(), text.getTransparency());
		shader.borderColour.loadVec3(text.getBorderColour().toVector());
		shader.borderSizes.loadVec2(new Vector2f(text.getTotalBorderSize(), text.getGlowSize()));
		shader.edgeData.loadVec2(new Vector2f(text.calculateEdgeStart(), text.calculateAntialiasSize()));
		GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, text.getVertexCount());
		OpenglUtils.unbindVAO(0, 1);
	}

	private void endRendering() {
		shader.stop();
	}

	@Override
	public void cleanUp() {
		shader.cleanUp();
	}
}
