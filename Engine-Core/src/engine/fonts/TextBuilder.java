package engine.fonts;

public class TextBuilder {
	private String text;
	private boolean centered;
	private float textSize;
	private FontType font;

	protected TextBuilder(String text) {
		this.text = text;
		centered = false;
		textSize = 1;
		font = FontType.SEGOE_UI;
	}

	public Text create() {
		return new Text(text, font, textSize, centered);
	}

	public TextBuilder center() {
		centered = true;
		return this;
	}

	public TextBuilder setFont(FontType font) {
		this.font = font;
		return this;
	}

	public TextBuilder setFontSize(float size) {
		textSize = size;
		return this;
	}
}
