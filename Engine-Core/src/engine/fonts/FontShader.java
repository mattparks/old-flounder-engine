package engine.fonts;

import engine.shaders.ShaderProgram;
import engine.shaders.UniformFloat;
import engine.shaders.UniformVec2;
import engine.shaders.UniformVec3;
import engine.shaders.UniformVec4;
import engine.toolbox.resources.MyFile;

public class FontShader extends ShaderProgram {
	private static final MyFile VERTEX_SHADER = new MyFile("engine/fonts", "fontVertex.glsl");
	private static final MyFile FRAGMENT_SHADER = new MyFile("engine/fonts", "fontFragment.glsl");

	protected UniformVec3 transform = new UniformVec3("transform");
	protected UniformVec4 colour = new UniformVec4("colour");
	protected UniformVec3 borderColour = new UniformVec3("borderColour");
	protected UniformVec2 borderSizes = new UniformVec2("borderSizes");
	protected UniformVec2 edgeData = new UniformVec2("edgeData");
	protected UniformFloat aspectRatio = new UniformFloat("aspectRatio");

	protected FontShader() {
		super(VERTEX_SHADER, FRAGMENT_SHADER);
		super.storeAllUniformLocations(transform, colour, borderColour, borderSizes, edgeData, aspectRatio);
	}
}
