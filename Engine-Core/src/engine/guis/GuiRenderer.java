package engine.guis;

import engine.basics.ICamera;
import engine.basics.IRenderer;
import engine.loaders.Loader;
import engine.toolbox.OpenglUtils;
import engine.toolbox.vector.Vector4f;
import org.lwjgl.opengl.GL11;

public class GuiRenderer extends IRenderer {
	private static final float[] POSITIONS = {0, 0, 0, 1, 1, 0, 1, 1};

	private GuiShader shader;
	private int vao;

	public GuiRenderer() {
		vao = Loader.createInterleavedVAO(POSITIONS, 2);
		shader = new GuiShader();
	}

	@Override
	public void renderObjects(Vector4f clipPlane, ICamera camera) {
		prepareRendering();

		GuiManager.getGuiTextures().forEach(this::renderGui);

		endRendering();
	}

	private void prepareRendering() {
		OpenglUtils.antialias(false);
		OpenglUtils.cullBackFaces(true);
		OpenglUtils.enableAlphaBlending();
		OpenglUtils.disableDepthTesting();
		shader.start();
	}

	private void renderGui(GuiTexture gui) {
		if (!gui.getTexture().isLoaded()) {
			return;
		}

		OpenglUtils.bindVAO(vao, 0);
		OpenglUtils.bindTextureToBank(gui.getTexture().getTextureID(), 0);
		shader.transform.loadVec4(gui.getPosition().x, gui.getPosition().y, gui.getScale().x, gui.getScale().y);
		shader.alpha.loadFloat(gui.getAlpha());
		shader.flipTexture.loadBoolean(gui.isFlipTexture());
		GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, POSITIONS.length / 2);
		OpenglUtils.unbindVAO(0);
	}

	private void endRendering() {
		shader.stop();
	}

	@Override
	public void cleanUp() {
		shader.cleanUp();
	}
}
