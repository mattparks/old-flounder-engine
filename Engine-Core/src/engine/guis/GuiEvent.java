package engine.guis;

public enum GuiEvent {
	LEFT_CLICK(), RIGHT_CLICK(), MOUSE_OVER()
}
