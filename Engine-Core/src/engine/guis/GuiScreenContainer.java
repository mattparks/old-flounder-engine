package engine.guis;

import java.util.List;

public class GuiScreenContainer extends GuiComponent {
	public GuiScreenContainer() {
		super.setScreenSpacePosition(0, 0, 1, 1);
	}

	@Override
	protected void updateSelf() {
	}

	@Override
	protected void getGuiTextures(List<GuiTexture> guiTextures) {
	}
}
