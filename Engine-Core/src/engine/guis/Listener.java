package engine.guis;

public interface Listener {
	void eventOccurred();
}
