/**
 * Contains classes for various utility features that don't belong anywhere else.
 */
package engine.toolbox;