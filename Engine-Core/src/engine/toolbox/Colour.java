package engine.toolbox;

import engine.toolbox.vector.Vector3f;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

public class Colour {
	private float r, g, b, a;
	private Vector3f colourVectorReusable;

	public Colour() {
		this(0, 0, 0, 1);
	}

	public Colour(Colour source) {
		this(source.getR(), source.getG(), source.getB(), source.getA());
	}

	public Colour(float r, float g, float b) {
		this(r, g, b, 1);
	}

	public Colour(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
		this.colourVectorReusable = new Vector3f();
	}

	public Colour(float r, float g, float b, boolean convert) {
		if (convert) {
			this.r = r / 255f;
			this.g = g / 255f;
			this.b = b / 255f;
			a = 1;
		} else {
			this.r = r;
			this.g = g;
			this.b = b;
			a = 1;
		}

		this.colourVectorReusable = new Vector3f();
	}

	public static Colour add(Colour colour1, Colour colour2, Colour dest) {
		if (dest == null) {
			return new Colour(colour1.r + colour2.r, colour1.g + colour2.g, colour1.b + colour2.b);
		} else {
			dest.r = colour1.r + colour2.r;
			dest.g = colour1.g + colour2.g;
			dest.b = colour1.b + colour2.b;
			return dest;
		}
	}

	public static Colour subtract(Colour colourLeft, Colour colourRight, Colour dest) {
		if (dest == null) {
			return new Colour(colourLeft.r - colourRight.r, colourLeft.g - colourRight.g, colourLeft.b - colourRight.b);
		} else {
			dest.r = colourLeft.r - colourRight.r;
			dest.g = colourLeft.g - colourRight.g;
			dest.b = colourLeft.b - colourRight.b;
			return dest;
		}
	}

	public static Colour multiply(Colour colourLeft, Colour colourRight, Colour dest) {
		if (dest == null) {
			return new Colour(colourLeft.r * colourRight.r, colourLeft.g * colourRight.g, colourLeft.b * colourRight.b);
		} else {
			dest.r = colourLeft.r * colourRight.r;
			dest.g = colourLeft.g * colourRight.g;
			dest.b = colourLeft.b * colourRight.b;
			return dest;
		}
	}

	public static Colour interpolateColours(Colour colour1, Colour colour2, float blend, Colour dest) {
		float colour1Weight = 1 - blend;
		float r = colour1Weight * colour1.r + blend * colour2.r;
		float g = colour1Weight * colour1.g + blend * colour2.g;
		float b = colour1Weight * colour1.b + blend * colour2.b;

		if (dest == null) {
			return new Colour(r, g, b);
		} else {
			dest.setColour(r, g, b);
			return dest;
		}
	}

	public void setColour(float r, float g, float b) {
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public FloatBuffer getAsFloatBuffer() {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(4);
		buffer.put(new float[]{r, g, b, a});
		buffer.flip();
		return buffer;
	}

	/**
	 * @return Returns a equivalent but in Vector3f form.
	 */
	public Vector3f toVector() {
		colourVectorReusable.set(r, g, b);
		return colourVectorReusable;
	}

	@Override
	public String toString() {
		return "(" + r + ", " + g + ", " + b + ")";
	}

	public Colour getUnit() {
		Colour colour = new Colour();

		if (r == 0 && g == 0 && b == 0) {
			return colour;
		}

		colour.setColour(this);
		colour.scale(1f / length());
		return colour;
	}

	public void setColour(Colour colour) {
		this.setColour(colour.getR(), colour.getG(), colour.getB(), colour.getA());
	}

	public Colour scale(float value) {
		r *= value;
		g *= value;
		b *= value;
		return this;
	}

	public float length() {
		return (float) Math.sqrt(lengthSquared());
	}

	public void setColour(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	public float getR() {
		return r;
	}

	public void setR(float r) {
		this.r = r;
	}

	public float getG() {
		return g;
	}

	public void setG(float g) {
		this.g = g;
	}

	public float getB() {
		return b;
	}

	public void setB(float b) {
		this.b = b;
	}

	public float getA() {
		return a;
	}

	public void setA(float a) {
		this.a = a;
	}

	public float lengthSquared() {
		return r * r + g * g + b * b;
	}

	public boolean isEqualTo(Colour colour) {
		return r == colour.getR() && g == colour.getG() && b == colour.getB() && a == colour.getA();
	}
}
