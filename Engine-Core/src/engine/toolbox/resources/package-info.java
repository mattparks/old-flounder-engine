/**
 * Contains classes for representing engine resources such as general files.
 */
package engine.toolbox.resources;