package engine.toolbox;

import engine.toolbox.vector.Matrix4f;
import engine.toolbox.vector.Vector2f;
import engine.toolbox.vector.Vector3f;
import engine.toolbox.vector.Vector4f;

import java.util.Random;

/**
 * A class that holds many various math functions.
 */
public class Maths {
	public static final float DEGREES_IN_CIRCLE = 360;
	public static final float DEGREES_IN_HALF_CIRCLE = 180;
	public static final Random RANDOM = new Random();

	/**
	 * Gets the height on a point off of a 3d triangle.
	 *
	 * @param p1 Point 1 on the triangle.
	 * @param p2 Point 2 on the triangle.
	 * @param p3 Point 3 on the triangle.
	 * @param pos The XZ position of the object.
	 *
	 * @return Height of the triangle at the position.
	 */
	public static float baryCentric(Vector3f p1, Vector3f p2, Vector3f p3, Vector2f pos) {
		float det = (p2.getZ() - p3.getZ()) * (p1.getX() - p3.getX()) + (p3.getX() - p2.getX()) * (p1.getZ() - p3.getZ());
		float l1 = ((p2.getZ() - p3.getZ()) * (pos.getX() - p3.getX()) + (p3.getX() - p2.getX()) * (pos.getY() - p3.getZ())) / det;
		float l2 = ((p3.getZ() - p1.getZ()) * (pos.getX() - p3.getX()) + (p1.getX() - p3.getX()) * (pos.getY() - p3.getZ())) / det;
		float l3 = 1.0f - l1 - l2;

		return l1 * p1.getY() + l2 * p2.getY() + l3 * p3.getY();
	}

	/**
	 * Rotates a vector in a direction by a amount.
	 *
	 * @param direction The direction to rotate by.
	 * @param rotX Rotation in the x axis.
	 * @param rotY Rotation in the y axis.
	 * @param rotZ Rotation in the z axis.
	 *
	 * @return Returns a rotated Vector3f.
	 */
	public static Vector3f rotateVector(Vector3f direction, float rotX, float rotY, float rotZ) {
		Matrix4f matrix = createTransformationMatrix(new Vector3f(0, 0, 0), new Vector3f(rotX, rotY, rotZ), 1);
		Vector4f direction4 = new Vector4f(direction.x, direction.y, direction.z, 1.0f);
		Matrix4f.transform(matrix, direction4, direction4);
		return new Vector3f(direction4.x, direction4.y, direction4.z);
	}

	/**
	 * Creates a new transformation matrix for a object in 2d space.
	 *
	 * @param translation Translation amount the XY.
	 * @param scale How much to scale the matrix.
	 *
	 * @return Returns the transformation matrix.
	 */
	public static Matrix4f createTransformationMatrix(Vector2f translation, float scale) {
		Matrix4f transformationMatrix = new Matrix4f();
		transformationMatrix.setIdentity();
		Matrix4f.translate(transformationMatrix, translation, transformationMatrix);
		Matrix4f.scale(transformationMatrix, new Vector3f(scale, scale, scale), transformationMatrix);
		return transformationMatrix;
	}

	/**
	 * Creates a new transformation matrix for a object in 3d space.
	 *
	 * @param translation Translation amount the XYZ.
	 * @param rotation Rotation amount the XYZ.
	 * @param scale How much to scale the matrix.
	 *
	 * @return Returns the transformation matrix.
	 */
	public static Matrix4f createTransformationMatrix(Vector3f translation, Vector3f rotation, float scale) {
		Matrix4f transformationMatrix = new Matrix4f();
		transformationMatrix.setIdentity();
		Matrix4f.translate(transformationMatrix, translation, transformationMatrix);
		Matrix4f.rotate(transformationMatrix, new Vector3f(1, 0, 0), (float) Math.toRadians(rotation.getX()), transformationMatrix); // Rotate the X component.
		Matrix4f.rotate(transformationMatrix, new Vector3f(0, 1, 0), (float) Math.toRadians(rotation.getY()), transformationMatrix); // Rotate the Y component.
		Matrix4f.rotate(transformationMatrix, new Vector3f(0, 0, 1), (float) Math.toRadians(rotation.getZ()), transformationMatrix); // Rotate the Z component.
		Matrix4f.scale(transformationMatrix, new Vector3f(scale, scale, scale), transformationMatrix);
		return transformationMatrix;
	}

	/**
	 * Interpolates two values by a blendFactor using cos interpolation.
	 *
	 * @param a The first value.
	 * @param b The second value.
	 * @param blend The blend value.
	 *
	 * @return Returns a interpolated value.
	 */
	public static float cosInterpolate(float a, float b, float blend) {
		double ft = blend * Math.PI;
		float f = (float) ((1f - Math.cos(ft)) * 0.5f);
		return a * (1 - f) + b * f;
	}

	/**
	 * Converts degrees to radians.
	 *
	 * @param degrees The value in degrees
	 *
	 * @return Returns a value in radians.
	 */
	public static float degreesToRadians(float degrees) {
		return degrees * (float) (Math.PI / 180d);
	}

	/**
	 * Turns a 4x4 matrix into an array.
	 *
	 * @param matrix The matrix to turn into an array.
	 *
	 * @return A 16 float array.
	 */
	public static float[] matrixToArray(Matrix4f matrix) {
		float[] result = new float[16];
		result[0] = matrix.m00;
		result[1] = matrix.m01;
		result[2] = matrix.m02;
		result[3] = matrix.m03;
		result[4] = matrix.m10;
		result[5] = matrix.m11;
		result[6] = matrix.m12;
		result[7] = matrix.m13;
		result[8] = matrix.m20;
		result[9] = matrix.m21;
		result[10] = matrix.m22;
		result[11] = matrix.m23;
		result[12] = matrix.m30;
		result[13] = matrix.m31;
		result[14] = matrix.m32;
		result[15] = matrix.m33;
		return result;
	}

	/**
	 * A flooring modulus operator. Works similarly to the % operator, except this rounds towards negative infinity rather than towards 0.
	 * <p>
	 * For example: <br>
	 * -7 % 3 = -2; floorMod(-7, 3) = 2 <br>
	 * -6 % 3 = -0; floorMod(-6, 3) = 0 <br>
	 * -5 % 3 = -2; floorMod(-5, 3) = 1 <br>
	 * -4 % 3 = -1; floorMod(-4, 3) = 2 <br>
	 * -3 % 3 = -0; floorMod(-3, 3) = 0 <br>
	 * -2 % 3 = -2; floorMod(-2, 3) = 1 <br>
	 * -1 % 3 = -1; floorMod(-1, 3) = 2 <br>
	 * 0 % 3 = 0; floorMod(0, 3) = 0 <br>
	 * 1 % 3 = 1; floorMod(1, 3) = 1 <br>
	 * 2 % 3 = 2; floorMod(2, 3) = 2 <br>
	 * 3 % 3 = 0; floorMod(3, 3) = 0 <br>
	 * 4 % 3 = 1; floorMod(4, 3) = 1 <br>
	 *
	 * @param numerator The numerator of the modulus operator.
	 * @param denominator The denominator of the modulus operator.
	 *
	 * @return numerator % denominator, rounded towards negative infinity.
	 */
	public static int floorMod(int numerator, int denominator) {
		if (denominator < 0) {
			throw new IllegalArgumentException("FloorMod does not support negative denominators!");
		}

		if (numerator > 0) {
			return numerator % denominator;
		} else {
			int mod = (-numerator) % denominator;

			if (mod != 0) {
				mod = denominator - mod;
			}

			return mod;
		}
	}

	/**
	 * A flooring modulus operator. Works similarly to the % operator, except this rounds towards negative infinity rather than towards 0.
	 * <p>
	 * For example: <br>
	 * -7 % 3 = -2; floorMod(-7, 3) = 2 <br>
	 * -6 % 3 = -0; floorMod(-6, 3) = 0 <br>
	 * -5 % 3 = -2; floorMod(-5, 3) = 1 <br>
	 * -4 % 3 = -1; floorMod(-4, 3) = 2 <br>
	 * -3 % 3 = -0; floorMod(-3, 3) = 0 <br>
	 * -2 % 3 = -2; floorMod(-2, 3) = 1 <br>
	 * -1 % 3 = -1; floorMod(-1, 3) = 2 <br>
	 * 0 % 3 = 0; floorMod(0, 3) = 0 <br>
	 * 1 % 3 = 1; floorMod(1, 3) = 1 <br>
	 * 2 % 3 = 2; floorMod(2, 3) = 2 <br>
	 * 3 % 3 = 0; floorMod(3, 3) = 0 <br>
	 * 4 % 3 = 1; floorMod(4, 3) = 1 <br>
	 *
	 * @param numerator The numerator of the modulus operator.
	 * @param denominator The denominator of the modulus operator.
	 *
	 * @return numerator % denominator, rounded towards negative infinity.
	 */
	public static double floorMod(double numerator, double denominator) {
		if (denominator < 0) {
			throw new IllegalArgumentException("FloorMod does not support negative denominators!");
		}

		if (numerator > 0) {
			return numerator % denominator;
		} else {
			double mod = (-numerator) % denominator;

			if (mod != 0) {
				mod = denominator - mod;
			}

			return mod;
		}
	}

	/**
	 * Normalizes a angle into the range of 0-360.
	 *
	 * @param angle The source angle.
	 *
	 * @return The normalized angle.
	 */
	public static float normalizeAngle(float angle) {
		if (angle >= 360) {
			return angle - 360;
		} else if (angle < 0) {
			return angle + 360;
		}

		return angle;
	}

	/**
	 * Used to floor the value if less than the min.
	 *
	 * @param min The minimum value.
	 * @param value The value.
	 *
	 * @return Returns a value with deadband applied.
	 */
	public static double deadband(double min, double value) {
		return Math.abs(value) >= Math.abs(min) ? value : 0;
	}

	/**
	 * Ensures {@code value} is in the range of {@code min} to {@code max}. If {@code value} is greater than {@code max}, this will return {@code max}. If {@code value} is less than {@code min}, this will return {@code min}. Otherwise, {@code value} is returned unchanged.
	 *
	 * @param value The value to clamp.
	 * @param min The smallest value of the result.
	 * @param max The largest value of the result.
	 *
	 * @return {@code value}, clamped between {@code min} and {@code max}.
	 */
	public static double clamp(double value, double min, double max) {
		if (value < min) {
			value = min;
		} else if (value > max) {
			value = max;
		}

		return value;
	}

	public static double maxValue(double... fs) {
		double max = 0;

		for (double v : fs) {
			if (v > max) {
				max = v;
			}
		}

		return max;
	}

	public static double minValue(double... fs) {
		double min = 0;

		for (double v : fs) {
			if (v < min) {
				min = v;
			}
		}

		return min;
	}

	/**
	 * Limits the value.
	 *
	 * @param value The value.
	 * @param limit The limit.
	 *
	 * @return A limited value.
	 */
	public static float limit(float value, float limit) {
		return value > limit ? limit : value;
	}

	/**
	 * Gets the maximum vector size.
	 *
	 * @param a The first vector to get values from.
	 * @param b The second vector to get values from.
	 *
	 * @return The maximum vector.
	 */
	public static Vector3f max(Vector3f a, Vector3f b) {
		return new Vector3f(Math.max(a.getX(), b.getX()), Math.max(a.getY(), b.getY()), Math.max(a.getZ(), b.getZ()));
	}

	/**
	 * Gets the maximum value in a vector.
	 *
	 * @param vector The value to get the maximum value from.
	 *
	 * @return The maximum value.
	 */
	public static float max(Vector3f vector) {
		return Math.max(vector.getX(), Math.max(vector.getY(), vector.getZ()));
	}

	/**
	 * Gets the lowest vector size.
	 *
	 * @param a The first vector to get values from.
	 * @param b The second vector to get values from.
	 *
	 * @return The lowest vector.
	 */
	public static Vector3f min(Vector3f a, Vector3f b) {
		return new Vector3f(Math.min(a.getX(), b.getX()), Math.min(a.getY(), b.getY()), Math.min(a.getZ(), b.getZ()));
	}

	/**
	 * Gets the lowest value in a vector.
	 *
	 * @param vector The value to get the lowest value from.
	 *
	 * @return The lowest value.
	 */
	public static float min(Vector3f vector) {
		return Math.min(vector.getX(), Math.min(vector.getY(), vector.getZ()));
	}

	/**
	 * Gets the total distance between 2 vectors.
	 *
	 * @param point1 The first point.
	 * @param point2 The second point.
	 *
	 * @return The total distance between the points.
	 */
	public static float getDistance(Vector3f point1, Vector3f point2) {
		return (float) Math.sqrt(squared(point2.getX() - point1.getX()) + squared(point2.getY() - point1.getY()) + squared(point2.getZ() - point1.getZ()));
	}

	/**
	 * Squares a value.
	 *
	 * @param value Value to square.
	 *
	 * @return Returns the squared value.
	 */
	public static float squared(float value) {
		return value * value;
	}

	/**
	 * Gets the distance between two points squared.
	 *
	 * @param point1 The first point.
	 * @param point2 The second point.
	 *
	 * @return The squared distance between the two points.
	 */
	public static float getDistanceSquared(Vector3f point1, Vector3f point2) {
		float dx = point1.getX() - point2.getX();
		float dy = point1.getY() - point2.getY();
		float dz = point1.getZ() - point2.getZ();
		return dx * dx + dy * dy + dz * dz;
	}

	/**
	 * Gets the vector distance between 2 vectors.
	 *
	 * @param point1 The first point.
	 * @param point2 The second point.
	 *
	 * @return The vector distance between the points.
	 */
	public static Vector3f getVectorDistance(Vector3f point1, Vector3f point2) {
		return new Vector3f(squared(point2.getX() - point1.getX()), squared(point2.getY() - point1.getY()), squared(point2.getZ() - point1.getZ()));
	}
}
