/**
 * Contains classes for various vector and matrix functionalities.
 */
package engine.toolbox.vector;