package engine.toolbox.vector;

import java.nio.FloatBuffer;

/**
 * Holds a 3-tuple vector.
 */
public class Vector3f {
	public float x, y, z;

	/**
	 * Constructor for Vector3f.
	 */
	public Vector3f() {
	}

	/**
	 * Constructor for Vector3f.
	 *
	 * @param source Creates this vector out of a existing one.
	 */
	public Vector3f(final Vector3f source) {
		set(source);
	}

	/**
	 * Constructor for Vector3f.
	 *
	 * @param source Creates this vector out of a existing Vector4f.
	 */
	public Vector3f(final Vector4f source) {
		set(new Vector3f(source.x, source.y, source.z));
	}

	/**
	 * Constructor for Vector3f.
	 *
	 * @param x Start x.
	 * @param y Start y.
	 * @param z Start z.
	 */
	public Vector3f(final float x, final float y, final float z) {
		set(x, y, z);
	}

	/**
	 * Adds two vectors together and places the result in the destination vector.
	 *
	 * @param left The left source vector.
	 * @param right The right source vector.
	 * @param destination The destination vector or null if a new vector is to be created.
	 *
	 * @return The destination matrix.
	 */
	public static Vector3f add(final Vector3f left, final Vector3f right, Vector3f destination) {
		if (destination == null) {
			return new Vector3f(left.x + right.x, left.y + right.y, left.z + right.z);
		} else {
			destination.set(left.x + right.x, left.y + right.y, left.z + right.z);
			return destination;
		}
	}

	/**
	 * Subtracts two vectors from each other and places the result in the destination vector.
	 *
	 * @param left The left source vector.
	 * @param right The right source vector.
	 * @param destination The destination vector or null if a new vector is to be created.
	 *
	 * @return The destination matrix.
	 */
	public static Vector3f subtract(final Vector3f left, final Vector3f right, Vector3f destination) {
		if (destination == null) {
			return new Vector3f(left.x - right.x, left.y - right.y, left.z - right.z);
		} else {
			destination.set(left.x - right.x, left.y - right.y, left.z - right.z);
			return destination;
		}
	}

	/**
	 * Calculates the angle between two vectors.
	 *
	 * @param left The left source vector.
	 * @param right The right source vector.
	 *
	 * @return The angle between the two vectors, in radians.
	 */
	public static float angle(final Vector3f left, final Vector3f right) {
		float dls = dot(left, right) / (left.length() * right.length());

		if (dls < -1f) {
			dls = -1f;
		} else if (dls > 1.0f) {
			dls = 1.0f;
		}

		return (float) Math.acos(dls);
	}

	/**
	 * The dot product of two vectors is calculated as v1.x * v2.x + v1.y * v2.y + v1.z * v2.z
	 *
	 * @param left The left source vector.
	 * @param right The right source vector.
	 *
	 * @return Left dot right.
	 */
	public static float dot(final Vector3f left, final Vector3f right) {
		return left.x * right.x + left.y * right.y + left.z * right.z;
	}

	/**
	 * The cross product of two vectors.
	 *
	 * @param left The left source vector.
	 * @param right The right source vector.
	 * @param destination The destination vector or null if a new vector is to be created.
	 *
	 * @return The destination matrix.
	 */
	public static Vector3f cross(final Vector3f left, final Vector3f right, Vector3f destination) {
		if (destination == null) {
			destination = new Vector3f();
		}

		destination.set(left.y * right.z - left.z * right.y, right.x * left.z - right.z * left.x, left.x * right.y - left.y * right.x);
		return destination;
	}

	/**
	 * Loads from another Vector3f.
	 *
	 * @param source The source vector.
	 *
	 * @return this.
	 */
	public Vector3f set(final Vector3f source) {
		x = source.getX();
		y = source.getY();
		z = source.getZ();
		return this;
	}

	/**
	 * @return x.
	 */
	public float getX() {
		return x;
	}

	/**
	 * Set X.
	 *
	 * @param x The new X value.
	 */
	public void setX(final float x) {
		this.x = x;
	}

	/**
	 * @return y.
	 */
	public float getY() {
		return y;
	}

	/**
	 * Set Y.
	 *
	 * @param y The new Y value.
	 */
	public void setY(final float y) {
		this.y = y;
	}

	/**
	 * @return z.
	 */
	public float getZ() {
		return z;
	}

	/**
	 * Set Z.
	 *
	 * @param z The new Z value.
	 */
	public void setZ(final float z) {
		this.z = z;
	}

	/**
	 * Sets values in the vector.
	 *
	 * @param x The new X value.
	 * @param y The new Y value.
	 */
	public void set(final float x, final float y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Sets values in the vector.
	 *
	 * @param x The new X value.
	 * @param y The new Y value.
	 * @param z The new z value.
	 */
	public void set(final float x, final float y, final float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * Translates this vector.
	 *
	 * @param x The translation in x.
	 * @param y the translation in y.
	 * @param z the translation in z.
	 *
	 * @return this.
	 */
	public Vector3f translate(final float x, final float y, final float z) {
		this.x += x;
		this.y += y;
		this.z += z;
		return this;
	}

	/**
	 * Negates this vector and places the result in a destination vector.
	 *
	 * @param destination The destination vector or null if a new vector is to be created.
	 *
	 * @return The destination vector.
	 */
	public Vector3f negate(Vector3f destination) {
		if (destination == null) {
			destination = new Vector3f();
		}

		destination.x = -x;
		destination.y = -y;
		destination.z = -z;
		return destination;
	}

	/**
	 * normalizes this vector and places the result in a destination vector.
	 *
	 * @param destination The destination vector or null if a new vector is to be created.
	 *
	 * @return The destination vector.
	 */
	public Vector3f normalize(Vector3f destination) {
		float l = length();

		if (destination == null) {
			destination = new Vector3f(x / l, y / l, z / l);
		} else {
			destination.set(x / l, y / l, z / l);
		}

		return destination;
	}

	/**
	 * Loads this vector from a FloatBuffer.
	 *
	 * @param buffer The buffer to load it from, at the current position.
	 *
	 * @return this.
	 */
	public Vector3f load(final FloatBuffer buffer) {
		x = buffer.get();
		y = buffer.get();
		z = buffer.get();
		return this;
	}

	/**
	 * Negates this vector.
	 *
	 * @return this.
	 */
	public Vector3f negate() {
		x = -x;
		y = -y;
		z = -z;
		return this;
	}

	/**
	 * @return The length of the vector.
	 */
	public float length() {
		return (float) Math.sqrt(lengthSquared());
	}

	/**
	 * normalizes this vector.
	 *
	 * @return this.
	 */
	public final Vector3f normalize() {
		float len = length();

		if (len != 0.0f) {
			float l = 1.0f / len;
			return scale(l);
		} else {
			throw new IllegalStateException("Zero length vector");
		}
	}

	/**
	 * Scales this vector.
	 *
	 * @param scale The scale factor.
	 *
	 * @return this.
	 */
	public Vector3f scale(final float scale) {
		x *= scale;
		y *= scale;
		z *= scale;
		return this;
	}

	/**
	 * @return The length squared of the vector.
	 */
	public float lengthSquared() {
		return x * x + y * y + z * z;
	}

	/**
	 * Stores this vector in a FloatBuffer.
	 *
	 * @param buffer The buffer to store it in, at the current position.
	 *
	 * @return this.
	 */
	public Vector3f store(FloatBuffer buffer) {
		buffer.put(x);
		buffer.put(y);
		buffer.put(z);
		return this;
	}

	@Override
	public boolean equals(final Object object) {
		if (this == object) {
			return true;
		}

		if (object == null) {
			return false;
		}

		if (getClass() != object.getClass()) {
			return false;
		}

		Vector3f other = (Vector3f) object;

		return x == other.x && y == other.y && z == other.z;
	}

	@Override
	public String toString() {
		return "Vector3f[" + x + ", " + y + ", " + z + "]";
	}
}
