package engine.toolbox.vector;

import java.nio.FloatBuffer;

/**
 * Holds a 2-tuple vector.
 */
public class Vector2f {
	public float x, y;

	/**
	 * Constructor for Vector2f.
	 */
	public Vector2f() {
	}

	/**
	 * Constructor for Vector2f.
	 *
	 * @param source Creates this vector out of a existing one.
	 */
	public Vector2f(final Vector2f source) {
		set(source);
	}

	/**
	 * Constructor for Vector2f.
	 *
	 * @param x Start x.
	 * @param y Start y.
	 */
	public Vector2f(final float x, final float y) {
		set(x, y);
	}

	/**
	 * Adds two vectors together and places the result in the destination vector.
	 *
	 * @param left The left source vector.
	 * @param right The right source vector.
	 * @param destination The destination vector or null if a new vector is to be created.
	 *
	 * @return The destination matrix.
	 */
	public static Vector2f add(final Vector2f left, final Vector2f right, Vector2f destination) {
		if (destination == null) {
			return new Vector2f(left.x + right.x, left.y + right.y);
		} else {
			destination.set(left.x + right.x, left.y + right.y);
			return destination;
		}
	}

	/**
	 * Subtracts two vectors from each other and places the result in the destination vector.
	 *
	 * @param left The left source vector.
	 * @param right The right source vector.
	 * @param destination The destination vector or null if a new vector is to be created.
	 *
	 * @return The destination matrix.
	 */
	public static Vector2f subtract(final Vector2f left, final Vector2f right, Vector2f destination) {
		if (destination == null) {
			return new Vector2f(left.x - right.x, left.y - right.y);
		} else {
			destination.set(left.x - right.x, left.y - right.y);
			return destination;
		}
	}

	/**
	 * Calculates the angle between two vectors.
	 *
	 * @param left The left source vector.
	 * @param right The right source vector.
	 *
	 * @return The angle between the two vectors, in radians.
	 */
	public static float angle(final Vector2f left, final Vector2f right) {
		float dls = dot(left, right) / (left.length() * right.length());

		if (dls < -1f) {
			dls = -1f;
		} else if (dls > 1.0f) {
			dls = 1.0f;
		}

		return (float) Math.acos(dls);
	}

	/**
	 * The dot product of two vectors is calculated as v1.x * v2.x + v1.y * v2.y
	 *
	 * @param left The left source vector.
	 * @param right The right source vector.
	 *
	 * @return Left dot right.
	 */
	public static float dot(final Vector2f left, final Vector2f right) {
		return left.x * right.x + left.y * right.y;
	}

	/**
	 * Loads from another Vector2f.
	 *
	 * @param source The source vector.
	 *
	 * @return this.
	 */
	public Vector2f set(final Vector2f source) {
		x = source.getX();
		y = source.getY();
		return this;
	}

	/**
	 * @return x.
	 */
	public float getX() {
		return x;
	}

	/**
	 * Set X.
	 *
	 * @param x The new X value.
	 */
	public void setX(final float x) {
		this.x = x;
	}

	/**
	 * @return y.
	 */
	public float getY() {
		return y;
	}

	/**
	 * Set Y.
	 *
	 * @param y The new Y value.
	 */
	public void setY(final float y) {
		this.y = y;
	}

	/**
	 * Sets values in the vector.
	 *
	 * @param x The new X value.
	 * @param y The new Y value.
	 */
	public void set(final float x, final float y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Translates this vector.
	 *
	 * @param x The translation in x.
	 * @param y the translation in y.
	 *
	 * @return this.
	 */
	public Vector2f translate(final float x, final float y) {
		this.x += x;
		this.y += y;
		return this;
	}

	/**
	 * Negates this vector and places the result in a destination vector.
	 *
	 * @param destination The destination vector or null if a new vector is to be created.
	 *
	 * @return The destination vector.
	 */
	public Vector2f negate(Vector2f destination) {
		if (destination == null) {
			destination = new Vector2f();
		}

		destination.x = -x;
		destination.y = -y;
		return destination;
	}

	/**
	 * Normalises this vector and places the result in a destination vector.
	 *
	 * @param destination The destination vector or null if a new vector is to be created.
	 *
	 * @return The destination vector.
	 */
	public Vector2f normalize(Vector2f destination) {
		float l = length();

		if (destination == null) {
			destination = new Vector2f(x / l, y / l);
		} else {
			destination.set(x / l, y / l);
		}

		return destination;
	}

	/**
	 * Loads this vector from a FloatBuffer.
	 *
	 * @param buffer The buffer to load it from, at the current position.
	 *
	 * @return this.
	 */
	public Vector2f load(final FloatBuffer buffer) {
		x = buffer.get();
		y = buffer.get();
		return this;
	}

	/**
	 * Negates this vector.
	 *
	 * @return this.
	 */
	public Vector2f negate() {
		x = -x;
		y = -y;
		return this;
	}

	/**
	 * @return The length of the vector.
	 */
	public float length() {
		return (float) Math.sqrt(lengthSquared());
	}

	/**
	 * Normalises this vector.
	 *
	 * @return this.
	 */
	public final Vector2f normalise() {
		float len = length();

		if (len != 0.0f) {
			float l = 1.0f / len;
			return scale(l);
		} else {
			throw new IllegalStateException("Zero length vector");
		}
	}

	/**
	 * Scales this vector.
	 *
	 * @param scale The scale factor.
	 *
	 * @return this.
	 */
	public Vector2f scale(final float scale) {
		x *= scale;
		y *= scale;
		return this;
	}

	/**
	 * @return The length squared of the vector.
	 */
	public float lengthSquared() {
		return x * x + y * y;
	}

	/**
	 * Stores this vector in a FloatBuffer.
	 *
	 * @param buffer The buffer to store it in, at the current position.
	 *
	 * @return this.
	 */
	public Vector2f store(FloatBuffer buffer) {
		buffer.put(x);
		buffer.put(y);
		return this;
	}

	@Override
	public boolean equals(final Object object) {
		if (this == object) {
			return true;
		}

		if (object == null) {
			return false;
		}

		if (getClass() != object.getClass()) {
			return false;
		}

		Vector2f other = (Vector2f) object;

		return x == other.x && y == other.y;
	}

	@Override
	public String toString() {
		return "Vector2f[" + x + ", " + y + "]";
	}
}
