package engine.toolbox.vector;

import java.nio.FloatBuffer;

/**
 * Holds a 4-tuple vector.
 */
public class Vector4f {
	public float x, y, z, w;

	/**
	 * Constructor for Vector4f.
	 */
	public Vector4f() {
	}

	/**
	 * Constructor for Vector4f.
	 *
	 * @param source Creates this vector out of a existing one.
	 */
	public Vector4f(final Vector4f source) {
		set(source);
	}

	/**
	 * Constructor for Vector4f.
	 *
	 * @param x Start x.
	 * @param y Start y.
	 * @param z Start z.
	 * @param w Start w.
	 */
	public Vector4f(final float x, final float y, final float z, final float w) {
		set(x, y, z, w);
	}

	/**
	 * Adds two vectors together and places the result in the destination vector.
	 *
	 * @param left The left source vector.
	 * @param right The right source vector.
	 * @param destination The destination vector or null if a new vector is to be created.
	 *
	 * @return The destination matrix.
	 */
	public static Vector4f add(final Vector4f left, final Vector4f right, Vector4f destination) {
		if (destination == null) {
			return new Vector4f(left.x + right.x, left.y + right.y, left.z + right.z, left.w + right.w);
		} else {
			destination.set(left.x + right.x, left.y + right.y, left.z + right.z, left.w + right.w);
			return destination;
		}
	}

	/**
	 * Subtracts two vectors from each other and places the result in the destination vector.
	 *
	 * @param left The left source vector.
	 * @param right The right source vector.
	 * @param destination The destination vector or null if a new vector is to be created.
	 *
	 * @return The destination matrix.
	 */
	public static Vector4f subtract(final Vector4f left, final Vector4f right, Vector4f destination) {
		if (destination == null) {
			return new Vector4f(left.x - right.x, left.y - right.y, left.z - right.z, left.w - right.w);
		} else {
			destination.set(left.x - right.x, left.y - right.y, left.z - right.z, left.w - right.w);
			return destination;
		}
	}

	/**
	 * Calculates the angle between two vectors.
	 *
	 * @param left The left source vector.
	 * @param right The right source vector.
	 *
	 * @return The angle between the two vectors, in radians.
	 */
	public static float angle(final Vector4f left, final Vector4f right) {
		float dls = dot(left, right) / (left.length() * right.length());

		if (dls < -1f) {
			dls = -1f;
		} else if (dls > 1.0f) {
			dls = 1.0f;
		}

		return (float) Math.acos(dls);
	}

	/**
	 * The dot product of two vectors is calculated as v1.x * v2.x + v1.y * v2.y + v1.z * v2.z + v1.w * v2.w
	 *
	 * @param left The left source vector.
	 * @param right The right source vector.
	 *
	 * @return Left dot right.
	 */
	public static float dot(final Vector4f left, final Vector4f right) {
		return left.x * right.x + left.y * right.y + left.z * right.z + left.w * right.w;
	}

	/**
	 * Loads from another Vector4f.
	 *
	 * @param source The source vector.
	 *
	 * @return this.
	 */
	public Vector4f set(final Vector4f source) {
		x = source.getX();
		y = source.getY();
		z = source.getZ();
		w = source.getW();
		return this;
	}

	/**
	 * @return x.
	 */
	public float getX() {
		return x;
	}

	/**
	 * Set X.
	 *
	 * @param x The new X value.
	 */
	public void setX(final float x) {
		this.x = x;
	}

	/**
	 * @return y.
	 */
	public float getY() {
		return y;
	}

	/**
	 * Set Y.
	 *
	 * @param y The new Y value.
	 */
	public void setY(final float y) {
		this.y = y;
	}

	/**
	 * @return z.
	 */
	public float getZ() {
		return z;
	}

	/**
	 * Set Z.
	 *
	 * @param z The new Z value.
	 */
	public void setZ(final float z) {
		this.z = z;
	}

	/**
	 * @return w.
	 */
	public float getW() {
		return w;
	}

	/**
	 * Set W.
	 *
	 * @param w The new W value.
	 */
	public void setW(final float w) {
		this.w = w;
	}

	/**
	 * Sets values in the vector.
	 *
	 * @param x The new X value.
	 * @param y The new Y value.
	 */
	public void set(final float x, final float y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Sets values in the vector.
	 *
	 * @param x The new X value.
	 * @param y The new Y value.
	 * @param z The new z value.
	 */
	public void set(final float x, final float y, final float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * Sets values in the vector.
	 *
	 * @param x The new X value.
	 * @param y The new Y value.
	 * @param z The new z value.
	 * @param w The new w value.
	 */
	public void set(final float x, final float y, final float z, final float w) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	/**
	 * Translates this vector.
	 *
	 * @param x The translation in x.
	 * @param y the translation in y.
	 * @param z the translation in z.
	 * @param w the translation in w.
	 *
	 * @return this.
	 */
	public Vector4f translate(final float x, final float y, final float z, final float w) {
		this.x += x;
		this.y += y;
		this.z += z;
		this.w += w;
		return this;
	}

	/**
	 * Negates this vector and places the result in a destination vector.
	 *
	 * @param destination The destination vector or null if a new vector is to be created.
	 *
	 * @return The destination vector.
	 */
	public Vector4f negate(Vector4f destination) {
		if (destination == null) {
			destination = new Vector4f();
		}

		destination.x = -x;
		destination.y = -y;
		destination.z = -z;
		destination.w = -w;
		return destination;
	}

	/**
	 * normalizes this vector and places the result in a destination vector.
	 *
	 * @param destination The destination vector or null if a new vector is to be created.
	 *
	 * @return The destination vector.
	 */
	public Vector4f normalize(Vector4f destination) {
		float l = length();

		if (destination == null) {
			destination = new Vector4f(x / l, y / l, z / l, w / l);
		} else {
			destination.set(x / l, y / l, z / l, w / l);
		}

		return destination;
	}

	/**
	 * Loads this vector from a FloatBuffer.
	 *
	 * @param buffer The buffer to load it from, at the current position.
	 *
	 * @return this.
	 */
	public Vector4f load(final FloatBuffer buffer) {
		x = buffer.get();
		y = buffer.get();
		z = buffer.get();
		w = buffer.get();
		return this;
	}

	/**
	 * Negates this vector.
	 *
	 * @return this.
	 */
	public Vector4f negate() {
		x = -x;
		y = -y;
		z = -z;
		w = -w;
		return this;
	}

	/**
	 * @return The length of the vector.
	 */
	public float length() {
		return (float) Math.sqrt(lengthSquared());
	}

	/**
	 * normalizes this vector.
	 *
	 * @return this.
	 */
	public final Vector4f normalize() {
		float len = length();

		if (len != 0.0f) {
			float l = 1.0f / len;
			return scale(l);
		} else {
			throw new IllegalStateException("Zero length vector");
		}
	}

	/**
	 * Scales this vector.
	 *
	 * @param scale The scale factor.
	 *
	 * @return this.
	 */
	public Vector4f scale(final float scale) {
		x *= scale;
		y *= scale;
		z *= scale;
		w *= scale;
		return this;
	}

	/**
	 * @return The length squared of the vector.
	 */
	public float lengthSquared() {
		return x * x + y * y + z * z + w * w;
	}

	/**
	 * Stores this vector in a FloatBuffer.
	 *
	 * @param buffer The buffer to store it in, at the current position.
	 *
	 * @return this.
	 */
	public Vector4f store(FloatBuffer buffer) {
		buffer.put(x);
		buffer.put(y);
		buffer.put(z);
		buffer.put(w);
		return this;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}

		Vector4f other = (Vector4f) obj;

		return x == other.x && y == other.y && z == other.z && w == other.w;
	}

	@Override
	public String toString() {
		return "Vector4f[" + x + ", " + y + ", " + z + ", " + w + "]";
	}
}
