package engine.space;

import engine.physics.AABB;
import engine.toolbox.Maths;
import engine.toolbox.vector.Matrix4f;

/**
 * Represents the region of space in the modeled world that may appear on the screen.
 */
public class Frustum {
	// Each frustum planes.
	public static final int RIGHT = 0;
	public static final int LEFT = 1;
	public static final int BOTTOM = 2;
	public static final int TOP = 3;
	public static final int BACK = 4;
	public static final int FRONT = 5;

	// The values stored in the planes.
	public static final int A = 0;
	public static final int B = 1;
	public static final int C = 2;
	public static final int D = 3;
	private static Frustum frustum = new Frustum();
	public float[][] m_Frustum = new float[6][4];

	/**
	 * Creates a frustum from the view and projection matrix.
	 *
	 * @param projection The projection matrix.
	 * @param viewMatrix The view matrix.
	 *
	 * @return A frustum created from the projection and view matrices.
	 */
	public static Frustum getFrustum(Matrix4f projection, Matrix4f viewMatrix) {
		frustum.calculateFrustum(projection, viewMatrix);
		return frustum;
	}

	private void calculateFrustum(Matrix4f projection, Matrix4f viewMatrix) {
		float[] proj = Maths.matrixToArray(projection);
		float[] view = Maths.matrixToArray(viewMatrix);
		float[] clip = new float[16];

		clip[0] = view[0] * proj[0] + view[1] * proj[4] + view[2] * proj[8] + view[3] * proj[12];
		clip[1] = view[0] * proj[1] + view[1] * proj[5] + view[2] * proj[9] + view[3] * proj[13];
		clip[2] = view[0] * proj[2] + view[1] * proj[6] + view[2] * proj[10] + view[3] * proj[14];
		clip[3] = view[0] * proj[3] + view[1] * proj[7] + view[2] * proj[11] + view[3] * proj[15];

		clip[4] = view[4] * proj[0] + view[5] * proj[4] + view[6] * proj[8] + view[7] * proj[12];
		clip[5] = view[4] * proj[1] + view[5] * proj[5] + view[6] * proj[9] + view[7] * proj[13];
		clip[6] = view[4] * proj[2] + view[5] * proj[6] + view[6] * proj[10] + view[7] * proj[14];
		clip[7] = view[4] * proj[3] + view[5] * proj[7] + view[6] * proj[11] + view[7] * proj[15];

		clip[8] = view[8] * proj[0] + view[9] * proj[4] + view[10] * proj[8] + view[11] * proj[12];
		clip[9] = view[8] * proj[1] + view[9] * proj[5] + view[10] * proj[9] + view[11] * proj[13];
		clip[10] = view[8] * proj[2] + view[9] * proj[6] + view[10] * proj[10] + view[11] * proj[14];
		clip[11] = view[8] * proj[3] + view[9] * proj[7] + view[10] * proj[11] + view[11] * proj[15];

		clip[12] = view[12] * proj[0] + view[13] * proj[4] + view[14] * proj[8] + view[15] * proj[12];
		clip[13] = view[12] * proj[1] + view[13] * proj[5] + view[14] * proj[9] + view[15] * proj[13];
		clip[14] = view[12] * proj[2] + view[13] * proj[6] + view[14] * proj[10] + view[15] * proj[14];
		clip[15] = view[12] * proj[3] + view[13] * proj[7] + view[14] * proj[11] + view[15] * proj[15];

		// This will extract the LEFT side of the frustum
		m_Frustum[LEFT][A] = clip[3] - clip[0];
		m_Frustum[LEFT][B] = clip[7] - clip[4];
		m_Frustum[LEFT][C] = clip[11] - clip[8];
		m_Frustum[LEFT][D] = clip[15] - clip[12];

		normalizePlane(m_Frustum, LEFT);

		// This will extract the RIGHT side of the frustum
		m_Frustum[RIGHT][A] = clip[3] + clip[0];
		m_Frustum[RIGHT][B] = clip[7] + clip[4];
		m_Frustum[RIGHT][C] = clip[11] + clip[8];
		m_Frustum[RIGHT][D] = clip[15] + clip[12];

		normalizePlane(m_Frustum, RIGHT);

		// This will extract the BOTTOM side of the frustum
		m_Frustum[BOTTOM][A] = clip[3] + clip[1];
		m_Frustum[BOTTOM][B] = clip[7] + clip[5];
		m_Frustum[BOTTOM][C] = clip[11] + clip[9];
		m_Frustum[BOTTOM][D] = clip[15] + clip[13];

		normalizePlane(m_Frustum, BOTTOM);

		// This will extract the TOP side of the frustum
		m_Frustum[TOP][A] = clip[3] - clip[1];
		m_Frustum[TOP][B] = clip[7] - clip[5];
		m_Frustum[TOP][C] = clip[11] - clip[9];
		m_Frustum[TOP][D] = clip[15] - clip[13];

		normalizePlane(m_Frustum, TOP);

		// This will extract the FRONT side of the frustum
		m_Frustum[FRONT][A] = clip[3] - clip[2];
		m_Frustum[FRONT][B] = clip[7] - clip[6];
		m_Frustum[FRONT][C] = clip[11] - clip[10];
		m_Frustum[FRONT][D] = clip[15] - clip[14];

		normalizePlane(m_Frustum, FRONT);

		// This will extract the BACK side of the frustum
		m_Frustum[BACK][A] = clip[3] + clip[2];
		m_Frustum[BACK][B] = clip[7] + clip[6];
		m_Frustum[BACK][C] = clip[11] + clip[10];
		m_Frustum[BACK][D] = clip[15] + clip[14];

		normalizePlane(m_Frustum, BACK);
	}

	private void normalizePlane(float[][] frustum, int side) {
		float magnitude = (float) Math.sqrt(frustum[side][A] * frustum[side][A] + frustum[side][B] * frustum[side][B] + frustum[side][C] * frustum[side][C]);

		frustum[side][A] /= magnitude;
		frustum[side][B] /= magnitude;
		frustum[side][C] /= magnitude;
		frustum[side][D] /= magnitude;
	}

	/**
	 * Is the point contained in the frustum?
	 *
	 * @param x The points X coord.
	 * @param y The points Y coord.
	 * @param z The points Z coord.
	 *
	 * @return True if contained, false if outside.
	 */
	public boolean pointInFrustum(float x, float y, float z) {
		for (int i = 0; i < 6; i++) {
			if (m_Frustum[i][0] * x + m_Frustum[i][1] * y + m_Frustum[i][2] * z + m_Frustum[i][3] <= 0.0F) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Is the sphere contained in the frustum?
	 *
	 * @param x The sphere X coord.
	 * @param y The sphere Y coord.
	 * @param z The sphere Z coord.
	 * @param radius The spheres radius.
	 *
	 * @return True if contained, false if outside.
	 */
	public boolean sphereInFrustum(float x, float y, float z, float radius) {
		for (int i = 0; i < 6; i++) {
			if (m_Frustum[i][0] * x + m_Frustum[i][1] * y + m_Frustum[i][2] * z + m_Frustum[i][3] <= -radius) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Is the cube contained fully in the frustum?
	 *
	 * @param x1 The point 1's X coord.
	 * @param y1 The point 1's Y coord.
	 * @param z1 The point 1's Z coord.
	 * @param x2 The point 2's X coord.
	 * @param y2 The point 2's Y coord.
	 * @param z2 The point 2's Z coord.
	 *
	 * @return True if fully contained, false if outside.
	 */
	public boolean cubeFullyInFrustum(float x1, float y1, float z1, float x2, float y2, float z2) {
		for (int i = 0; i < 6; i++) {
			if (m_Frustum[i][0] * x1 + m_Frustum[i][1] * y1 + m_Frustum[i][2] * z1 + m_Frustum[i][3] <= 0.0F) {
				return false;
			}

			if (m_Frustum[i][0] * x2 + m_Frustum[i][1] * y1 + m_Frustum[i][2] * z1 + m_Frustum[i][3] <= 0.0F) {
				return false;
			}

			if (m_Frustum[i][0] * x1 + m_Frustum[i][1] * y2 + m_Frustum[i][2] * z1 + m_Frustum[i][3] <= 0.0F) {
				return false;
			}

			if (m_Frustum[i][0] * x2 + m_Frustum[i][1] * y2 + m_Frustum[i][2] * z1 + m_Frustum[i][3] <= 0.0F) {
				return false;
			}

			if (m_Frustum[i][0] * x1 + m_Frustum[i][1] * y1 + m_Frustum[i][2] * z2 + m_Frustum[i][3] <= 0.0F) {
				return false;
			}

			if (m_Frustum[i][0] * x2 + m_Frustum[i][1] * y1 + m_Frustum[i][2] * z2 + m_Frustum[i][3] <= 0.0F) {
				return false;
			}

			if (m_Frustum[i][0] * x1 + m_Frustum[i][1] * y2 + m_Frustum[i][2] * z2 + m_Frustum[i][3] <= 0.0F) {
				return false;
			}

			if (m_Frustum[i][0] * x2 + m_Frustum[i][1] * y2 + m_Frustum[i][2] * z2 + m_Frustum[i][3] <= 0.0F) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Is the aabb contained partially in the frustum?
	 *
	 * @param aabb The AABB to be checked.
	 *
	 * @return True if partially contained, false if outside.
	 */
	public boolean aabbInFrustum(AABB aabb) {
		return aabb == null || cubeInFrustum(aabb.getMinExtents().getX(), aabb.getMinExtents().getY(), aabb.getMinExtents().getZ(), aabb.getMaxExtents().getX(), aabb.getMaxExtents().getY(), aabb.getMaxExtents().getZ());
	}

	/**
	 * Is the cube contained partially in the frustum?
	 *
	 * @param x1 The point 1's X coord.
	 * @param y1 The point 1's Y coord.
	 * @param z1 The point 1's Z coord.
	 * @param x2 The point 2's X coord.
	 * @param y2 The point 2's Y coord.
	 * @param z2 The point 2's Z coord.
	 *
	 * @return True if partially contained, false if outside.
	 */
	public boolean cubeInFrustum(float x1, float y1, float z1, float x2, float y2, float z2) {
		for (int i = 0; i < 6; i++) {
			if (m_Frustum[i][0] * x1 + m_Frustum[i][1] * y1 + m_Frustum[i][2] * z1 + m_Frustum[i][3] <= 0.0F && m_Frustum[i][0] * x2 + m_Frustum[i][1] * y1 + m_Frustum[i][2] * z1 + m_Frustum[i][3] <= 0.0F && m_Frustum[i][0] * x1 + m_Frustum[i][1] * y2 + m_Frustum[i][2] * z1 + m_Frustum[i][3] <= 0.0F && m_Frustum[i][0] * x2 + m_Frustum[i][1] * y2 + m_Frustum[i][2] * z1 + m_Frustum[i][3] <= 0.0F && m_Frustum[i][0] * x1 + m_Frustum[i][1] * y1 + m_Frustum[i][2] * z2 + m_Frustum[i][3] <= 0.0F && m_Frustum[i][0] * x2 + m_Frustum[i][1] * y1 + m_Frustum[i][2] * z2 + m_Frustum[i][3] <= 0.0F && m_Frustum[i][0] * x1 + m_Frustum[i][1] * y2 + m_Frustum[i][2] * z2 + m_Frustum[i][3] <= 0.0F && m_Frustum[i][0] * x2 + m_Frustum[i][1] * y2 + m_Frustum[i][2] * z2 + m_Frustum[i][3] <= 0.0F) {
				return false;
			}
		}

		return true;
	}
}