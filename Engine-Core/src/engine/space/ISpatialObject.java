package engine.space;

import engine.physics.AABB;

/**
 * Represents an object that has some notion of space, and can be stored in a {@link ISpatialStructure}.
 */
public interface ISpatialObject {
	/**
	 * @return Returns a AABB fully enclosing the object.
	 */
	AABB getAABB();
}
