/**
 * Contains classes useful for representing and manipulating multidimensional space.
 */
package engine.space;