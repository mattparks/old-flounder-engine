package engine.devices;

import engine.basics.options.OptionsGraphics;
import engine.toolbox.Logger;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWFramebufferSizeCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.system.MemoryUtil;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.Calendar;

/**
 * Manages the creation, updating and destruction of the display, as well as timing and frame times.
 */
public class DeviceDisplay {
	public static GLFWErrorCallback CALLBACK_ERROR = GLFWErrorCallback.createPrint(System.err);
	public static GLFWWindowSizeCallback CALLBACK_SCREEN_SIZE = new ScreenSizeCallback();
	public static GLFWFramebufferSizeCallback CALLBACK_FRAMEBUFFER_SIZE = new FrameBufferSizeCallback();

	private static boolean currentlyFullscreen;
	private static boolean closeRequested;
	private static long window;
	private static Canvas canvas;
	private static int oldWidth, oldHeight;

	/**
	 * Creates a GLFW display device.
	 *
	 * @param canvas A optional canvas that the display can be attached to.
	 */
	public static void createDeviceDisplay(Canvas canvas) {
		DeviceDisplay.closeRequested = false;
		DeviceDisplay.canvas = canvas;
		DeviceDisplay.oldWidth = OptionsGraphics.DISPLAY_WIDTH;
		DeviceDisplay.oldHeight = OptionsGraphics.DISPLAY_HEIGHT;
		DeviceDisplay.window = -1;

		// Sets up an error callback.
		GLFW.glfwSetErrorCallback(CALLBACK_ERROR);

		// Initializes GLFW. Most GLFW functions will not work before doing this.
		if (GLFW.glfwInit() != GL11.GL_TRUE) {
			throw new IllegalStateException("Unable to initialize GLFW");
		}

		// Creates a new window.
		createDisplay();

		// Logs OpenGL version info.
		Logger.log("Number of Cores: " + Runtime.getRuntime().availableProcessors());
		Logger.log("OpenGL Version: " + GL11.glGetString(GL11.GL_VERSION));
	}

	/**
	 * @return Returns the current GLFW time * 1000.
	 */
	public static float getCurrentTime() {
		return (float) (GLFW.glfwGetTime() * 1000);
	}

	/**
	 * Update the display. Should be called once every frame.
	 */
	public static void update() {
		// Polls for window events. The key callback will only be invoked during this call.
		GLFW.glfwPollEvents();

		// Updates the displays createDisplay capabilities.
		if (OptionsGraphics.FULLSCREEN != currentlyFullscreen && canvas == null) {
			// Destroys the old window (if it exists).
			GLFW.glfwDestroyWindow(window);

			// Creates a new window.
			createDisplay();
		}

		// Updates the display image.
		GLFW.glfwSwapBuffers(window); // Swaps the colour buffers.

		// If the canvas exists, then paint the windows image to it.
		if (canvas != null) {
			ByteBuffer nativeBuffer = BufferUtils.createByteBuffer(getWidth() * getHeight() * 3);
			BufferedImage image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_3BYTE_BGR);
			GL11.glReadPixels(0, 0, getWidth(), getHeight(), GL12.GL_BGR, GL11.GL_UNSIGNED_BYTE, nativeBuffer);
			byte[] imgData = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
			nativeBuffer.get(imgData);

			Graphics2D graphics = image.createGraphics();
			canvas.printAll(graphics);
			graphics.dispose();
		}
	}

	private static void createDisplay() { // TODO: Fix switching!
		boolean creatingFromNothing = window == -1;
		boolean isGoingFullscreen = false;

		// Gets the resolution of the primary monitor.
		GLFWVidMode vidMode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor());

		// Sets values from current window so it can be reverted back to later.
		if (OptionsGraphics.FULLSCREEN && canvas == null) {
			oldWidth = getWidth();
			oldHeight = getHeight();

			OptionsGraphics.DISPLAY_WIDTH = vidMode.width();
			OptionsGraphics.DISPLAY_HEIGHT = vidMode.height();
			isGoingFullscreen = true;
		} else {
			OptionsGraphics.DISPLAY_WIDTH = oldWidth;
			OptionsGraphics.DISPLAY_HEIGHT = oldHeight;
		}

		// Configures the window.
		GLFW.glfwDefaultWindowHints();
		GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, 3);
		GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR, 3);
		GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GL11.GL_FALSE); // The window will stay hidden until after creation.
		GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, isGoingFullscreen ? GL11.GL_FALSE : GL11.GL_TRUE); // The window will be resizable depending on if its createDisplay.

		// Creates a new window.
		window = GLFW.glfwCreateWindow(OptionsGraphics.DISPLAY_WIDTH, OptionsGraphics.DISPLAY_HEIGHT, OptionsGraphics.DISPLAY_TITLE, isGoingFullscreen ? GLFW.glfwGetPrimaryMonitor() : MemoryUtil.NULL, MemoryUtil.NULL);

		// Checks if the window was created successfully.
		if (window == MemoryUtil.NULL) {
			throw new RuntimeException("Failed to create the GLFW window");
		}

		// Sets up the engines callbacks.
		GLFW.glfwSetKeyCallback(window, DeviceKeyboard.CALLBACK_KEYBOARD);
		GLFW.glfwSetScrollCallback(window, DeviceMouse.CALLBACK_MOUSE_SCROLL);
		GLFW.glfwSetMouseButtonCallback(window, DeviceMouse.CALLBACK_MOUSE_BUTTON);
		GLFW.glfwSetCursorPosCallback(window, DeviceMouse.CALLBACK_MOUSE_POSITION);
		GLFW.glfwSetCursorEnterCallback(window, DeviceMouse.CALLBACK_MOUSE_ENTER);
		GLFW.glfwSetWindowSizeCallback(window, DeviceDisplay.CALLBACK_SCREEN_SIZE);
		GLFW.glfwSetFramebufferSizeCallback(window, CALLBACK_FRAMEBUFFER_SIZE);

		// Makes the OpenGL context current.
		GLFW.glfwMakeContextCurrent(window);

		// Enables v-sync.
		GLFW.glfwSwapInterval(OptionsGraphics.V_SYNC ? 1 : 0);

		// Enables GL for LWJGL's usage on this thread.
		GL.createCapabilities();
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		GL11.glViewport(0, 0, OptionsGraphics.DISPLAY_WIDTH, OptionsGraphics.DISPLAY_HEIGHT);

		// Centers the window.
		if (!isGoingFullscreen) {
			GLFW.glfwSetWindowPos(window, (vidMode.width() - OptionsGraphics.DISPLAY_WIDTH) / 2, (vidMode.height() - OptionsGraphics.DISPLAY_HEIGHT) / 2);
		}

		// Makes the window visible.
		if (canvas == null) {
			GLFW.glfwShowWindow(window);
		} else {
			GLFW.glfwHideWindow(window);
		}

		// Puts every value on the same page.
		currentlyFullscreen = isGoingFullscreen;
		OptionsGraphics.FULLSCREEN = isGoingFullscreen;
	}

	/**
	 * Takes a screenshot of the current image of the display and saves it into the screenshots folder a png image.
	 */
	public static void screenshot() {
		// Tries to create an image, otherwise throws an exception.
		String name = Calendar.getInstance().get(Calendar.MONTH) + 1 + "." + Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + "." + Calendar.getInstance().get(Calendar.HOUR) + "." + Calendar.getInstance().get(Calendar.MINUTE) + "." + (Calendar.getInstance().get(Calendar.SECOND) + 1);
		File saveDirectory = new File("screenshots");

		if (!saveDirectory.exists()) {
			try {
				saveDirectory.mkdir();
			} catch (SecurityException e) {
				System.err.println("The screenshot directory could not be created!");
				e.printStackTrace();
				return;
			}
		}

		File file = new File(saveDirectory + "/" + name + ".png"); // The file to save the pixels too.
		String format = "png"; // "PNG" or "JPG".

		try { // Tries to create image, else show exception.
			// Creates an rbg array of total pixels.
			int[] pixels = new int[getWidth() * getHeight()];
			int bIndex;

			// Allocates space for RBG pixels.
			ByteBuffer fb = ByteBuffer.allocateDirect(getWidth() * getHeight() * 3);

			// Grabs a copy of the current frame contents as RGB.
			GL11.glReadPixels(0, 0, getWidth(), getHeight(), GL11.GL_RGB, GL11.GL_UNSIGNED_BYTE, fb);

			BufferedImage imageIn = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);

			// Converts RGB data in ByteBuffer to integer array.
			for (int i = 0; i < pixels.length; i++) {
				bIndex = i * 3;
				pixels[i] = ((fb.get(bIndex) << 16)) + ((fb.get(bIndex + 1) << 8)) + (fb.get(bIndex + 2));
			}

			// Allocates coloured pixels to buffered Image.
			imageIn.setRGB(0, 0, getWidth(), getHeight(), pixels, 0, getWidth());

			// Creates the transformation direction (horizontal).
			AffineTransform at = AffineTransform.getScaleInstance(1, -1);
			at.translate(0, -imageIn.getHeight(null));

			// Applies transformation.
			AffineTransformOp opRotated = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
			ImageIO.write(opRotated.filter(imageIn, null), format, file);
		} catch (Exception e) {
			System.out.println("ScreenShot() exception: " + e);
		}
	}

	/**
	 * @return Returns the width of the display in pixels.
	 */
	public static int getWidth() {
		return OptionsGraphics.DISPLAY_WIDTH;
	}

	/**
	 * @return Returns the height of the display in pixels.
	 */
	public static int getHeight() {
		return OptionsGraphics.DISPLAY_HEIGHT;
	}

	/**
	 * @return Returns the aspect ratio between the displays width and height.
	 */
	public static float getAspectRatio() {
		return getWidth() * 1f / (getHeight() * 1f);
	}

	/**
	 * @return {@code true} if the display should currently be open. Returns {@code false} if Esc is pressed or the exit button is pressed.
	 */
	public static boolean isOpen() {
		return !(GLFW.glfwWindowShouldClose(window) == GL11.GL_TRUE || closeRequested);
	}

	/**
	 * Indicates that the game has been requested to close. At the end of the current frame the main game loop will exit.
	 */
	public static void requestClosure() {
		closeRequested = true;
	}

	/**
	 * Closes the GLFW display, do not renderObjects after calling this.
	 */
	public static void cleanUp() {
		GLFW.glfwDestroyWindow(window);
		GLFW.glfwTerminate();
		CALLBACK_SCREEN_SIZE.release();
		CALLBACK_FRAMEBUFFER_SIZE.release();
		CALLBACK_ERROR.release();
	}

	private static class ScreenSizeCallback extends GLFWWindowSizeCallback {
		@Override
		public void invoke(long window, int width, int height) {
			OptionsGraphics.DISPLAY_WIDTH = width;
			OptionsGraphics.DISPLAY_HEIGHT = height;
		}
	}

	private static class FrameBufferSizeCallback extends GLFWFramebufferSizeCallback {
		@Override
		public void invoke(long window, int width, int height) {
			GL11.glViewport(0, 0, width, height);
		}
	}
}
