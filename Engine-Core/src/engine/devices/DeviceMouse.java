package engine.devices;

import engine.basics.EngineCore;
import engine.toolbox.Maths;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWCursorEnterCallback;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;
import org.lwjgl.opengl.GL11;

public class DeviceMouse {
	public static final int MOUSE_BUTTON_1 = 0;
	public static final int MOUSE_BUTTON_2 = 1;
	public static final int MOUSE_BUTTON_3 = 2;
	public static final int MOUSE_BUTTON_4 = 3;
	public static final int MOUSE_BUTTON_5 = 4;
	public static final int MOUSE_BUTTON_6 = 5;
	public static final int MOUSE_BUTTON_7 = 6;
	public static final int MOUSE_BUTTON_8 = 7;
	public static final int MOUSE_BUTTON_LAST = MOUSE_BUTTON_8;
	public static final int MOUSE_BUTTON_LEFT = MOUSE_BUTTON_1;
	public static final int MOUSE_BUTTON_RIGHT = MOUSE_BUTTON_2;
	public static final int MOUSE_BUTTON_MIDDLE = MOUSE_BUTTON_3;

	public static final MouseScrollCallback CALLBACK_MOUSE_SCROLL = new MouseScrollCallback();
	public static final MouseButtonCallback CALLBACK_MOUSE_BUTTON = new MouseButtonCallback();
	public static final GLFWCursorPosCallback CALLBACK_MOUSE_POSITION = new MousePositionCallback();
	public static final GLFWCursorEnterCallback CALLBACK_MOUSE_ENTER = new MouseEnterCallback();

	private static boolean[] buttons = new boolean[MOUSE_BUTTON_LAST];
	private static boolean displaySelected;
	private static double lastMousePositionX;
	private static double lastMousePositionY;
	private static double mousePositionX;
	private static double mousePositionY;
	private static double mouseDeltaX;
	private static double mouseDeltaY;
	private static double mouseDeltaWheel;

	public static void update() {
		mouseDeltaX = (lastMousePositionX - mousePositionX) * EngineCore.getDeltaSeconds();
		mouseDeltaY = (lastMousePositionY - mousePositionY) * EngineCore.getDeltaSeconds();

		lastMousePositionX = mousePositionX;
		lastMousePositionY = mousePositionY;

		mouseDeltaWheel += (mouseDeltaWheel > 0) ? -25 * EngineCore.getDeltaSeconds() : 25 * EngineCore.getDeltaSeconds();
		mouseDeltaWheel = Maths.deadband(0.875f, mouseDeltaWheel);
	}

	/**
	 * Gets whether or not a particular mouse button is currently pressed.
	 *
	 * @param button The mouse button to test.
	 *
	 * @return Whether or not the button is currently pressed.
	 */
	public static boolean getMouse(int button) {
		return buttons[button];
	}

	/**
	 * @return Returns true if the cursor is inside of the window.
	 */
	public static boolean isDisplaySelected() {
		return displaySelected;
	}

	/**
	 * @return The location of the mouse cursor on x; public static final int in screen coords.
	 */
	public static double getMouseX() {
		return mousePositionX;
	}

	/**
	 * @return The location of the mouse cursor on y; public static final int in screen coords.
	 */
	public static double getMouseY() {
		return mousePositionY;
	}

	/**
	 * @return The amount the mouse has moved since the previous update on X.
	 */
	public static double getMouseDeltaX() {
		return mouseDeltaX * EngineCore.getDeltaSeconds();
	}

	/**
	 * @return The amount the mouse has moved since the previous update on Y.
	 */
	public static double getMouseDeltaY() {
		return mouseDeltaY * EngineCore.getDeltaSeconds();
	}

	/**
	 * @return The amount the mouse wheel has moved since the previous update.
	 */
	public static double getMouseDeltaWheel() {
		return mouseDeltaWheel;
	}

	/**
	 * Closes the input system, do not check buttons after calling this.
	 */
	public static void cleanUp() {
		CALLBACK_MOUSE_SCROLL.release();
		CALLBACK_MOUSE_BUTTON.release();
		CALLBACK_MOUSE_POSITION.release();
		CALLBACK_MOUSE_ENTER.release();
	}

	private static class MouseScrollCallback extends GLFWScrollCallback {
		@Override
		public void invoke(long window, double xOffset, double yOffset) {
			mouseDeltaWheel += yOffset;
		}
	}

	private static class MouseButtonCallback extends GLFWMouseButtonCallback {
		@Override
		public void invoke(long window, int button, int action, int mods) {
			buttons[button] = action != GLFW.GLFW_RELEASE;
		}
	}

	private static class MousePositionCallback extends GLFWCursorPosCallback {
		@Override
		public void invoke(long window, double xPosition, double yPosition) {
			double mouseXBefore = mousePositionX;
			double mouseYBefore = mousePositionY;

			mousePositionX = xPosition / DeviceDisplay.getWidth();
			mousePositionY = yPosition / DeviceDisplay.getHeight();
		}
	}

	private static class MouseEnterCallback extends GLFWCursorEnterCallback {
		@Override
		public void invoke(long window, int entered) {
			displaySelected = entered == GL11.GL_TRUE;
		}
	}
}
