package engine.devices;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

public class DeviceJoystick {
	public static final int JOYSTICK_1 = 0;
	public static final int JOYSTICK_2 = 1;
	public static final int JOYSTICK_3 = 2;
	public static final int JOYSTICK_4 = 3;
	public static final int JOYSTICK_5 = 4;
	public static final int JOYSTICK_6 = 5;
	public static final int JOYSTICK_7 = 6;
	public static final int JOYSTICK_8 = 7;
	public static final int JOYSTICK_9 = 8;
	public static final int JOYSTICK_10 = 9;
	public static final int JOYSTICK_11 = 10;
	public static final int JOYSTICK_12 = 11;
	public static final int JOYSTICK_13 = 12;
	public static final int JOYSTICK_14 = 13;
	public static final int JOYSTICK_15 = 14;
	public static final int JOYSTICK_16 = 15;
	public static final int JOYSTICK_LAST = JOYSTICK_16;

	private static FloatBuffer[] joystickAxes;
	private static ByteBuffer[] joystickButtons;

	public static void createJoystickDevice() {
		joystickAxes = new FloatBuffer[JOYSTICK_LAST];
		joystickButtons = new ByteBuffer[joystickAxes.length];
	}

	public static void update() {
		for (int i = 0; i < joystickAxes.length; i++) {
			if (joystickAxes[i] == null) {
				continue;
			}

			updateJoystick(i);
		}
	}

	private static void updateJoystick(int i) {
		FloatBuffer newAxes = GLFW.glfwGetJoystickAxes(i);
		ByteBuffer newButtons = GLFW.glfwGetJoystickButtons(i);

		if (newAxes != null) {
			joystickAxes[i] = newAxes;
		} else if (joystickAxes[i] == null) {
			joystickAxes[i] = BufferUtils.createFloatBuffer(0);
		}

		if (newButtons != null) {
			joystickButtons[i] = newButtons;
		} else if (joystickButtons[i] == null) {
			joystickButtons[i] = BufferUtils.createByteBuffer(0);
		}
	}

	/**
	 * Gets a string describing a particular joystick.
	 *
	 * @param joystick The joystick of interest.
	 *
	 * @return A string describing the specified joystick.
	 */
	public static String getJoystickName(int joystick) {
		return GLFW.glfwGetJoystickName(joystick);
	}

	/**
	 * Gets the value of a joystick's axis.
	 *
	 * @param joystick The joystick of interest.
	 * @param axis The axis of interest.
	 *
	 * @return The value of the joystick's axis.
	 */
	public static double getJoystickAxis(int joystick, int axis) {
		int numAxes = getNumJoystickAxes(joystick);

		if (axis < 0 || axis >= numAxes) {
			return 0.0;
		}

		return joystickAxes[joystick].get(axis);
	}

	/**
	 * Gets the number of axes a joystick offers.
	 *
	 * @param joystick The joystick of interest.
	 *
	 * @return The number of axes the joystick offers.
	 */
	public static int getNumJoystickAxes(int joystick) {
		initJoystick(joystick);
		return joystickAxes[joystick].capacity();
	}

	private static void initJoystick(int i) {
		if (joystickAxes[i] == null) {
			updateJoystick(i);
		}
	}

	/**
	 * Gets the whether a button on a joystick is pressed.
	 *
	 * @param joystick The joystick of interest.
	 * @param button The button of interest.
	 *
	 * @return Whether a button on a joystick is pressed.
	 */
	public static boolean getJoystickButton(int joystick, int button) {
		return !(button < 0 || button >= getNumJoystickButtons(joystick)) && joystickButtons[joystick].get(button) == 1;
	}

	/**
	 * Gets the number of buttons a joystick offers.
	 *
	 * @param joystick The joystick of interest.
	 *
	 * @return The number of buttons the joystick offers.
	 */
	public static int getNumJoystickButtons(int joystick) {
		initJoystick(joystick);
		return joystickButtons[joystick].capacity();
	}

	/**
	 * Closes the input system, do not check buttons after calling this.
	 */
	public static void cleanUp() {
	}
}
