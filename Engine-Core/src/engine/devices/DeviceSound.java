package engine.devices;

import engine.sounds.AudioController;
import engine.sounds.AudioListener;
import engine.sounds.MusicPlayer;
import engine.sounds.PlayRequest;
import engine.sounds.Sound;
import engine.sounds.SoundLoader;
import engine.sounds.SourcePoolManager;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Vector3f;
import org.lwjgl.openal.AL10;
import org.lwjgl.openal.AL11;
import org.lwjgl.openal.ALContext;

/**
 * An Sound Device implemented using OpenAL.
 */
public class DeviceSound {
	public static final MyFile SOUND_FOLDER = new MyFile(MyFile.RES_FOLDER, "sounds");

	private static SourcePoolManager sourcePool;
	private static AudioListener listener;
	private static MusicPlayer musicPlayer;
	private static ALContext context;

	/**
	 * Creates a OpenAL sound device.
	 *
	 * @param theListener The audio listener to be used when playing distanced sounds.
	 */
	public static void createDeviceSound(AudioListener theListener) {
		context = ALContext.create();
		AL10.alGetError();
		AL10.alDistanceModel(AL11.AL_LINEAR_DISTANCE_CLAMPED);
		sourcePool = new SourcePoolManager();
		listener = theListener;
		musicPlayer = new MusicPlayer();
	}

	/**
	 * Updates sounds. Should be called once every frame.
	 */
	public static void update() {
		Vector3f position = listener.getPosition();
		AL10.alListener3f(AL10.AL_POSITION, position.x, position.y, position.z);
		musicPlayer.update();
		sourcePool.update();
	}

	public static AudioController playSystemSound(Sound sound) {
		if (!sound.isLoaded()) {
			return null;
		}

		return sourcePool.play(PlayRequest.newSystemPlayRequest(sound));
	}

	public static AudioController play3DSound(PlayRequest playRequest) {
		if (!playRequest.getSound().isLoaded()) {
			return null;
		}

		return sourcePool.play(playRequest);
	}

	public static AudioListener getListener() {
		return listener;
	}

	public static MusicPlayer getMusicPlayer() {
		return musicPlayer;
	}

	/**
	 * Closes all sound systems, do not play sounds after calling this.
	 */
	public static void cleanUp() {
		SoundLoader.cleanUp();
		sourcePool.cleanUp();
		musicPlayer.cleanUp();
		context.destroy();
	}
}
