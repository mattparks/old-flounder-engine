package engine.visual.fxDrivers;

public abstract class ValueDriver {
	private float currentTime = 0;
	private float length;

	public ValueDriver(float length) {
		this.length = length;
	}

	public float update(float delta) {
		currentTime += delta;
		currentTime %= length;
		float time = currentTime / length;
		return calculateValue(time);
	}

	protected abstract float calculateValue(float time);
}
