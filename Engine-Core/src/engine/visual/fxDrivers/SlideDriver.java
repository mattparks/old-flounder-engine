package engine.visual.fxDrivers;

import engine.toolbox.Maths;

public class SlideDriver extends ValueDriver {
	private float startValue;
	private float endValue;
	private float max = 0;
	private boolean reachedTarget = false;

	public SlideDriver(float start, float end, float length) {
		super(length);
		startValue = start;
		endValue = end;
	}

	@Override
	protected float calculateValue(float time) {
		if (!reachedTarget && time >= max) {
			max = time;
			return Maths.cosInterpolate(startValue, endValue, time);
		} else {
			reachedTarget = true;
			return startValue + (endValue - startValue);
		}
	}
}
