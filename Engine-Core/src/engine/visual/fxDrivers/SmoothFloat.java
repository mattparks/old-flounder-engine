package engine.visual.fxDrivers;

import engine.basics.EngineCore;

public class SmoothFloat {
	private final float agility;
	private float target;
	private float actual;

	public SmoothFloat(float initialValue, float agility) {
		target = initialValue;
		actual = initialValue;
		this.agility = agility;
	}

	public void update() {
		float offset = target - actual;
		float change = offset * EngineCore.getDeltaSeconds() * agility;
		actual += change;
	}

	public float get() {
		return actual;
	}

	public void set(float target) {
		this.target = target;
	}
}
