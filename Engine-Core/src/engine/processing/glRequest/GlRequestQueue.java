package engine.processing.glRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds OpenGL requests that are in queue.
 */
public class GlRequestQueue {
	private List<GlRequest> requestQueue = new ArrayList<>();

	/**
	 * Adds a new GL request to queue.
	 *
	 * @param request The GL request to add.
	 */
	public synchronized void addRequest(GlRequest request) {
		requestQueue.add(request);
	}

	/**
	 * @return Returns the next item in queue and then removes it from this list.
	 */
	public synchronized GlRequest acceptNextRequest() {
		return requestQueue.remove(0);
	}

	/**
	 * @return Returns the time required to process the next item in queue/
	 */
	public synchronized float getNextRequestRequiredTime() {
		if (hasRequests()) {
			return requestQueue.get(0).getTimeRequired();
		} else {
			System.err.println("GL Request Queue is empty!");
			return 10000;
		}
	}

	/**
	 * @return Returns true if there are any items left in queue.
	 */
	public synchronized boolean hasRequests() {
		return !requestQueue.isEmpty();
	}
}
