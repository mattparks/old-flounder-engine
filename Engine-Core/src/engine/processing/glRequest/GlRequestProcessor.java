package engine.processing.glRequest;

/**
 * Class that is responsible for processing OpenGL requests.
 */
public class GlRequestProcessor {
	public static final float MAX_TIME_MILLIS = 10f;
	public static final float MAX_TIME = 100f;

	private static GlRequestQueue requestQueue = new GlRequestQueue();

	/**
	 * Sends a new request into queue.
	 *
	 * @param request The request to add.
	 */
	public static void sendRequest(GlRequest request) {
		requestQueue.addRequest(request);
	}

	/**
	 * Deals with in the time slot available.
	 */
	public static void dealWithTopRequests() {
		float remainingTime = MAX_TIME;

		while (requestQueue.hasRequests()) {
			if (requestQueue.getNextRequestRequiredTime() <= remainingTime) {
				GlRequest request = requestQueue.acceptNextRequest();
				remainingTime -= request.getTimeRequired();
				request.executeGlRequest();
			} else {
				break;
			}
		}
	}

	/**
	 * Completes all requests left in queue.
	 */
	public static void completeAllRequests() {
		while (requestQueue.hasRequests()) {
			requestQueue.acceptNextRequest().executeGlRequest();
		}
	}
}
