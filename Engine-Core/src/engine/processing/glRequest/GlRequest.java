package engine.processing.glRequest;

/**
 * Interface for executable OpenGL requests.
 */
public interface GlRequest {
	/**
	 * Executed when the request is being processed.
	 */
	void executeGlRequest();

	/**
	 * @return Returns a estimate of the time required for the request to be processed.
	 */
	float getTimeRequired();
}
