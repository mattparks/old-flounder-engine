package engine.processing;

/**
 * Class that is responsible for processing resource requests in a separate thread.
 */
public class RequestProcessor extends Thread {
	private static RequestProcessor PROCESSOR = new RequestProcessor();

	private RequestQueue requestQueue = new RequestQueue();
	private boolean running = true;

	private RequestProcessor() {
		start();
	}

	/**
	 * Adds a new resource request to queue.
	 *
	 * @param request The resource request to add.
	 */
	public static void sendRequest(ResourceRequest request) {
		PROCESSOR.addRequestToQueue(request);
	}

	/**
	 * Cleans up the request processor and destroys the thread.
	 */
	public static void cleanUp() {
		PROCESSOR.kill();
	}

	private synchronized void addRequestToQueue(ResourceRequest request) {
		requestQueue.addRequest(request);
		notify();
	}

	private synchronized void kill() {
		running = false;
		notify();
	}

	@Override
	public synchronized void run() {
		while (running || requestQueue.hasRequests()) {
			if (requestQueue.hasRequests()) {
				requestQueue.acceptNextRequest().doResourceRequest();
			} else {
				try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
