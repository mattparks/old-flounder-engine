/**
 * Contains classes for processing types of requests.
 */
package engine.processing;