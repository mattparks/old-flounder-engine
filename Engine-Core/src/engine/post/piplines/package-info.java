/**
 * Contains classes for pipelines that are used creating {@link engine.textures.fbos.FBO}'s by using {@link engine.post.PostFilter}'s.
 */
package engine.post.piplines;