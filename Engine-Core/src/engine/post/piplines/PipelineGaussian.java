package engine.post.piplines;

import engine.post.PostPipeline;
import engine.post.filters.FilterBlurHorizontal;
import engine.post.filters.FilterBlurVertical;
import engine.textures.fbos.FBO;
import engine.toolbox.vector.Vector4f;

public class PipelineGaussian extends PostPipeline {
	private final FilterBlurHorizontal filterBlurHorizontal;
	private final FilterBlurVertical filterBlurVertical;
	private Vector4f blendSpreadValue; // 0 - 1, x being width min, y width being max, z being height min, w width height max..

	public PipelineGaussian(int width, int height, boolean fitToDisplay) {
		filterBlurHorizontal = fitToDisplay ? new FilterBlurHorizontal() : new FilterBlurHorizontal(width, height);
		filterBlurVertical = fitToDisplay ? new FilterBlurVertical() : new FilterBlurVertical(width, height);
		this.blendSpreadValue = new Vector4f(0.0f, 1.0f, 0.0f, 1.0f);
	}

	@Override
	public void renderPipeline(FBO startFBO) {
		filterBlurHorizontal.setBlendSpread(blendSpreadValue);
		filterBlurVertical.setBlendSpread(blendSpreadValue);

		filterBlurHorizontal.applyFilter(startFBO.getColourTexture());
		filterBlurVertical.applyFilter(filterBlurHorizontal.fbo.getColourTexture());
	}

	public void setBlendSpreadValue(Vector4f spreadValue) {
		this.blendSpreadValue = spreadValue;
	}

	public void setScale(float scale) {
		filterBlurHorizontal.setScale(scale);
		filterBlurVertical.setScale(scale);
	}

	@Override
	public FBO getOutput() {
		return filterBlurVertical.fbo;
	}

	@Override
	public void cleanUp() {
		filterBlurHorizontal.cleanUp();
		filterBlurVertical.cleanUp();
	}
}
