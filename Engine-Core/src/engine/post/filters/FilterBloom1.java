package engine.post.filters;

import engine.post.PostFilter;
import engine.toolbox.resources.MyFile;

public class FilterBloom1 extends PostFilter {
	public FilterBloom1() {
		super(new MyFile(PostFilter.POST_LOC, "bloom1Fragment.glsl"));
		super.storeUniforms();
	}

	@Override
	public void storeValues() {
	}
}
