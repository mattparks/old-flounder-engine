package engine.post.filters;

import engine.basics.EngineCore;
import engine.post.PostFilter;
import engine.shaders.UniformFloat;
import engine.toolbox.resources.MyFile;

public class FilterWobble extends PostFilter {
	private UniformFloat moveIt = new UniformFloat("moveIt");
	private float wobbleAmount;

	public FilterWobble() {
		super(new MyFile(PostFilter.POST_LOC, "wobbleFragment.glsl"));
		super.storeUniforms(moveIt);
	}

	@Override
	public void storeValues() {
		moveIt.loadFloat(wobbleAmount += 3 * EngineCore.getDeltaSeconds());
	}
}
