package engine.post.filters;

import engine.devices.DeviceDisplay;
import engine.post.PostFilter;
import engine.shaders.ShaderProgram;
import engine.shaders.UniformFloat;
import engine.shaders.UniformVec4;
import engine.textures.fbos.FBO;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Vector4f;

public class FilterBlurHorizontal extends PostFilter {
	private UniformFloat width = new UniformFloat("width");
	private UniformFloat scale = new UniformFloat("scale");
	private UniformVec4 blendSpread = new UniformVec4("blendSpread"); // 0 - 1, x being width min, y width being max, z being height min, w width height max..
	private int widthValue;
	private float scaleValue;
	private Vector4f blendSpreadValue;
	private boolean fitToDisplay;

	public FilterBlurHorizontal(int widthValue, int heightValue) {
		super(new ShaderProgram(VERTEX_LOCATION, new MyFile(PostFilter.POST_LOC, "blurHorizontalFragment.glsl")), FBO.newFBO(widthValue, heightValue).create());
		fitToDisplay = false;
		init(widthValue);
	}

	public FilterBlurHorizontal() {
		super(new ShaderProgram(VERTEX_LOCATION, new MyFile(PostFilter.POST_LOC, "blurHorizontalFragment.glsl")), FBO.newFBO(DeviceDisplay.getWidth(), DeviceDisplay.getHeight()).fitToScreen().create());
		fitToDisplay = true;
		init(DeviceDisplay.getWidth());
	}

	private void init(int widthValue) {
		super.storeUniforms(width, scale, blendSpread);
		this.widthValue = widthValue;
		this.scaleValue = 2.0f;
		this.blendSpreadValue = new Vector4f(0.0f, 1.0f, 0.0f, 1.0f);
	}

	public void setBlendSpread(Vector4f blendSpreadValue) {
		this.blendSpreadValue = blendSpreadValue;
	}

	public void setScale(float scale) {
		this.scaleValue = scale;
	}

	@Override
	public void storeValues() {
		if (fitToDisplay) {
			widthValue = DeviceDisplay.getWidth();
		}

		width.loadFloat(widthValue);
		scale.loadFloat(scaleValue);
		blendSpread.loadVec4(blendSpreadValue);
	}
}
