package engine.post.filters;

import engine.post.PostFilter;
import engine.toolbox.resources.MyFile;

public class FilterEmboss extends PostFilter {
	public FilterEmboss() {
		super(new MyFile(PostFilter.POST_LOC, "embossFragment.glsl"));
		super.storeUniforms();
	}

	@Override
	public void storeValues() {
	}
}
