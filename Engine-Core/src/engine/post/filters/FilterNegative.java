package engine.post.filters;

import engine.post.PostFilter;
import engine.toolbox.resources.MyFile;

public class FilterNegative extends PostFilter {
	public FilterNegative() {
		super(new MyFile(PostFilter.POST_LOC, "negativeFragment.glsl"));
		super.storeUniforms();
	}

	@Override
	public void storeValues() {
	}
}
