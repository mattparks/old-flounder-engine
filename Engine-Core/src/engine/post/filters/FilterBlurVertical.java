package engine.post.filters;

import engine.devices.DeviceDisplay;
import engine.post.PostFilter;
import engine.shaders.ShaderProgram;
import engine.shaders.UniformFloat;
import engine.shaders.UniformVec4;
import engine.textures.fbos.FBO;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Vector4f;

public class FilterBlurVertical extends PostFilter {
	private UniformFloat height = new UniformFloat("height");
	private UniformFloat scale = new UniformFloat("scale");
	private UniformVec4 blendSpread = new UniformVec4("blendSpread"); // 0 - 1, x being width min, y width being max, z being height min, w width height max..
	private int heightValue;
	private float scaleValue;
	private Vector4f blendSpreadValue;
	private boolean fitToDisplay;

	public FilterBlurVertical(int widthValue, int heightValue) {
		super(new ShaderProgram(VERTEX_LOCATION, new MyFile(PostFilter.POST_LOC, "blurVerticalFragment.glsl")), FBO.newFBO(widthValue, heightValue).create());
		fitToDisplay = false;
		init(heightValue);
	}

	public FilterBlurVertical() {
		super(new ShaderProgram(VERTEX_LOCATION, new MyFile(PostFilter.POST_LOC, "blurVerticalFragment.glsl")), FBO.newFBO(DeviceDisplay.getWidth(), DeviceDisplay.getHeight()).fitToScreen().create());
		fitToDisplay = true;
		init(DeviceDisplay.getHeight());
	}

	private void init(int heightValue) {
		super.storeUniforms(height, scale, blendSpread);
		this.heightValue = heightValue;
		this.scaleValue = 2.0f;
		this.blendSpreadValue = new Vector4f(0.0f, 1.0f, 0.0f, 1.0f);
	}

	public void setBlendSpread(Vector4f blendSpreadValue) {
		this.blendSpreadValue = blendSpreadValue;
	}

	public void setScale(float scale) {
		this.scaleValue = scale;
	}

	@Override
	public void storeValues() {
		if (fitToDisplay) {
			heightValue = DeviceDisplay.getHeight();
		}

		height.loadFloat(heightValue);
		scale.loadFloat(scaleValue);
		blendSpread.loadVec4(blendSpreadValue);
	}
}
