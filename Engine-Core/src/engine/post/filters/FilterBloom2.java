package engine.post.filters;

import engine.post.PostFilter;
import engine.toolbox.resources.MyFile;

public class FilterBloom2 extends PostFilter {
	public FilterBloom2() {
		super(new MyFile(PostFilter.POST_LOC, "bloom2Fragment.glsl"));
		super.storeUniforms();
	}

	@Override
	public void storeValues() {
	}
}
