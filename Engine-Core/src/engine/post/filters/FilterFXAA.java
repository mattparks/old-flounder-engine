package engine.post.filters;

import engine.post.PostFilter;
import engine.toolbox.resources.MyFile;

public class FilterFXAA extends PostFilter {
	public FilterFXAA() {
		super(new MyFile(PostFilter.POST_LOC, "fxaaFragment.glsl"));
		super.storeUniforms();
	}

	@Override
	public void storeValues() {
	}
}
