package engine.post.filters;

import engine.post.PostFilter;
import engine.toolbox.resources.MyFile;

public class FilterSepia extends PostFilter {
	public FilterSepia() {
		super(new MyFile(PostFilter.POST_LOC, "sepiaFragment.glsl"));
		super.storeUniforms();
	}

	@Override
	public void storeValues() {
	}
}
