package engine.post.filters;

import engine.basics.EngineCore;
import engine.post.PostFilter;
import engine.shaders.UniformFloat;
import engine.toolbox.resources.MyFile;

public class FilterDOF extends PostFilter {
	private UniformFloat aimDistance = new UniformFloat("aimDistance");
	private UniformFloat nearPlane = new UniformFloat("nearPlane");
	private UniformFloat farPlane = new UniformFloat("farPlane");

	public FilterDOF() {
		super(new MyFile(PostFilter.POST_LOC, "dofFragment.glsl"));
		super.storeUniforms(aimDistance, nearPlane, farPlane);
	}

	@Override
	public void storeValues() {
		aimDistance.loadFloat(EngineCore.getModule().getCamera().getAimDistance());
		nearPlane.loadFloat(EngineCore.getModule().getCamera().getNearPlane());
		farPlane.loadFloat(EngineCore.getModule().getCamera().getFarPlane());
	}
}
