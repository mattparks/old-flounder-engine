package engine.post.filters;

import engine.post.PostFilter;
import engine.toolbox.resources.MyFile;

public class FilterTone extends PostFilter {
	public FilterTone() {
		super(new MyFile(PostFilter.POST_LOC, "toneFragment.glsl"));
		super.storeUniforms();
	}

	@Override
	public void storeValues() {
	}
}
