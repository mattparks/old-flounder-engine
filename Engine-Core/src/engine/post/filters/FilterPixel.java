package engine.post.filters;

import engine.post.PostFilter;
import engine.toolbox.resources.MyFile;

public class FilterPixel extends PostFilter {
	public FilterPixel() {
		super(new MyFile(PostFilter.POST_LOC, "pixelFragment.glsl"));
		super.storeUniforms();
	}

	@Override
	public void storeValues() {
	}
}
