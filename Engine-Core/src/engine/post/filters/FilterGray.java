package engine.post.filters;

import engine.post.PostFilter;
import engine.toolbox.resources.MyFile;

public class FilterGray extends PostFilter {
	public FilterGray() {
		super(new MyFile(PostFilter.POST_LOC, "grayFragment.glsl"));
		super.storeUniforms();
	}

	@Override
	public void storeValues() {
	}
}
