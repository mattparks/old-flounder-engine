package engine.post;

import engine.devices.DeviceDisplay;
import engine.loaders.Loader;
import engine.shaders.ShaderProgram;
import engine.shaders.Uniform;
import engine.textures.fbos.FBO;
import engine.toolbox.Colour;
import engine.toolbox.OpenglUtils;
import engine.toolbox.resources.MyFile;
import org.lwjgl.opengl.GL11;

/**
 * Represents a post effect shader and on application saves the result into a FBO.
 */
public abstract class PostFilter {
	public static final MyFile POST_LOC = new MyFile("engine/post/filters");
	public static final MyFile VERTEX_LOCATION = new MyFile(POST_LOC, "defaultVertex.glsl");

	private static final float[] POSITIONS = {0, 0, 0, 1, 1, 0, 1, 1};
	private static final int VAO = Loader.createInterleavedVAO(POSITIONS, 2);

	public ShaderProgram shader;
	public FBO fbo;

	public PostFilter(MyFile fragmentShader) {
		this(new ShaderProgram(VERTEX_LOCATION, fragmentShader), FBO.newFBO(DeviceDisplay.getWidth(), DeviceDisplay.getHeight()).fitToScreen().create());
	}

	public PostFilter(ShaderProgram shader, FBO fbo) {
		this.shader = shader;
		this.fbo = fbo;
	}

	/**
	 * Can be used to store values into the shader, this is called when the filter is applied and the shader has been already started.
	 */
	public abstract void storeValues();

	/**
	 * Stores any shader uniforms into the filters shader program.
	 *
	 * @param uniforms The uniforms to store in the shader program.
	 */
	public void storeUniforms(Uniform... uniforms) {
		shader.storeAllUniformLocations(uniforms);
	}

	/**
	 * Renders the filter to its FBO.
	 *
	 * @param textures A list of textures in indexed order to be bound for the shader program.
	 */
	public void applyFilter(int... textures) {
		fbo.bindFrameBuffer();
		OpenglUtils.prepareNewRenderParse(new Colour(1.0f, 1.0f, 1.0f));
		shader.start();
		storeValues();
		OpenglUtils.antialias(false);
		OpenglUtils.disableDepthTesting();
		OpenglUtils.cullBackFaces(true);
		OpenglUtils.bindVAO(VAO, 0);

		for (int i = 0; i < textures.length; i++) {
			OpenglUtils.bindTextureToBank(textures[i], i);
		}

		GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, POSITIONS.length); // Render post filter.

		OpenglUtils.unbindVAO(0);
		shader.stop();
		OpenglUtils.disableBlending();
		OpenglUtils.enableDepthTesting();
		fbo.unbindFrameBuffer();
	}

	/**
	 * Cleans up all of the filter processes and images.
	 */
	public void cleanUp() {
		fbo.delete();
		shader.cleanUp();
	}
}
