package engine.textures;

import engine.processing.glRequest.GlRequest;

public class TextureDeleteRequest implements GlRequest {
	private int textureID;

	public TextureDeleteRequest(int textureID) {
		this.textureID = textureID;
	}

	@Override
	public void executeGlRequest() {
		TextureManager.deleteTexture(textureID);
	}

	@Override
	public float getTimeRequired() {
		return 10;
	}
}
