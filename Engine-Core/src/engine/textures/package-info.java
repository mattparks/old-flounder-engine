/**
 * Contains classes capable of loading and managing textures.
 */
package engine.textures;