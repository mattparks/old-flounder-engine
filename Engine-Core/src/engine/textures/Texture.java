package engine.textures;

import engine.processing.glRequest.GlRequestProcessor;
import engine.toolbox.resources.MyFile;

/**
 * Class that represents a loaded texture.
 */
public class Texture {
	private int textureID;
	private boolean hasTransparency;
	private int numberOfRows;
	private MyFile file;
	private boolean loaded;

	protected Texture() {
		hasTransparency = false;
		numberOfRows = 1;
		loaded = false;
	}

	/**
	 * Creates a new Texture Builder.
	 *
	 * @param file The texture file to be loaded.
	 *
	 * @return A new Texture Builder.
	 */
	public static TextureBuilder newTexture(MyFile file) {
		return new TextureBuilder(file);
	}

	public static Texture getEmptyTexture() {
		return new Texture();
	}

	/**
	 * Deletes the texture.
	 */
	public void delete() {
		loaded = false;
		GlRequestProcessor.sendRequest(new TextureDeleteRequest(textureID));
	}

	public int getTextureID() {
		return textureID;
	}

	public void setTextureID(int id) {
		textureID = id;
		loaded = true;
	}

	public boolean isHasTransparency() {
		return hasTransparency;
	}

	public void setHasTransparency(boolean hasTransparency) {
		this.hasTransparency = hasTransparency;
	}

	public int getNumberOfRows() {
		return numberOfRows;
	}

	public void setNumberOfRows(int numberOfRows) {
		this.numberOfRows = numberOfRows;
	}

	public MyFile getFile() {
		return file;
	}

	public void setFile(MyFile file) {
		this.file = file;
	}

	public boolean isLoaded() {
		return loaded;
	}
}
