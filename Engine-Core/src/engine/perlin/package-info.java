/**
 * Contains classes for randomly generating height values of a 'seed'.
 */
package engine.perlin;