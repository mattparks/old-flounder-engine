package engine.perlin;

import engine.toolbox.Maths;

public class GameSeed {
	private static long seed = Maths.RANDOM.nextInt(1000000);

	/**
	 * @return Returns the current seed being used.
	 */
	public static long getSeed() {
		return GameSeed.seed;
	}
}
