package engine.basics;

import engine.devices.DeviceDisplay;
import engine.devices.DeviceJoystick;
import engine.devices.DeviceKeyboard;
import engine.devices.DeviceMouse;
import engine.devices.DeviceSound;
import engine.guis.GuiManager;
import engine.loaders.Loader;
import engine.processing.RequestProcessor;
import engine.processing.glRequest.GlRequestProcessor;
import engine.profiling.EngineProfiler;
import engine.textures.TextureManager;

import java.awt.*;

/**
 * Deals with much of the initializing, updating, and cleaning up of the engine.
 */
public class EngineCore {
	private static IModule module;

	/**
	 * Carries out any necessary initializations of the engine.
	 *
	 * @param canvas A optional canvas that the display can be attached to.
	 * @param module The engines main module component to run off of.
	 */
	public static void init(Canvas canvas, IModule module) {
		DeviceDisplay.createDeviceDisplay(canvas);
		DeviceSound.createDeviceSound(module.getCamera());
		DeviceJoystick.createJoystickDevice();
		(EngineCore.module = module).init();
		EngineProfiler.init();
	}

	/**
	 * Updates many engine systems before every frame.
	 */
	public static void preRenderUpdate() {
		DeviceJoystick.update();
		DeviceMouse.update();
		DeviceKeyboard.update();

		module.update();

		// if (!gamePaused) {
		// ParticleManager.update(camera);
		// }

		GuiManager.updateGuis();
		DeviceSound.update();
	}

	/**
	 * Renders the engines master renderer.
	 */
	public static void render() {
		module.getRendererMaster().render();
	}

	/**
	 * Updates the display and carries out OpenGL request calls.
	 */
	public static void postRenderUpdate() {
		GlRequestProcessor.dealWithTopRequests();
		DeviceDisplay.update();
	}

	public static IModule getModule() {
		return module;
	}

	public static float getDeltaSeconds() {
		return module.getGame().getDelta();
	}

	/**
	 * Deals with closing down the engine and all necessary systems.
	 */
	public static void close() {
		Loader.cleanUp();
		RequestProcessor.cleanUp();
		GlRequestProcessor.completeAllRequests();
		TextureManager.cleanUp();

		EngineCore.module.cleanUp();
		EngineProfiler.cleanUp();

		DeviceSound.cleanUp();
		DeviceJoystick.cleanUp();
		DeviceKeyboard.cleanUp();
		DeviceMouse.cleanUp();
		DeviceDisplay.cleanUp();
	}
}
