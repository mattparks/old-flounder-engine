package engine.basics;

import java.io.IOException;

/**
 * A module that the Engine will run off of.
 */
public class IModule {
	private IRendererMaster rendererMaster;
	private ICamera camera;
	private IGame game;

	/**
	 * Creates a new engine module to run off of.
	 *
	 * @param rendererMaster The master renderer for the Engine to use when rendering.
	 * @param camera The camera to be used when running the master renderer.
	 */
	public IModule(IRendererMaster rendererMaster, ICamera camera) {
		this.rendererMaster = rendererMaster;
		this.camera = camera;
	}

	/**
	 * Used to add the game object too the module.
	 *
	 * @param game The game object to be added to the module.
	 */
	public void addGame(IGame game) {
		this.game = game;
	}

	/**
	 * Initialises the module and its renderer and game.
	 */
	public void init() {
		try {
			rendererMaster.init();
			game.init();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Updates the game and the camera.
	 */
	public void update() {
		game.update();
		camera.moveCamera(game.getFocusPosition(), game.getFocusRotation(), game.isGamePaused());
	}

	/**
	 * Cleans up all of the modules processes. Should be called when the module closes.
	 */
	public void cleanUp() {
		rendererMaster.cleanUp();
		game.cleanUp();
	}

	public IGame getGame() {
		return game;
	}

	public ICamera getCamera() {
		return camera;
	}

	public void setCamera(ICamera camera) {
		this.camera = camera;
	}

	public IRendererMaster getRendererMaster() {
		return rendererMaster;
	}
}
