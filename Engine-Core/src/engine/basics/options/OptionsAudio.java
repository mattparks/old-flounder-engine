package engine.basics.options;

/**
 * Holds basic audio engine settings.
 */
public class OptionsAudio {
	public static float SOUND_VOLUME = 1.0f;
}
