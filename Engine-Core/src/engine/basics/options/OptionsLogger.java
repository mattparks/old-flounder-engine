package engine.basics.options;

/**
 * Holds basic logger settings.
 */
public class OptionsLogger {
	public static final boolean LOG_TO_CONSOLE = true;
	public static final boolean LOG_TO_FILE = false;
}
