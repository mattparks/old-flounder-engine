package engine.basics.options;

/**
 * Holds basic graphical engine settings.
 */
public class OptionsGraphics {
	public static String DISPLAY_TITLE = "Flounder Demo";
	public static int DISPLAY_WIDTH = 1280;
	public static int DISPLAY_HEIGHT = 720;
	public static boolean V_SYNC = false;
	public static int FPS_CAP = 144;

	public static boolean FULLSCREEN = false;
	public static boolean SHADOWS = true;
	public static boolean ANTI_ALIASING = true;
	public static int MSAA_SAMPLES = 4;
	public static float DISPLAY_PIXEL_RATIO = 1.0f;

	public static int POST_EFFECT = 0;
	public static int POST_EFFECT_MAX = 10;
}
