package engine.basics;

import engine.devices.DeviceDisplay;
import engine.toolbox.Logger;
import engine.toolbox.vector.Vector3f;
import org.lwjgl.glfw.GLFW;

import java.awt.*;
import java.io.IOException;

/**
 * Represents a game loop for the engine to run from.
 */
public abstract class IGame extends Thread {
	private float lastFrameTime = 0.0f;
	private float delta = 0.0f;
	private float time = 0.0f;

	private Vector3f focusPosition;
	private Vector3f focusRotation;
	private boolean gamePaused;
	private float screenBlur;

	/**
	 * Creates a new game loop for the engine to run from.
	 *
	 * @param canvas A optional canvas that the display can be attached to.
	 * @param module The engines module to be used when idealizing the engine.
	 */
	public IGame(Canvas canvas, IModule module) {
		module.addGame(this);
		EngineCore.init(canvas, module);
		this.run();
	}

	@Override
	public void run() {
		long lastTime = System.nanoTime();
		float ns = 1000000000.0f / 60.0f;
		float delta2 = 0.0f;
		float frames = 0.0f;
		float updates = 0.0f;
		long timerStart = System.currentTimeMillis();

		while (DeviceDisplay.isOpen()) {
			long startTime = System.nanoTime();
			delta2 += (startTime - lastTime) / ns;
			lastTime = startTime;
			boolean update = delta2 >= 1.0;

			if (update) {
				EngineCore.preRenderUpdate();
				updates++;
				delta2--;

				// Updates static update times.
				float currentFrameTime = (float) GLFW.glfwGetTime();
				delta = currentFrameTime - lastFrameTime;
				lastFrameTime = currentFrameTime;
				time += delta;
			}

			EngineCore.render();
			frames++;

			if (update) {
				EngineCore.postRenderUpdate();
			}

			if (System.currentTimeMillis() - timerStart > 1000) {
				Logger.log(updates + "ups, " + frames + "fps.");
				timerStart += 1000;
				updates = 0;
				frames = 0;
			}
		}

		cleanUp();
		EngineCore.close();
	}

	/**
	 * Used to initialise the game, can be used to generate the world.
	 *
	 * @throws IOException Thrown if the game could not be initialised correctly.
	 */
	public abstract void init() throws IOException;

	/**
	 * Used to update internal game objects and values. (call the {@link #updateGame(Vector3f, Vector3f, boolean, float) updateGame} method at the end of the function.
	 */
	public abstract void update();

	/**
	 * Cleans up all of the game objects. Should be called when the game closes.
	 */
	public abstract void cleanUp();

	/**
	 * Updates the current engines game settings.
	 *
	 * @param focusPosition The position of the object the camera focuses on.
	 * @param focusRotation The rotation of the object the camera focuses on.
	 * @param gamePaused Is the game currently gamePaused? Used to stop inputs to camera in menus.
	 * @param screenBlur The factor (-1 to 1) by where the screen will be blurred from on the paused screen.
	 */
	public void updateGame(Vector3f focusPosition, Vector3f focusRotation, boolean gamePaused, float screenBlur) {
		this.focusPosition = focusPosition;
		this.focusRotation = focusRotation;
		this.gamePaused = gamePaused;
		this.screenBlur = screenBlur;
	}

	public Vector3f getFocusPosition() {
		return focusPosition;
	}

	public Vector3f getFocusRotation() {
		return focusRotation;
	}

	public boolean isGamePaused() {
		return gamePaused;
	}

	public float getScreenBlur() {
		return screenBlur;
	}

	public float getDelta() {
		return delta;
	}

	public float getTime() {
		return time;
	}
}
