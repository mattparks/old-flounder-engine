package engine.basics;

import engine.profiling.ProfileTimer;
import engine.toolbox.vector.Vector4f;

/**
 * Represents a sub renderer in the engine.
 */
public abstract class IRenderer {
	private ProfileTimer profileTimer;
	private double renderTimeMs;

	/**
	 * Creates a new sub renderer in the engine.
	 */
	public IRenderer() {
		profileTimer = new ProfileTimer();
		renderTimeMs = 0;
	}

	/**
	 * Called when the renderer is needed to be rendered.
	 *
	 * @param clipPlane The current clip plane.
	 * @param camera The camera to be used when rendering.
	 */
	public void render(Vector4f clipPlane, ICamera camera) {
		profileTimer.startInvocation();
		renderObjects(clipPlane, camera);
		profileTimer.stopInvocation();
		renderTimeMs = profileTimer.reset();
	}

	public double getRenderTimeMs() {
		return renderTimeMs;
	}

	/**
	 * An internal render method for Renderers.
	 *
	 * @param clipPlane The current clip plane.
	 * @param camera The camera to be used when rendering.
	 */
	public abstract void renderObjects(Vector4f clipPlane, ICamera camera);

	/**
	 * Cleans up all of the renderers processes.
	 */
	public abstract void cleanUp();
}
