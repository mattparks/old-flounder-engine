package engine.basics;

import engine.toolbox.vector.Matrix4f;

/**
 * The engines main renderer, it organizes render objects passes of subtract renderers.
 */
public abstract class IRendererMaster {
	/**
	 * Initializes the various renderer types and various functionalities.
	 */
	public abstract void init();

	/**
	 * Carries out the rendering of all components.
	 */
	public abstract void render();

	/**
	 * Cleans up all of the render objects processes. Should be called when the game closes.
	 */
	public abstract void cleanUp();

	/**
	 * @return Returns the projection matrix used in the current scene renderObjects.
	 */
	public abstract Matrix4f getProjectionMatrix();
}
