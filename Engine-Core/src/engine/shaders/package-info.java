/**
 * Contains classes for creating shaders and loading {@link engine.shaders.Uniform} variables to {@link engine.shaders.ShaderProgram}'s.
 */
package engine.shaders;