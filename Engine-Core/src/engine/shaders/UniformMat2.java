package engine.shaders;

import engine.toolbox.vector.Matrix2f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL20;

import java.nio.FloatBuffer;

/**
 * Represents a Matrix2F uniform type that can be loaded to the shader.
 */
public class UniformMat2 extends Uniform {
	private Matrix2f currentValue;

	public UniformMat2(String name) {
		super(name);
	}

	/**
	 * Loads a Matrix2F to the uniform if the value already on the GPU is not the same as the new value.
	 *
	 * @param value The new value.
	 */
	public void loadMat2(Matrix2f value) {
		if (value != null && (currentValue == null || !value.equals(currentValue))) {
			FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);
			value.store(matrixBuffer);
			matrixBuffer.flip();
			Matrix2f.load(value, currentValue);
			GL20.glUniformMatrix2fv(super.getLocation(), false, matrixBuffer);
		}
	}
}
