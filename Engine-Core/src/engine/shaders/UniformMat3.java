package engine.shaders;

import engine.toolbox.vector.Matrix3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL20;

import java.nio.FloatBuffer;

/**
 * Represents a Matrix3F uniform type that can be loaded to the shader.
 */
public class UniformMat3 extends Uniform {
	private Matrix3f currentValue;

	public UniformMat3(String name) {
		super(name);
	}

	/**
	 * Loads a Matrix3F to the uniform if the value already on the GPU is not the same as the new value.
	 *
	 * @param value The new value.
	 */
	public void loadMat3(Matrix3f value) {
		if (value != null && (currentValue == null || !value.equals(currentValue))) {
			FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);
			value.store(matrixBuffer);
			matrixBuffer.flip();
			Matrix3f.load(value, currentValue);
			GL20.glUniformMatrix3fv(super.getLocation(), false, matrixBuffer);
		}
	}
}
