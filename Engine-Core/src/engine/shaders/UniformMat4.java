package engine.shaders;

import engine.toolbox.vector.Matrix4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL20;

import java.nio.FloatBuffer;

/**
 * Represents a Matrix4F uniform type that can be loaded to the shader.
 */
public class UniformMat4 extends Uniform {
	private Matrix4f currentValue;

	public UniformMat4(String name) {
		super(name);
	}

	/**
	 * Loads a Matrix4F to the uniform if the value already on the GPU is not the same as the new value.
	 *
	 * @param value The new value.
	 */
	public void loadMat4(Matrix4f value) {
		if (value != null && (currentValue == null || !value.equals(currentValue))) {
			FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);
			value.store(matrixBuffer);
			matrixBuffer.flip();
			Matrix4f.load(value, currentValue);
			GL20.glUniformMatrix4fv(super.getLocation(), false, matrixBuffer);
		}
	}
}
