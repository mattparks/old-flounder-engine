package engine.physics;

import engine.toolbox.vector.Vector3f;

public strictfp class AABBMesh implements MeshCollider {
	private AABB[] mesh;

	public AABBMesh(AABB[] mesh) {
		this.mesh = mesh;

		for (AABB aabb : mesh) {
			aabb.setMaxExtents(aabb.getMaxExtents());
			aabb.setMinExtents(aabb.getMinExtents());
		}
	}

	public AABBMesh move(Vector3f position, Vector3f rotation) {
		AABB[] newMesh = new AABB[mesh.length];

		for (int i = 0; i < mesh.length; i++) {
			newMesh[i] = mesh[i].recalculate(position, rotation);
		}

		return new AABBMesh(newMesh);
	}

	public IntersectData intersects(AABBMesh coll) {
		return this.intersects(coll.getMesh());
	}

	public IntersectData intersects(AABB[] coll) {
		if (coll == null) {
			return new IntersectData(false, 0);
		}

		boolean intersection = false;
		float minDist = Float.POSITIVE_INFINITY;

		for (AABB aabb : coll) {
			IntersectData data = this.intersects(aabb);

			if (data.getDistance() != Float.NEGATIVE_INFINITY) {
				if (!intersection) {
					intersection = data.isIntersection();
				}

				minDist = Math.min(minDist, data.getDistance());
			}
		}

		return new IntersectData(intersection, minDist);
	}

	public AABB[] getMesh() {
		return mesh;
	}

	public void setMesh(AABB[] AABBMesh) {
		mesh = AABBMesh;
	}

	@Override
	public AABB getAABB() {
		throw new UnsupportedOperationException("Not supported.");
	}

	@Override
	public IntersectData intersects(AABB coll) {
		if (coll == null) {
			return new IntersectData(false, 0);
		}

		boolean intersection = false;
		float minDist = Float.POSITIVE_INFINITY;

		for (AABB aabb : mesh) {
			IntersectData data = aabb.intersects(coll);

			if (data.getDistance() != Float.NEGATIVE_INFINITY) {
				if (!intersection) {
					intersection = data.isIntersection();
				}

				minDist = Math.min(minDist, data.getDistance());
			}
		}

		return new IntersectData(intersection, minDist);
	}

	@Override
	public RadialCollider getRadialCollider() {
		throw new UnsupportedOperationException("Not supported.");
	}

	@Override
	public IntersectData intersects(RadialCollider coll) {
		throw new UnsupportedOperationException("Not supported.");
	}

	@Override
	public IntersectData intersects(Collidable coll) {
		throw new UnsupportedOperationException("Not supported.");
	}
}