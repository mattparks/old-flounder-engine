package engine.physics;

import engine.toolbox.vector.Vector3f;

public class PlaneCollider {
	private final float distance;
	private final Vector3f normal;

	public PlaneCollider(Vector3f normal, float distance) {
		this.normal = normal;
		this.distance = distance;
	}

	public PlaneCollider normalized() {
		return new PlaneCollider(new Vector3f(normal.getX() / normal.length(), normal.getY() / normal.length(), normal.getZ() / normal.length()), distance / normal.length());
	}

	public IntersectData intersect(RadialCollider coll) {
		final float distFromColl = Math.abs(Vector3f.dot(getNormal(), coll.getPosition()) + distance) - coll.getRadius();
		return new IntersectData(distFromColl < 0, distFromColl);
	}

	public Vector3f getNormal() {
		return normal;
	}

	public float getDistance() {
		return distance;
	}
}
