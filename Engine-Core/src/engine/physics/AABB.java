package engine.physics;

import engine.toolbox.Maths;
import engine.toolbox.vector.Vector3f;

import java.util.Objects;

public strictfp class AABB {
	private Vector3f minExtents;
	private Vector3f maxExtents;

	public AABB() {
		this(new Vector3f(), new Vector3f());
	}

	public AABB(Vector3f minExtents, Vector3f maxExtents) {
		this.minExtents = minExtents;
		this.maxExtents = maxExtents;
	}

	/**
	 * Creates a new AABB equivalent to this, scaled away from the center origin.
	 *
	 * @param scale Amount to scale up the AABB.
	 *
	 * @return A new AABB, scaled by the specified amounts.
	 */
	public AABB scale(Vector3f scale) {
		return new AABB(new Vector3f(minExtents.getX() * scale.getX(), minExtents.getY() * scale.getY(), minExtents.getZ() * scale.getZ()), new Vector3f(maxExtents.getX() * scale.getX(), maxExtents.getY() * scale.getY(), maxExtents.getZ() * scale.getZ()));
	}

	/**
	 * Creates a new AABB equivalent to this, but scaled away from the origin by a certain amount.
	 *
	 * @param expand Amount to scale up the AABB.
	 *
	 * @return A new AABB, scaled by the specified amounts.
	 */
	public AABB expand(Vector3f expand) {
		return new AABB(new Vector3f(minExtents.getX() - expand.getX(), minExtents.getY() - expand.getY(), minExtents.getZ() - expand.getZ()), new Vector3f(maxExtents.getX() + expand.getX(), maxExtents.getY() + expand.getY(), maxExtents.getZ() + expand.getZ()));
	}

	/**
	 * Creates an AABB that bounds both this AABB and another AABB.
	 *
	 * @param other The other AABB being bounded.
	 *
	 * @return An AABB that bounds both this AABB and {@code other}.
	 */
	public AABB combine(AABB other) {
		float newMinX = Math.min(minExtents.getX(), other.getMinExtents().getX());
		float newMinY = Math.min(minExtents.getY(), other.getMinExtents().getY());
		float newMinZ = Math.min(minExtents.getZ(), other.getMinExtents().getZ());
		float newMaxX = Math.max(maxExtents.getX(), other.getMaxExtents().getX());
		float newMaxY = Math.max(maxExtents.getY(), other.getMaxExtents().getY());
		float newMaxZ = Math.max(maxExtents.getZ(), other.getMaxExtents().getZ());
		return new AABB(new Vector3f(newMinX, newMinY, newMinZ), new Vector3f(newMaxX, newMaxY, newMaxZ));
	}

	public Vector3f getMinExtents() {
		return minExtents;
	}

	public void setMinExtents(Vector3f minExtents) {
		this.minExtents = minExtents;
	}

	public Vector3f getMaxExtents() {
		return maxExtents;
	}

	public void setMaxExtents(Vector3f maxExtents) {
		this.maxExtents = maxExtents;
	}

	/**
	 * Creates a new AABB equivalent to this, but stretched by a certain amount.
	 *
	 * @param stretch The amount to stretch.
	 *
	 * @return A new AABB, stretched by the specified amounts.
	 */
	public AABB stretch(Vector3f stretch) {
		float newMinX, newMaxX, newMinY, newMaxY, newMinZ, newMaxZ;

		if (stretch.getX() < 0) {
			newMinX = minExtents.getX() + stretch.getX();
			newMaxX = maxExtents.getX();
		} else {
			newMinX = minExtents.getX();
			newMaxX = maxExtents.getX() + stretch.getX();
		}

		if (stretch.getY() < 0) {
			newMinY = minExtents.getY() + stretch.getY();
			newMaxY = maxExtents.getY();
		} else {
			newMinY = minExtents.getY();
			newMaxY = maxExtents.getY() + stretch.getY();
		}

		if (stretch.getZ() < 0) {
			newMinZ = minExtents.getZ() + stretch.getZ();
			newMaxZ = maxExtents.getZ();
		} else {
			newMinZ = minExtents.getZ();
			newMaxZ = maxExtents.getZ() + stretch.getZ();
		}

		return new AABB(new Vector3f(newMinX, newMinY, newMinZ), new Vector3f(newMaxX, newMaxY, newMaxZ));
	}

	/**
	 * Creates an AABB equivalent to this, but in a new position and rotation.
	 *
	 * @param position The amount to move.
	 * @param rotation The amount to rotate.
	 *
	 * @return An AABB equivalent to this, but in a new position.
	 */
	public AABB recalculate(Vector3f position, Vector3f rotation) {
		// Maths.rotateVector(minExtents, rotation.getX(), rotation.getY(), rotation.getZ())
		// Maths.rotateVector(maxExtents, rotation.getX(), rotation.getY(), rotation.getZ())
		return new AABB(Vector3f.add(minExtents, position, null), Vector3f.add(maxExtents, position, null));
	}

	public IntersectData intersects(AABB other) throws IllegalArgumentException {
		if (other == null) {
			throw new IllegalArgumentException("Null AABB collider.");
		} else if (equals(other)) {
			return new IntersectData(true, 0);
		}

		final float maxDist = Maths.max(Maths.max(new Vector3f(getMinExtents().getX() - other.getMaxExtents().getX(), getMinExtents().getY() - other.getMaxExtents().getY(), getMinExtents().getZ() - other.getMaxExtents().getZ()), new Vector3f(other.getMinExtents().getX() - getMaxExtents().getX(), other.getMinExtents().getY() - getMaxExtents().getY(), other.getMinExtents().getZ() - getMaxExtents().getZ())));

		return new IntersectData(maxDist < 0, maxDist);
	}

	public boolean contains(Vector3f point) {
		if (point.getX() > maxExtents.getX()) {
			return false;
		} else if (point.getX() < minExtents.getX()) {
			return false;
		} else if (point.getY() > maxExtents.getY()) {
			return false;
		} else if (point.getY() < minExtents.getY()) {
			return false;
		} else if (point.getZ() > maxExtents.getZ()) {
			return false;
		} else if (point.getZ() < minExtents.getZ()) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 79 * hash + Objects.hashCode(minExtents);
		hash = 79 * hash + Objects.hashCode(maxExtents);
		return hash;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null) {
			return false;
		} else if (super.getClass() != obj.getClass()) {
			return false;
		}

		final AABB that = (AABB) obj;

		if (!Objects.equals(minExtents, that.minExtents)) {
			return false;
		} else if (!Objects.equals(maxExtents, that.maxExtents)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AABB{" + "minExtents=" + minExtents + ", maxExtents=" + maxExtents + '}';
	}
}