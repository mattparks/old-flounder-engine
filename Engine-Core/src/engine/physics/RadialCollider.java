package engine.physics;

import engine.toolbox.Maths;
import engine.toolbox.vector.Vector3f;

import java.util.Objects;

public class RadialCollider {
	private float radiusSq;
	private Vector3f position;

	public RadialCollider() {
		this(0, null);
	}

	public RadialCollider(float radiusSq, Vector3f position) {
		this.radiusSq = radiusSq;
		this.position = position;
	}

	public IntersectData intersects(RadialCollider coll) {
		if (coll == null) {
			return new IntersectData(false, 0);
		} else if (equals(coll)) {
			return new IntersectData(true, 0);
		}

		float distSq = Maths.getDistanceSquared(getPosition(), coll.getPosition());
		return new IntersectData(distSq <= Math.min(getRadiusSq(), coll.getRadiusSq()), (float) java.lang.Math.sqrt(distSq));
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public float getRadiusSq() {
		return radiusSq;
	}

	public void setRadiusSq(float radiusSq) {
		this.radiusSq = radiusSq;
	}

	public float getRadius() {
		return Maths.squared(radiusSq);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 37 * hash + Float.floatToIntBits(radiusSq);
		hash = 37 * hash + Objects.hashCode(position);
		return hash;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null) {
			return false;
		} else if (super.getClass() != obj.getClass()) {
			return false;
		}

		final RadialCollider other = (RadialCollider) obj;

		if (Float.floatToIntBits(radiusSq) != Float.floatToIntBits(other.radiusSq)) {
			return false;
		} else if (!Objects.equals(position, other.position)) {
			return false;
		}

		return true;
	}
}
