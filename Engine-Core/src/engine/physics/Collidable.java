package engine.physics;

public interface Collidable {
	AABB getAABB();

	IntersectData intersects(AABB coll);

	RadialCollider getRadialCollider();

	IntersectData intersects(RadialCollider coll);

	IntersectData intersects(Collidable coll);
}