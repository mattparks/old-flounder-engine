package engine.sounds;

import engine.devices.DeviceSound;
import engine.toolbox.Maths;
import engine.toolbox.vector.Vector3f;
import engine.visual.fxDrivers.SmoothFloat;

import java.util.List;

public class AmbientNode {
	private static final float RADIUS_CHANGE_AGIL = 0.5f;
	private static final float RANGE_THRESHOLD = 1.2f;

	private Vector3f position;
	private SmoothFloat innerRadius;
	private SmoothFloat fadeOutRadius;
	private List<Sound> sounds;

	private float volume;
	private boolean active;
	private AudioController controller;
	private Sound lastPlayed;

	public AmbientNode(Vector3f position, float innerRange, float fadeOutRange, List<Sound> sounds) {
		this.position = position;
		innerRadius = new SmoothFloat(innerRange, RADIUS_CHANGE_AGIL);
		fadeOutRadius = new SmoothFloat(fadeOutRange, RADIUS_CHANGE_AGIL);
		this.sounds = sounds;

		volume = 1.0f;
		active = false;
		controller = null;
		lastPlayed = null;
	}

	public void update() {
		updateValues();
		float distance = getDistanceFromListener();

		if (!active && distance <= getRange()) {
			playNewSound();
		} else if (active) {
			updateActiveNode(distance);
		}
	}

	private void updateValues() {
		innerRadius.update();
		fadeOutRadius.update();
	}

	private float getDistanceFromListener() {
		Vector3f listenerPos = DeviceSound.getListener().getPosition();
		return Vector3f.subtract(listenerPos, position, null).length();
	}

	public float getRange() {
		return innerRadius.get() + fadeOutRadius.get();
	}

	private void playNewSound() {
		Sound sound = chooseNextSound();
		PlayRequest request = PlayRequest.new3dSoundPlayRequest(sound, volume, position, innerRadius.get(), getRange());
		controller = DeviceSound.play3DSound(request);
		active = controller != null;
	}

	private void updateActiveNode(float distance) {
		if (distance >= getRange() * RANGE_THRESHOLD) {
			controller.stop();
			active = false;
		} else {
			boolean stillPlaying = controller.update();

			if (!stillPlaying) {
				playNewSound();
			}
		}
	}

	private Sound chooseNextSound() {
		int index = Maths.RANDOM.nextInt(sounds.size());

		if (sounds.size() > 1) {
			index = (index + 1) % sounds.size();
		}

		Sound sound = sounds.get(index);
		lastPlayed = sound;
		return sound;
	}

	public void setVolume(float targetVolume) {
		volume = targetVolume;
	}

	public void setRanges(float innerRange, float fadeOutRange) {
		innerRadius.set(innerRange);
		fadeOutRadius.set(fadeOutRange);
	}

	public void setSounds(List<Sound> sounds) {
		this.sounds = sounds;
		if (controller != null) {
			controller.fadeOut();
		}
	}

	public void setSound(Sound sound) {
		sounds.clear();
		sounds.add(sound);

		if (controller != null) {
			controller.fadeOut();
		}
	}

	public void addSound(Sound sound) {
		sounds.add(sound);
	}

	public void removeSound(Sound sound) {
		sounds.remove(sound);
	}
}
