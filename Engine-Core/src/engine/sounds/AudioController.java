package engine.sounds;

import engine.basics.EngineCore;
import engine.toolbox.vector.Vector3f;

public class AudioController {
	private static final float FADE_TIME = 2.0f;

	private SoundSource source;
	private boolean active;
	private boolean fading;
	private float finalVolume;
	private float fadeFactor;

	protected AudioController(SoundSource source) {
		this.source = source;
		active = true;
		fading = false;
		fadeFactor = 1.0f;
	}

	protected void stop() {
		if (active) {
			source.stop();
		}
	}

	protected boolean update() {
		if (active) {
			updateActiveController();
		}

		return active;
	}

	private void updateActiveController() {
		if (!source.isPlaying()) {
			active = false;
		} else if (fading) {
			updateFadingOut();
		}
	}

	private void updateFadingOut() {
		fadeFactor -= EngineCore.getDeltaSeconds() / FADE_TIME;
		source.setVolume(finalVolume * fadeFactor);

		if (fadeFactor <= 0.0f) {
			source.stop();
		}
	}

	protected void setPosition(Vector3f position) {
		if (active) {
			source.setPosition(position);
		}
	}

	protected void fadeOut() {
		fading = true;
		finalVolume = source.getVolume();
	}

	protected float getVolume() {
		return source.getVolume();
	}

	protected void setInactive() {
		active = false;
	}
}
