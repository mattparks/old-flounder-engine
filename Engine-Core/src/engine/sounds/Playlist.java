package engine.sounds;

import engine.toolbox.Maths;

import java.util.ArrayList;
import java.util.List;

public class Playlist {
	private List<Sound> musics = new ArrayList<>();

	public void addMusic(Sound music) {
		musics.add(music);
	}

	public void clear() {
		musics.clear();
	}

	protected List<Sound> getShuffledMusicList(Sound previouslyPlayed) {
		ArrayList<Sound> tempList = new ArrayList<>();
		tempList.addAll(musics);
		ArrayList<Sound> shuffledList = new ArrayList<>();

		while (!tempList.isEmpty()) {
			shuffledList.add(removeRandomTrackFromList(tempList));
		}

		ensurePreviousTrackNotRepeated(shuffledList, previouslyPlayed);
		return shuffledList;
	}

	private Sound removeRandomTrackFromList(List<Sound> listOfMusic) {
		int index = Maths.RANDOM.nextInt(listOfMusic.size());
		return listOfMusic.remove(index);
	}

	private void ensurePreviousTrackNotRepeated(List<Sound> newPlaylist, Sound previouslyPlayed) {
		if (!newPlaylist.isEmpty() && newPlaylist.get(0) == previouslyPlayed) {
			Sound track = newPlaylist.remove(0);
			newPlaylist.add(track);
		}
	}

	protected List<Sound> getOrderedTracks() {
		return musics;
	}
}
