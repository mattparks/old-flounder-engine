package engine.sounds;

public class SoundEffect {
	private Sound sound;
	private float range;

	public SoundEffect(Sound sound, float range) {
		this.sound = sound;
		this.range = range;
	}

	protected Sound getSound() {
		return sound;
	}

	protected float getRange() {
		return range;
	}
}
