package engine.sounds;

import engine.processing.RequestProcessor;
import engine.processing.glRequest.GlRequest;
import engine.processing.glRequest.GlRequestProcessor;
import engine.toolbox.resources.MyFile;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

public class Sound {
	public static Map<String, SoftReference<Sound>> loaded = new HashMap<>();

	private int bufferID;
	private MyFile file;
	private float volume = 1.0f;
	private boolean isLoaded = false;

	private Sound(MyFile file, float volume) {
		this.file = file;
		this.volume = volume;
	}

	public static Sound loadSoundNow(MyFile soundFile, float volume) {
		SoftReference<Sound> ref = loaded.get(soundFile.getPath());
		Sound data = ref == null ? null : ref.get();

		if (data == null) {
			System.out.println(soundFile.getPath() + " is being loaded into builder memory!");
			loaded.remove(soundFile.getPath());
			data = new Sound(soundFile, volume);
			new SoundLoadRequest(data).doResourceRequest();
			loaded.put(soundFile.getPath(), new SoftReference<>(data));
		}

		return data;
	}

	public static Sound loadSoundInBackground(MyFile soundFile, float volume) {
		SoftReference<Sound> ref = loaded.get(soundFile.getPath());
		Sound data = ref == null ? null : ref.get();

		if (data == null) {
			loaded.remove(soundFile.getPath());
			data = new Sound(soundFile, volume);
			RequestProcessor.sendRequest(new SoundLoadRequest(data));
			loaded.put(soundFile.getPath(), new SoftReference<>(data));
		}

		return data;
	}

	public void delete() {
		if (!isLoaded) {
			return;
		}

		GlRequestProcessor.sendRequest(new GlRequest() {
			@Override
			public void executeGlRequest() {
				SoundLoader.removeSoundFromMemory(bufferID);
			}

			@Override
			public float getTimeRequired() {
				return 50.0f;
			}
		});

		isLoaded = false;
	}

	protected int getBufferID() {
		return bufferID;
	}

	protected void setBuffer(int buffer) {
		bufferID = buffer;
		isLoaded = true;
	}

	protected MyFile getSoundFile() {
		return file;
	}

	protected float getVolume() {
		return volume;
	}

	public boolean isLoaded() {
		return isLoaded;
	}
}
