package engine.sounds;

import engine.toolbox.resources.MyFile;
import org.lwjgl.openal.AL10;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SoundLoader {
	private static List<Integer> buffers = new ArrayList<>();

	protected static int loadSoundData(WaveData waveFile) {
		int bufferID = AL10.alGenBuffers();
		buffers.add(bufferID);
		AL10.alBufferData(bufferID, waveFile.format, waveFile.data, waveFile.sampleRate);
		waveFile.dispose();

		if (AL10.alGetError() != AL10.AL_FALSE) {
			System.err.println("Problem loading sound data.");
		}

		return bufferID;
	}

	protected static WaveData decodeSoundFile(MyFile wavFile) {
		InputStream stream = wavFile.getInputStream();
		WaveData waveFile = WaveData.create(wavFile.getPath().substring(1));

		try {
			stream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return waveFile;
	}

	protected static void removeSoundFromMemory(Integer bufferID) {
		buffers.remove(bufferID);
		AL10.alDeleteBuffers(bufferID);
	}

	public static void cleanUp() {
		buffers.iterator().forEachRemaining(AL10::alDeleteBuffers);
	}
}
