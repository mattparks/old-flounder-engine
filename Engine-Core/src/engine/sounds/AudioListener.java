package engine.sounds;

import engine.toolbox.vector.Vector3f;

public interface AudioListener {
	Vector3f getPosition();
}
