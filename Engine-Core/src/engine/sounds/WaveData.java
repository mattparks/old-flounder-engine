package engine.sounds;

import engine.toolbox.Logger;
import org.lwjgl.openal.AL10;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;

/**
 * Utility class for loading wave files.
 */
public class WaveData {
	/**
	 * Actual wave data.
	 */
	public final ByteBuffer data;

	/**
	 * Format type of data.
	 */
	public final int format;

	/**
	 * Sample rate of data.
	 */
	public final int sampleRate;

	/**
	 * Creates a new WaveData.
	 *
	 * @param data Actual wave data.
	 * @param format Format of wave data.
	 * @param sampleRate Sample rate of data.
	 */
	private WaveData(ByteBuffer data, int format, int sampleRate) {
		this.data = data;
		this.format = format;
		this.sampleRate = sampleRate;
	}

	/**
	 * Creates a WaveData container from the specified in the classpath.
	 *
	 * @param path Path to file (relative, and in classpath).
	 *
	 * @return WaveData Containing data, or null if a failure occurred.
	 */
	public static WaveData create(String path) {
		return create(WaveData.class.getClassLoader().getResource(path));
	}

	/**
	 * Creates a WaveData container from the specified url.
	 *
	 * @param path URL to file.
	 *
	 * @return WaveData containing data, or null if a failure occurred.
	 */
	public static WaveData create(URL path) {
		try {
			return create(AudioSystem.getAudioInputStream(new BufferedInputStream(path.openStream())));
		} catch (Exception e) {
			Logger.error("Unable to create WaveData from: " + path);
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Creates a WaveData container from the specified stream.
	 *
	 * @param ais AudioInputStream to read from.
	 *
	 * @return WaveData containing data, or null if a failure occurred.
	 */
	public static WaveData create(AudioInputStream ais) {
		// Get format of data.
		AudioFormat audioformat = ais.getFormat();

		// Get channels.
		int channels;

		if (audioformat.getChannels() == 1) {
			if (audioformat.getSampleSizeInBits() == 8) {
				channels = AL10.AL_FORMAT_MONO8;
			} else if (audioformat.getSampleSizeInBits() == 16) {
				channels = AL10.AL_FORMAT_MONO16;
			} else {
				throw new RuntimeException("Illegal sample size.");
			}
		} else if (audioformat.getChannels() == 2) {
			if (audioformat.getSampleSizeInBits() == 8) {
				channels = AL10.AL_FORMAT_STEREO8;
			} else if (audioformat.getSampleSizeInBits() == 16) {
				channels = AL10.AL_FORMAT_STEREO16;
			} else {
				throw new RuntimeException("Illegal sample size.");
			}
		} else {
			throw new RuntimeException("Only mono or stereo is supported.");
		}

		// Read data into buffer.
		byte[] buf = new byte[audioformat.getChannels() * (int) ais.getFrameLength() * audioformat.getSampleSizeInBits() / 8];
		int total = 0;
		int read;

		try {
			while ((read = ais.read(buf, total, buf.length - total)) != -1 && total < buf.length) {
				total += read;
			}
		} catch (IOException ioe) {
			return null;
		}

		// Insert data into bytebuffer.
		ByteBuffer buffer = convertAudioBytes(buf, audioformat.getSampleSizeInBits() == 16);

		// ByteBuffer buffer = ByteBuffer.allocateDirect(buf.length);
		// buffer.put(buf);
		// buffer.rewind();

		// Create our result.
		WaveData wavedata = new WaveData(buffer, channels, (int) audioformat.getSampleRate());

		// Close stream.
		try {
			ais.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return wavedata;
	}

	/**
	 * Convert the audio bytes into the stream.
	 *
	 * @param audioBytes The audio bytes.
	 * @param twoBytesData True if we using double byte data.
	 *
	 * @return The byte buffer of data.
	 */
	private static ByteBuffer convertAudioBytes(byte[] audioBytes, boolean twoBytesData) {
		ByteBuffer dest = ByteBuffer.allocateDirect(audioBytes.length);
		dest.order(ByteOrder.nativeOrder());
		ByteBuffer src = ByteBuffer.wrap(audioBytes);
		src.order(ByteOrder.LITTLE_ENDIAN);

		if (twoBytesData) {
			ShortBuffer dest_short = dest.asShortBuffer();
			ShortBuffer src_short = src.asShortBuffer();

			while (src_short.hasRemaining()) {
				dest_short.put(src_short.get());
			}
		} else {
			while (src.hasRemaining()) {
				dest.put(src.get());
			}
		}

		dest.rewind();
		return dest;
	}

	/**
	 * Creates a WaveData container from the specified InputStream.
	 *
	 * @param is InputStream to read from.
	 *
	 * @return WaveData containing data, or null if a failure occurred.
	 */
	public static WaveData create(InputStream is) {
		try {
			return create(AudioSystem.getAudioInputStream(is));
		} catch (Exception e) {
			Logger.error("Unable to WaveData create from InputStream!");
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Creates a WaveData container from the specified ByteBuffer. If the buffer is backed by an array, it will be used directly, else the contents of the buffer will be copied using get(byte[]).
	 *
	 * @param buffer ByteBuffer containing sound file.
	 *
	 * @return WaveData containing data, or null if a failure occurred.
	 */
	public static WaveData create(ByteBuffer buffer) {
		try {
			byte[] bytes;

			if (buffer.hasArray()) {
				bytes = buffer.array();
			} else {
				bytes = new byte[buffer.capacity()];
				buffer.get(bytes);
			}
			return create(bytes);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Creates a WaveData container from the specified bytes.
	 *
	 * @param buffer Array of bytes containing the complete wave file.
	 *
	 * @return WaveData containing data, or null if a failure occurred.
	 */
	public static WaveData create(byte[] buffer) {
		try {
			return create(AudioSystem.getAudioInputStream(new BufferedInputStream(new ByteArrayInputStream(buffer))));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Disposes the wave data.
	 */
	public void dispose() {
		data.clear();
	}
}
