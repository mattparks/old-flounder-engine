package engine.sounds;

import engine.basics.EngineCore;

import java.util.ArrayList;
import java.util.List;

public class MusicPlayer {
	private static final float FADE_TIME = 2.0f;
	private float musicVolume = 1.0f;
	private SoundSource source = new SoundSource();
	private Playlist currentPlaylist;
	private List<Sound> musicQueue = new ArrayList<>();
	private Sound currentlyPlaying = null;
	private boolean fadeOut = false;
	private float fadeFactor = 1.0f;
	private float finalVolume;
	private boolean shuffle;

	public MusicPlayer() {
		source.loop(false);
		source.setUndiminishing();
	}

	public void playEventMusic(Sound music, boolean fadeOutPrevious) {
		musicQueue.add(0, music);

		if (fadeOutPrevious) {
			fadeOutCurrentTrack();
		} else {
			source.stop();
		}
	}

	private void fadeOutCurrentTrack() {
		if (currentlyPlaying != null) {
			fadeOut = true;
			finalVolume = source.getVolume();
		}
	}

	public float getVolume() {
		return musicVolume;
	}

	public void setVolume(float volume) {
		musicVolume = volume;

		if (currentlyPlaying != null) {
			source.setVolume(musicVolume * currentlyPlaying.getVolume());
		}
	}

	public void playMusicPlaylist(Playlist playlist, boolean shuffle) {
		this.shuffle = shuffle;
		currentPlaylist = playlist;
		fadeOutCurrentTrack();
		musicQueue.clear();
		fillQueue();
	}

	private void fillQueue() {
		if (currentPlaylist == null) {
			return;
		}

		if (shuffle) {
			musicQueue.addAll(currentPlaylist.getShuffledMusicList(currentlyPlaying));
		} else {
			musicQueue.addAll(currentPlaylist.getOrderedTracks());
		}
	}

	public Sound getCurrentlyPlaying() {
		return currentlyPlaying;
	}

	public void update() {
		if (fadeOut) {
			updateFadeOut();
		}

		if (!source.isPlaying() && !musicQueue.isEmpty()) {
			playNextTrack();
		}
	}

	private void updateFadeOut() {
		fadeFactor -= EngineCore.getDeltaSeconds() / FADE_TIME;
		source.setVolume(finalVolume * fadeFactor);

		if (fadeFactor <= 0.0f) {
			fadeOut = false;
			fadeFactor = 1.0f;
			source.stop();
		}
	}

	private void playNextTrack() {
		Sound nextTrack = musicQueue.remove(0);

		if (musicQueue.isEmpty()) {
			fillQueue();
		}

		currentlyPlaying = nextTrack;
		source.setVolume(musicVolume * currentlyPlaying.getVolume());
		source.playSound(currentlyPlaying);
	}

	public void cleanUp() {
		source.delete();
	}
}
