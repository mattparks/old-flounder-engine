package engine.sounds;

import engine.basics.options.OptionsAudio;
import engine.toolbox.vector.Vector3f;

public class PlayRequest {
	private Vector3f position = new Vector3f(0.0f, 0.0f, 0.0f);
	private float innerRange = 1.0f;
	private float outerRange = 1.0f;
	private boolean systemSound = true;
	private boolean loop = false;
	private float volume = 1.0f;
	private Sound sound;

	private PlayRequest(Sound sound, float volume) {
		this.sound = sound;
		this.volume = volume * OptionsAudio.SOUND_VOLUME;
	}

	public static PlayRequest newSystemPlayRequest(Sound systemSound) {
		return new PlayRequest(systemSound, 1.0f);
	}

	protected static PlayRequest new3dSoundPlayRequest(Sound sound, float volume, Vector3f position, float innerRange, float outerRange) {
		PlayRequest request = new PlayRequest(sound, volume);
		request.systemSound = false;
		request.innerRange = innerRange < 1.0f ? 1.0f : innerRange;
		request.outerRange = outerRange;
		request.position = position;
		return request;
	}

	protected Vector3f getPosition() {
		return position;
	}

	protected float getInnerRange() {
		return innerRange;
	}

	protected float getOuterRange() {
		return outerRange;
	}

	protected boolean isSystemSound() {
		return systemSound;
	}

	protected float getVolume() {
		return volume;
	}

	public Sound getSound() {
		return sound;
	}

	protected boolean isLooping() {
		return loop;
	}

	protected void setLooping(boolean loop) {
		this.loop = loop;
	}
}
