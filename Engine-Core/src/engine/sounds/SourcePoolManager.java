package engine.sounds;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SourcePoolManager {
	private static final int NUM_SOURCES = 20;

	private List<SoundSource> sourcePool = new ArrayList<>();
	private List<SoundSource> usedSources = new ArrayList<>();

	public SourcePoolManager() {
		for (int i = 0; i < NUM_SOURCES; i++) {
			sourcePool.add(new SoundSource());
		}
	}

	public AudioController play(PlayRequest playRequest) {
		if (!sourcePool.isEmpty()) {
			SoundSource source = sourcePool.remove(0);
			usedSources.add(source);
			source.setPosition(playRequest.getPosition());
			source.loop(playRequest.isLooping());

			if (!playRequest.isSystemSound()) {
				source.setRanges(playRequest.getInnerRange(), playRequest.getOuterRange());
			} else {
				source.setUndiminishing();
			}

			Sound sound = playRequest.getSound();
			source.setVolume(playRequest.getVolume() * sound.getVolume());
			return source.playSound(sound);
		}

		return null;
	}

	public void update() {
		Iterator<SoundSource> iterator = usedSources.iterator();

		while (iterator.hasNext()) {
			SoundSource source = iterator.next();

			if (source.isPlaying()) {
				continue;
			}

			iterator.remove();
			sourcePool.add(source);
		}
	}

	public void cleanUp() {
		sourcePool.forEach(SoundSource::delete);
	}
}
