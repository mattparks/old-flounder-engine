package engine.sounds;

import engine.devices.DeviceSound;
import engine.toolbox.Maths;
import engine.toolbox.vector.Vector3f;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SoundEmitter {
	private static final float RANGE_THRESHOLD = 1.2f;
	private Vector3f position;
	private float volume = 1.0f;
	private Map<SoundEffect, AudioController> playingSounds = new HashMap<>();

	public SoundEmitter(Vector3f position) {
		this.position = position;
	}

	public void update() {
		Iterator<Map.Entry<SoundEffect, AudioController>> iterator = playingSounds.entrySet().iterator();

		while (iterator.hasNext()) {
			if (updateAudioController(iterator.next())) {
				continue;
			}

			iterator.remove();
		}
	}

	private boolean updateAudioController(Map.Entry<SoundEffect, AudioController> entry) {
		AudioController controller = entry.getValue();

		if (!isInRange(entry.getKey())) {
			controller.stop();
		}

		return controller.update();
	}

	private boolean isInRange(SoundEffect soundEffect) {
		float range = soundEffect.getRange() * RANGE_THRESHOLD;
		float rangeSquared = Maths.squared(range);
		float disSquared = Vector3f.subtract(DeviceSound.getListener().getPosition(), position, null).lengthSquared();
		return disSquared < rangeSquared;
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(float x, float y, float z) {
		position.set(x, y, z);

		for (AudioController controller : playingSounds.values()) {
			controller.setPosition(position);
		}
	}

	public float getVolume() {
		return volume;
	}

	public void setVolume(float volume) {
		this.volume = volume;
	}

	public void playSound(SoundEffect soundEffect) {
		if (!soundEffect.getSound().isLoaded() || isPlayingSound(soundEffect) || !isInRange(soundEffect)) {
			return;
		}

		PlayRequest request = PlayRequest.new3dSoundPlayRequest(soundEffect.getSound(), volume, position, 0.0f, soundEffect.getRange());
		AudioController controller = DeviceSound.play3DSound(request);

		if (controller != null) {
			playingSounds.put(soundEffect, controller);
		}
	}

	public boolean isPlayingSound(SoundEffect sound) {
		return playingSounds.containsKey(sound);
	}

	public void silence() {
		playingSounds.values().forEach(AudioController::stop);
		playingSounds.clear();
	}

	public boolean isInUse() {
		return !playingSounds.isEmpty();
	}
}
