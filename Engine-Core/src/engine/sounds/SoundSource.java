package engine.sounds;

import engine.toolbox.vector.Vector3f;
import org.lwjgl.openal.AL10;

public class SoundSource {
	private int sourceID = SoundSource.createSource();
	private float volume = 1.0f;
	private AudioController currentController;

	protected SoundSource() {
		AL10.alSource3f(sourceID, AL10.AL_POSITION, 0.0f, 0.0f, 0.0f);
		AL10.alSource3f(sourceID, AL10.AL_VELOCITY, 1.0f, 0.0f, 0.0f);
		AL10.alSourcef(sourceID, AL10.AL_ROLLOFF_FACTOR, 0.0f);
		AL10.alSourcef(sourceID, AL10.AL_GAIN, volume);
	}

	private static int createSource() {
		int sourceID = AL10.alGenSources();

		if (AL10.alGetError() != AL10.AL_FALSE) {
			System.err.println("Problem creating source!");
		}

		return sourceID;
	}

	protected void setRange(float radius) {
		AL10.alSourcef(sourceID, AL10.AL_REFERENCE_DISTANCE, 1.0f);
		AL10.alSourcef(sourceID, AL10.AL_ROLLOFF_FACTOR, 1.0f);
		AL10.alSourcef(sourceID, AL10.AL_MAX_DISTANCE, radius);
	}

	protected void setUndiminishing() {
		AL10.alSourcef(sourceID, AL10.AL_ROLLOFF_FACTOR, 0.0f);
	}

	protected void setRanges(float primaryRadius, float secondaryRadius) {
		if (primaryRadius < 1.0f) {
			primaryRadius = 1.0f;
		}

		AL10.alSourcef(sourceID, AL10.AL_REFERENCE_DISTANCE, primaryRadius);
		AL10.alSourcef(sourceID, AL10.AL_ROLLOFF_FACTOR, 1.0f);
		AL10.alSourcef(sourceID, AL10.AL_MAX_DISTANCE, secondaryRadius);
	}

	protected float getVolume() {
		return volume;
	}

	protected void setVolume(float newVolume) {
		if (newVolume != volume) {
			AL10.alSourcef(sourceID, AL10.AL_GAIN, newVolume);
			volume = newVolume;
		}
	}

	protected void setPosition(Vector3f position) {
		AL10.alSource3f(sourceID, AL10.AL_POSITION, position.x, position.y, position.z);
	}

	protected void loop(boolean loop) {
		AL10.alSourcei(sourceID, AL10.AL_LOOPING, loop ? 1 : 0);
	}

	protected AudioController playSound(Sound sound) {
		if (!sound.isLoaded()) {
			return null;
		}

		stop();
		AL10.alSourcei(sourceID, AL10.AL_BUFFER, sound.getBufferID());
		AL10.alSourcei(sourceID, AL10.AL_LOOPING, 0);
		AL10.alSourcePlay(sourceID);
		currentController = new AudioController(this);
		return currentController;
	}

	protected void stop() {
		if (isPlaying()) {
			currentController.setInactive();
			AL10.alSourceStop(sourceID);
		}
	}

	public boolean isPlaying() {
		return AL10.alGetSourcei(sourceID, AL10.AL_SOURCE_STATE) == AL10.AL_PLAYING;
	}

	protected void delete() {
		AL10.alDeleteSources(sourceID);
	}
}
