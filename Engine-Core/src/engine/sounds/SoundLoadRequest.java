package engine.sounds;

import engine.processing.ResourceRequest;
import engine.processing.glRequest.GlRequest;
import engine.processing.glRequest.GlRequestProcessor;

public class SoundLoadRequest implements ResourceRequest, GlRequest {
	private Sound sound;
	private WaveData data;

	protected SoundLoadRequest(Sound sound) {
		this.sound = sound;
	}

	@Override
	public void executeGlRequest() {
		int buffer = SoundLoader.loadSoundData(data);
		sound.setBuffer(buffer);
	}

	@Override
	public float getTimeRequired() {
		return 10.0f;
	}

	@Override
	public void doResourceRequest() {
		data = SoundLoader.decodeSoundFile(sound.getSoundFile());
		GlRequestProcessor.sendRequest(this);
	}
}
