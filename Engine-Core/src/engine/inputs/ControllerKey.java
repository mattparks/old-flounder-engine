package engine.inputs;

public class ControllerKey {
	private BaseButton key;
	private boolean wasDown;

	/**
	 * Creates a new control.
	 *
	 * @param key The BaseButton key to be listened too.
	 */
	public ControllerKey(BaseButton key) {
		this.key = key;
	}

	/**
	 * @return Is the key down and was not down before? Key press recognized as one click.
	 */
	public boolean wasPressed() {
		boolean stillDown = wasDown && isDown();
		wasDown = isDown();
		return wasDown == !stillDown;
	}

	/**
	 * @return Is this key down.
	 */
	public boolean isDown() {
		return key.isDown();
	}
}
