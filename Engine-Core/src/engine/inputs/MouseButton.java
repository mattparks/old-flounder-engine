package engine.inputs;

import engine.devices.DeviceMouse;

/**
 * Handles buttons on a mouse.
 */
public class MouseButton extends BaseButton {
	/**
	 * Creates a new MouseButton.
	 *
	 * @param mouseButton The code for the mouse button being checked. Should be one of the IInput.MOUSE_BUTTON values.
	 */
	public MouseButton(int mouseButton) {
		this(new int[]{mouseButton});
	}

	/**
	 * Creates a new MouseButton.
	 *
	 * @param mouseButtons The codes for the mouse buttons being checked. They should be IInput.MOUSE_BUTTON values.
	 */
	public MouseButton(int[] mouseButtons) {
		super(mouseButtons, DeviceMouse::getMouse);
	}
}
