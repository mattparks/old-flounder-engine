package engine.inputs;

import engine.devices.DeviceJoystick;
import engine.toolbox.Maths;

/**
 * Axis from a joystick.
 */
public class JoystickAxis implements IAxis {
	private int joystick;
	private int[] joystickAxes;

	/**
	 * Creates a new JoystickAxis.
	 *
	 * @param joystick The joystick. Should be one of the IInput.JOYSTICK values.
	 * @param joystickAxis The axis on the joystick being checked.
	 */
	public JoystickAxis(int joystick, int joystickAxis) {
		this(joystick, new int[]{joystickAxis});
	}

	/**
	 * Creates a new JoystickAxis.
	 *
	 * @param joystick The joystick. Should be one of the IInput.JOYSTICK values.
	 * @param joystickAxes The axes on the joystick being checked.
	 */
	public JoystickAxis(int joystick, int[] joystickAxes) {
		this.joystick = joystick;
		this.joystickAxes = joystickAxes;
	}

	@Override
	public double getAmount() {
		if (joystickAxes == null || joystick == -1) {
			return 0.0;
		}

		double result = 0.0;

		for (int joystickAxe : joystickAxes) {
			result += DeviceJoystick.getJoystickAxis(joystick, joystickAxe);
		}

		return Maths.clamp(result, -1.0, 1.0);
	}
}
