package engine.inputs;

import engine.devices.DeviceJoystick;

/**
 * Handles buttons on a joystick.
 */
public class JoystickButton extends BaseButton {
	/**
	 * Creates a new JoystickButton.
	 *
	 * @param joystick The joystick. Should be one of the IInput.JOYSTICK values.
	 * @param joystickButton The button on the joystick being checked.
	 */
	public JoystickButton(int joystick, int joystickButton) {
		this(joystick, new int[]{joystickButton});
	}

	/**
	 * Creates a new JoystickButton.
	 *
	 * @param joystick The joystick. Should be one of the IInput.JOYSTICK values.
	 * @param joystickButtons The buttons on the joystick being checked.
	 */
	public JoystickButton(int joystick, int[] joystickButtons) {
		super(joystickButtons, (int code) -> DeviceJoystick.getJoystickButton(joystick, code));
	}
}
