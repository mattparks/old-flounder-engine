package engine.inputs;

import engine.devices.DeviceKeyboard;

/**
 * Handles buttons from a keyboard.
 */
public class KeyButton extends BaseButton {
	/**
	 * Creates a new KeyButton.
	 *
	 * @param keyCode The code for the key being checked. Should be one of the IInput.KEY values.
	 */
	public KeyButton(int keyCode) {
		this(new int[]{keyCode});
	}

	/**
	 * Creates a new KeyButton.
	 *
	 * @param keyCodes The codes for the buttons being checked. They should be IInput.KEY values.
	 */
	public KeyButton(int[] keyCodes) {
		super(keyCodes, DeviceKeyboard::getKey);
	}
}
