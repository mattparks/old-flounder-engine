package engine.inputs;

/**
 * Handles multiple buttons at once.
 */
public class CompoundButton implements IButton {
	private IButton[] buttons;

	/**
	 * Creates a new CompoundButton.
	 *
	 * @param button1 The first button being checked.
	 * @param button2 The second button being checked.
	 */
	public CompoundButton(IButton button1, IButton button2) {
		this(new IButton[]{button1, button2});
	}

	/**
	 * Creates a new CompoundButton.
	 *
	 * @param buttons The list of buttons being checked.
	 */
	public CompoundButton(IButton[] buttons) {
		this.buttons = buttons;
	}

	@Override
	public boolean isDown() {
		for (IButton button : buttons) {
			if (button.isDown()) {
				return true;
			}
		}

		return false;
	}
}
