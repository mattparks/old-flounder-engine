package engine.inputs;

/**
 * Interface for a binary input device.
 */
public interface IButton {
	/**
	 * Returns whether this button is currently pressed.
	 *
	 * @return True if the button is pressed, false otherwise.
	 */
	boolean isDown();
}
