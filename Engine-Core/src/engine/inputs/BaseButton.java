package engine.inputs;

/**
 * Base class for typical buttons.
 */
public abstract class BaseButton implements IButton {
	private int[] codes;
	private Command command;

	/**
	 * Creates a new BaseButton.
	 *
	 * @param codes The list of codes this button is checking.
	 * @param command The method of deciding if a code is down or not in the input system.
	 */
	public BaseButton(int[] codes, Command command) {
		this.codes = codes;
		this.command = command;
	}

	@Override
	public boolean isDown() {
		if (codes == null) {
			return false;
		}

		for (int code : codes) {
			if (command.isDown(code)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Decides whether a certain code is down or not.
	 */
	public interface Command {
		/**
		 * Decides whether a certain code is down or not.
		 *
		 * @param code The button code
		 *
		 * @return True if the button specified by the code is down in the input system, false otherwise.
		 */
		boolean isDown(int code);
	}
}
