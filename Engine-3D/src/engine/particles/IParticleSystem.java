package engine.particles;

/**
 * Represents a particle system that emits engine.particles.
 */
public interface IParticleSystem {
	/**
	 * Updates the system so it can preform updates.
	 */
	void update();
}
