/**
 * Contains classes for updating, managing and rendering particle systems.
 */
package engine.particles;