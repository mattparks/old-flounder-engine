package engine.world;

import engine.basics.ICamera;
import engine.entities.Entity;
import engine.entities.components.LightComponent;
import engine.entities.components.ModelComponent;
import engine.entities.components.SunComponent;
import engine.lights.Attenuation;
import engine.lights.Light;
import engine.models.ModelTextured;
import engine.particles.IParticleSystem;
import engine.space.QuadTree;
import engine.terrains.Terrain;
import engine.toolbox.Maths;
import engine.waters.Water;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Holds information about the engines world.
 */
public class Environment {
	private static QuadTree<Entity> genericEntityTree = new QuadTree<>();
	private static List<IParticleSystem> particleSystems = new ArrayList<>();

	private static Fog fog;
	private static Entity sun;

	/**
	 * Initializes the start game environment.
	 *
	 * @param fog The fog to be used in the world.
	 * @param sun The sun entity to be used in the world.
	 */
	public static void init(Fog fog, Entity sun) {
		Environment.fog = fog;
		Environment.sun = sun;
		World.init();
	}

	/**
	 * Updates all engine.entities in the viewable terrain range.
	 */
	public static void updateEntities() {
		genericEntityTree.getAll().forEach(Entity::update);

		for (Terrain t : World.getTerrainTree().getAll()) {
			for (int i = 0; i < t.getEntityQuadtree().getSize(); i++) {
				Entity e = t.getEntityQuadtree().get(i);
				e.update();
			}
		}
	}

	/**
	 * Updates all particle systems.
	 */
	public static void updateParticleSystems() {
		particleSystems.forEach(IParticleSystem::update);
	}

	/**
	 * Creates a list of renderable engine.terrains.
	 *
	 * @param camera The games camera to query engine.terrains from.
	 *
	 * @return Returns a list of engine.terrains.
	 */
	public static List<Terrain> getTerrainObjects(ICamera camera) {
		return World.getTerrainTree().queryInFrustum(camera.getViewFrustum());
	}

	/**
	 * Creates a list of renderable engine.waters.
	 *
	 * @param camera The games camera to query engine.waters from.
	 *
	 * @return Returns a list of engine.waters.
	 */
	public static List<Water> getWaterObjects(ICamera camera) {
		return World.getWaterTree().queryInFrustum(camera.getViewFrustum());
	}

	/**
	 * Creates a list of renderable engine.entities.
	 *
	 * @param camera The games camera to query engine.entities from.
	 *
	 * @return Returns a HashMap of sorted Textured Models and engine.entities.
	 */
	public static Map<ModelTextured, List<Entity>> getEntityObjects(ICamera camera) {
		Map<ModelTextured, List<Entity>> entities = new HashMap<>();
		List<Entity> quadSetList = genericEntityTree.queryInFrustum(camera.getViewFrustum());

		for (Terrain t : getTerrainObjects(camera)) {
			quadSetList.addAll(t.getEntityQuadtree().queryInFrustum(camera.getViewFrustum()));
		}

		for (Entity e : quadSetList) {
			ModelComponent mc = (ModelComponent) e.getComponent(ModelComponent.ID);

			if (mc != null) {
				ModelTextured entityModel = mc.getModel();
				List<Entity> batch = entities.get(entityModel);

				if (batch != null) {
					batch.add(e);
				} else {
					List<Entity> newBatch = new ArrayList<>();
					newBatch.add(e);
					entities.put(entityModel, newBatch);
				}
			}
		}

		return entities;
	}

	/**
	 * Creates a list of viewable engine.lights.
	 *
	 * @param camera The games camera to query engine.lights from.
	 *
	 * @return Returns a list of engine.lights.
	 */
	public static List<Light> getLightObjects(ICamera camera) {
		List<Light> lights = new ArrayList<>();
		List<Entity> quadSetList = genericEntityTree.getAll();

		for (Terrain t : getTerrainObjects(camera)) {
			quadSetList.addAll(t.getEntityQuadtree().getAll());
		}

		for (Entity e : quadSetList) {
			LightComponent lc = (LightComponent) e.getComponent(LightComponent.ID);

			if (lc != null) {
				Attenuation attenuation = lc.getLight().getAttenuation();
				float cameraDistance = Maths.getDistanceSquared(lc.getLight().getPosition(), camera.getPosition());
				float factor = attenuation.getConstant() + (attenuation.getLinear() * cameraDistance) + (attenuation.getExponent() * (cameraDistance * cameraDistance));

				// System.out.println(factor);

				lights.add(lc.getLight());
			}
		}

		// sortLights(engine.lights, camera, 4);

		return lights;
	}

	/*private static void sortLights(List<LightComponent> engine.lights, ICamera camera, int maxLights) {
		Comparator<LightComponent> lightComparator = (LightComponent left, LightComponent right) -> {
			int distanceResult = Maths.getDistanceSquared(left.getLight().getPosition(), camera.getPosition()) < Maths.getDistanceSquared(right.getLight().getPosition(), camera.getPosition()) ? -1 : 1;

			if (left.getEntity().getComponent(SunComponent.ID) != null) {
				distanceResult = -1;
			} else if (right.getEntity().getComponent(SunComponent.ID) != null) {
				distanceResult = 1;
			}

			return distanceResult;
		};

		Collections.sort(engine.lights, lightComparator);
		int k = engine.lights.size();

	 if (k >= maxLights) {
			engine.lights.subList(maxLights, k).clear();
		}
	}*/

	/**
	 * Carries out environment updates.
	 */
	public static void update() {
		Environment.updateEntities();
		Environment.updateParticleSystems();
	}

	public static QuadTree<Entity> setGenericEntityTree() {
		return genericEntityTree;
	}

	public static List<IParticleSystem> getParticleSystems() {
		return particleSystems;
	}

	public static Fog getFog() {
		return fog;
	}

	public static SunComponent getSun() {
		return (SunComponent) sun.getComponent(SunComponent.ID);
	}
}
