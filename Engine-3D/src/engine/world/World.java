package engine.world;

import engine.space.QuadTree;
import engine.terrains.Terrain;
import engine.waters.Water;

/**
 * Contains information on basic world constants and can retrieve height information.
 */
public class World {
	public static final float GRAVITY = -50;

	public static final float TERRAIN_SIZE = 5100f;
	public static final int TERRAIN_VERTEX_COUNT = 156;

	public static final float WATER_SIZE = World.TERRAIN_SIZE / 2.0f;
	public static final float WATER_Y_POS = 0.0f;

	private static QuadTree<Terrain> terrainTree;
	private static QuadTree<Water> waterTree;

	/**
	 * Initialises the world trees.
	 */
	public static void init() {
		terrainTree = new QuadTree<>();
		waterTree = new QuadTree<>();
	}

	/**
	 * Gets the terrain at a point.
	 *
	 * @param x Points X coord.
	 * @param z Points Z coord.
	 *
	 * @return Returns the terrain at that point.
	 */
	public static Terrain getTerrainAtPoint(float x, float z) {
		for (Terrain t : terrainTree.getAll()) {
			if (t.getHeightWorld(x, z) != 0) {
				return t;
			}
		}

		return null;
	}

	/**
	 * Gets the height of the terrain that the point is found to intersect.
	 *
	 * @param x Points X coord.
	 * @param z Points Z coord.
	 *
	 * @return Returns the Y height found at the point.
	 */
	public static float getTerrainHeight(float x, float z) {
		float height = 0;
		Terrain terrain = getTerrainAtPoint(x, z);

		if (terrain != null) {
			height = terrain.getHeightWorld(x, z);
		}

		return height;
	}

	/**
	 * Gets the water at a point.
	 *
	 * @param x Points X coord.
	 * @param z Points Z coord.
	 *
	 * @return Returns the water at that point.
	 */
	public static Water getWaterAtPoint(float x, float z) {
		for (Water w : waterTree.getAll()) {
			if (w.getHeightWorld(x, z) != 0) {
				return w;
			}
		}

		return null;
	}

	/**
	 * Gets the height of the water that the point is found to intersect.
	 *
	 * @param x Points X coord.
	 * @param z Points Z coord.
	 *
	 * @return Returns the Y height found at the point.
	 */
	public static float getWaterHeight(float x, float z) {
		float height = 0;
		Water water = getWaterAtPoint(x, z);

		if (water != null) {
			height = water.getHeightWorld(x, z);
		}

		return height;
	}

	public static QuadTree<Terrain> getTerrainTree() {
		return terrainTree;
	}

	public static QuadTree<Water> getWaterTree() {
		return waterTree;
	}
}
