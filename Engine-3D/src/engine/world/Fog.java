package engine.world;

import engine.toolbox.Colour;

/**
 * Represents a fog in the world.
 */
public class Fog {
	private Colour fogColour;
	private float fogDensity;
	private float fogGradient;
	private float skyLowerLimit;
	private float skyUpperLimit;

	/**
	 * Creates a new fog.
	 *
	 * @param colour The colour of the fog.
	 * @param fogDensity How dense the fog will be.
	 * @param fogGradient The gradient of the fog.
	 * @param skyLowerLimit At what height will the skybox fog begin to appear.
	 * @param skyUpperLimit At what height will there be skybox no fog.
	 */
	public Fog(Colour colour, float fogDensity, float fogGradient, float skyLowerLimit, float skyUpperLimit) {
		fogColour = colour;
		this.fogDensity = fogDensity;
		this.fogGradient = fogGradient;
		this.skyLowerLimit = skyLowerLimit;
		this.skyUpperLimit = skyUpperLimit;
	}

	public Colour getFogColour() {
		return fogColour;
	}

	public void setFogColour(Colour fogColour) {
		this.fogColour = fogColour;
	}

	public float getFogDensity() {
		return fogDensity;
	}

	public void setFogDensity(float fogDensity) {
		this.fogDensity = fogDensity;
	}

	public float getFogGradient() {
		return fogGradient;
	}

	public void setFogGradient(float fogGradient) {
		this.fogGradient = fogGradient;
	}

	public float getSkyLowerLimit() {
		return skyLowerLimit;
	}

	public float getSkyUpperLimit() {
		return skyUpperLimit;
	}
}
