/**
 * Contains classes for representing and rendering a skybox.
 */
package engine.skyboxes;