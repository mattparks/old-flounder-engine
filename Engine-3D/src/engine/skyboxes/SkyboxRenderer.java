package engine.skyboxes;

import engine.basics.EngineCore;
import engine.basics.ICamera;
import engine.basics.IRenderer;
import engine.basics.options.OptionsGraphics;
import engine.loaders.Loader;
import engine.textures.TextureManager;
import engine.toolbox.OpenglUtils;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Matrix4f;
import engine.toolbox.vector.Vector3f;
import engine.toolbox.vector.Vector4f;
import engine.world.Environment;
import org.lwjgl.opengl.GL11;

public class SkyboxRenderer extends IRenderer {
	public static final MyFile SKYBOXES_LOC = new MyFile(MyFile.RES_FOLDER, "skybox");

	private static final MyFile[] TEXTURE_FILES = {new MyFile(SKYBOXES_LOC, "nightRight.png"), new MyFile(SKYBOXES_LOC, "nightLeft.png"), new MyFile(SKYBOXES_LOC, "nightTop.png"), new MyFile(SKYBOXES_LOC, "nightBottom.png"), new MyFile(SKYBOXES_LOC, "nightBack.png"), new MyFile(SKYBOXES_LOC, "nightFront.png")};
	private static final int TEXTURE = TextureManager.loadCubeMap(TEXTURE_FILES);

	private static final float SIZE = 1800f;
	private static final float[] VERTICES = {-SIZE, SIZE, -SIZE, -SIZE, -SIZE, -SIZE, SIZE, -SIZE, -SIZE, SIZE, -SIZE, -SIZE, SIZE, SIZE, -SIZE, -SIZE, SIZE, -SIZE, -SIZE, -SIZE, SIZE, -SIZE, -SIZE, -SIZE, -SIZE, SIZE, -SIZE, -SIZE, SIZE, -SIZE, -SIZE, SIZE, SIZE, -SIZE, -SIZE, SIZE, SIZE, -SIZE, -SIZE, SIZE, -SIZE, SIZE, SIZE, SIZE, SIZE, SIZE, SIZE, SIZE, SIZE, SIZE, -SIZE, SIZE, -SIZE, -SIZE, -SIZE, -SIZE, SIZE, -SIZE, SIZE, SIZE, SIZE, SIZE, SIZE, SIZE, SIZE, SIZE, SIZE, -SIZE, SIZE, -SIZE, -SIZE, SIZE, -SIZE, SIZE, -SIZE, SIZE, SIZE, -SIZE, SIZE, SIZE, SIZE, SIZE, SIZE, SIZE, -SIZE, SIZE, SIZE, -SIZE, SIZE, -SIZE, -SIZE, -SIZE, -SIZE, -SIZE, -SIZE, SIZE, SIZE, -SIZE, -SIZE, SIZE, -SIZE, -SIZE, -SIZE, -SIZE, SIZE, SIZE, -SIZE, SIZE};
	public static final int VAO = Loader.createInterleavedVAO(VERTICES, 3);

	private SkyboxShader shader;

	public SkyboxRenderer() {
		shader = new SkyboxShader();
	}

	@Override
	public void renderObjects(Vector4f clipPlane, ICamera camera) {
		prepareRendering(clipPlane, camera);

		{
			OpenglUtils.bindVAO(VAO, 0);
			OpenglUtils.antialias(OptionsGraphics.ANTI_ALIASING);
			OpenglUtils.bindTextureToBank(TEXTURE, 0);

			GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, VERTICES.length); // Renders the skybox.

			OpenglUtils.unbindVAO(0);
		}

		endRendering();
	}

	private void prepareRendering(Vector4f clipPlane, ICamera camera) {
		shader.start();

		shader.projectionMatrix.loadMat4(EngineCore.getModule().getRendererMaster().getProjectionMatrix());
		shader.viewMatrix.loadMat4(createViewMatrix(camera.getViewMatrix()));

		shader.lowerFogLimit.loadFloat(Environment.getFog().getSkyLowerLimit());
		shader.upperFogLimit.loadFloat(Environment.getFog().getSkyUpperLimit());
		shader.fogColour.loadVec3(Environment.getFog().getFogColour().toVector());
	}

	private Matrix4f createViewMatrix(Matrix4f input) {
		Matrix4f matrix = new Matrix4f(input);
		matrix.m30 = 0;
		matrix.m31 = 0;
		matrix.m32 = 0;
		float rotation = 0;
		return Matrix4f.rotate(matrix, new Vector3f(0, 1, 0), (float) Math.toRadians(rotation), matrix);
	}

	private void endRendering() {
		shader.stop();
	}

	@Override
	public void cleanUp() {
		shader.cleanUp();
	}
}
