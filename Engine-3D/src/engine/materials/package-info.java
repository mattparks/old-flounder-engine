/**
 * Contains classes for loading and representing materials for meshes.
 */
package engine.materials;