package engine.materials;

import engine.textures.Texture;
import engine.toolbox.Colour;
import engine.toolbox.Logger;
import engine.toolbox.resources.MyFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class capable of loading MTL files into Materials.
 */
public class LoaderMTL {
	private static Map<String, SoftReference<List<Material>>> loaded = new HashMap<>();

	/**
	 * Loads a MTL file into a list of Material objects.
	 *
	 * @param file The file to be loaded.
	 *
	 * @return Returns a loaded list of MTLMaterials.
	 */
	public static List<Material> loadMTL(MyFile file) {
		SoftReference<List<Material>> ref = loaded.get(file.getPath());
		List<Material> data = ref == null ? null : ref.get();

		if (data == null) {
			BufferedReader reader = null;

			try {
				reader = file.getReader();
			} catch (Exception e) {
				e.printStackTrace();
			}

			String line;
			String parseMaterialName = "";
			Material parseMaterial = new Material();
			data = new ArrayList<>();

			if (reader == null) {
				Logger.error("Error creating reader the MTL: " + file);
			}

			try {
				while ((line = reader.readLine()) != null) {
					String prefix = line.split(" ")[0];

					if (line.startsWith("#")) {
						continue;
					}

					switch (prefix) {
						case "newmtl":
							if (!parseMaterialName.equals("")) {
								data.add(parseMaterial);
							}

							parseMaterialName = line.split(" ")[1];
							parseMaterial = new Material();
							parseMaterial.name = parseMaterialName;
							break;
						case "Ns":
							parseMaterial.specularCoefficient = Float.valueOf(line.split(" ")[1]);
							break;
						case "Ka":
							String[] rgbKa = line.split(" ");
							parseMaterial.ambientColour = new Colour(Float.valueOf(rgbKa[1]), Float.valueOf(rgbKa[2]), Float.valueOf(rgbKa[3]));
							break;
						case "Kd":
							String[] rgbKd = line.split(" ");
							parseMaterial.diffuseColour = new Colour(Float.valueOf(rgbKd[1]), Float.valueOf(rgbKd[2]), Float.valueOf(rgbKd[3]));
							break;
						case "Ks":
							String[] rgbKs = line.split(" ");
							parseMaterial.specularColour = new Colour(Float.valueOf(rgbKs[1]), Float.valueOf(rgbKs[2]), Float.valueOf(rgbKs[3]));
							break;
						case "map_Kd":
							parseMaterial.texture = Texture.newTexture(new MyFile(MyFile.RES_FOLDER, line.split(" ")[1])).create();
							break;
						case "map_bump":
							parseMaterial.normalMap = Texture.newTexture(new MyFile(MyFile.RES_FOLDER, line.split(" ")[1])).create();
							break;
						default:
							System.err.println("[MTL " + file.getName() + "] Unknown Line: " + line);
							break;
					}
				}

				reader.close();
				data.add(parseMaterial);
			} catch (IOException | NullPointerException e) {
				Logger.error("Error reading the MTL: " + file);
			}

			loaded.put(file.getPath(), new SoftReference<>(data));
		}

		return data;
	}
}
