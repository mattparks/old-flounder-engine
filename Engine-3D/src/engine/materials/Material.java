package engine.materials;

import engine.textures.Texture;
import engine.toolbox.Colour;

public class Material {
	public String name;
	public float specularCoefficient = 100;
	public Colour ambientColour = new Colour(0.2f, 0.2f, 0.2f);
	public Colour diffuseColour = new Colour(0.3f, 1, 1);
	public Colour specularColour = new Colour(1, 1, 1);
	public Texture texture;
	public Texture normalMap;

	@Override
	public String toString() {
		return name + "[ " + "SpecularCoefficient=(" + specularCoefficient + "), AmbientColour=" + ambientColour.toString() + ", DiffuseColour=" + diffuseColour.toString() + ", SpecularColour=" + specularColour.toString() + " ]";
	}
}
