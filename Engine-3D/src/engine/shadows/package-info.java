/**
 * Contains classes for rendering a 3D shadow map.
 */
package engine.shadows;