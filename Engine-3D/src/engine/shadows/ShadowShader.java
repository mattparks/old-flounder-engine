package engine.shadows;

import engine.shaders.ShaderProgram;
import engine.shaders.UniformMat4;
import engine.toolbox.resources.MyFile;

public class ShadowShader extends ShaderProgram {
	private static final MyFile VERTEX_SHADER = new MyFile("engine/shadows", "shadowVertex.glsl");
	private static final MyFile FRAGMENT_SHADER = new MyFile("engine/shadows", "shadowFragment.glsl");

	protected UniformMat4 mvpMatrix = new UniformMat4("mvpMatrix");

	protected ShadowShader() {
		super(VERTEX_SHADER, FRAGMENT_SHADER);
		super.storeAllUniformLocations(mvpMatrix);
	}
}
