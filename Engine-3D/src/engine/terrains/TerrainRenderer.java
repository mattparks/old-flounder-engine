package engine.terrains;

import engine.basics.EngineCore;
import engine.basics.ICamera;
import engine.basics.IRenderer;
import engine.basics.MasterRenderer3D;
import engine.basics.options.OptionsGraphics;
import engine.lights.Light;
import engine.models.Model;
import engine.shadows.ShadowRenderer;
import engine.toolbox.Maths;
import engine.toolbox.OpenglUtils;
import engine.toolbox.vector.Matrix4f;
import engine.toolbox.vector.Vector3f;
import engine.toolbox.vector.Vector4f;
import engine.world.Environment;
import org.lwjgl.opengl.GL11;

import java.util.List;

public class TerrainRenderer extends IRenderer {
	private TerrainShader shader;

	public TerrainRenderer() {
		shader = new TerrainShader();
	}

	@Override
	public void renderObjects(Vector4f clipPlane, ICamera camera) {
		prepareRendering(clipPlane, camera);

		for (Terrain t : Environment.getTerrainObjects(camera)) {
			prepareTerrain(((MasterRenderer3D) (EngineCore.getModule().getRendererMaster())).getShadowMapRenderer(), t);
			GL11.glDrawElements(GL11.GL_TRIANGLES, t.getModel().getVAOLength(), GL11.GL_UNSIGNED_INT, 0); // Render the terrain instance.
			unbindTerrain();
		}

		endRendering();
	}

	private void prepareRendering(Vector4f clipPlane, ICamera camera) {
		shader.start();
		shader.projectionMatrix.loadMat4(EngineCore.getModule().getRendererMaster().getProjectionMatrix());
		shader.viewMatrix.loadMat4(camera.getViewMatrix());
		shader.clipPlane.loadVec4(clipPlane);

		shader.fogColour.loadVec3(Environment.getFog().getFogColour().toVector());
		shader.fogDensity.loadFloat(Environment.getFog().getFogDensity());
		shader.fogGradient.loadFloat(Environment.getFog().getFogGradient());

		List<Light> lights = Environment.getLightObjects(camera);

		for (int i = 0; i < TerrainShader.MAX_LIGHTS; i++) {
			if (i < lights.size()) {
				shader.lightPosition[i].loadVec3(lights.get(i).getPosition());
				shader.lightColour[i].loadVec3(lights.get(i).getColour().toVector());
				shader.attenuation[i].loadVec3(lights.get(i).getAttenuation().toVector());
			} else {
				shader.lightPosition[i].loadVec3(new Vector3f(0, 0, 0));
				shader.lightColour[i].loadVec3(new Vector3f(0, 0, 0));
				shader.attenuation[i].loadVec3(new Vector3f(1, 0, 0));
			}
		}
	}

	private void prepareTerrain(ShadowRenderer shadowMapMaster, Terrain terrain) {
		Model model = terrain.getModel();
		TerrainTexturePack texturePack = terrain.getTexturePack();
		Matrix4f transformationMatrix = Maths.createTransformationMatrix(terrain.getPosition(), terrain.getRotation(), 1);

		OpenglUtils.bindVAO(model.getVaoID(), 0, 1, 2);
		OpenglUtils.antialias(OptionsGraphics.ANTI_ALIASING);
		OpenglUtils.cullBackFaces(true);

		shader.modelMatrix.loadMat4(transformationMatrix);

		shader.shadowSpaceMatrix.loadMat4(shadowMapMaster.getToShadowMapSpaceMatrix());
		shader.shadowDistance.loadFloat(shadowMapMaster.getShadowDistance());

		shader.shineDamper.loadFloat(1); // TODO: Add shines.
		shader.reflectivity.loadFloat(0);

		shader.shadowMapSize.loadFloat(ShadowRenderer.SHADOW_MAP_SIZE);

		OpenglUtils.bindTextureToBank(texturePack.getBackgroundTexture().getTextureID(), 0);
		OpenglUtils.bindTextureToBank(texturePack.getRTexture().getTextureID(), 1);
		OpenglUtils.bindTextureToBank(texturePack.getGTexture().getTextureID(), 2);
		OpenglUtils.bindTextureToBank(texturePack.getBTexture().getTextureID(), 3);
		OpenglUtils.bindTextureToBank(shadowMapMaster.getShadowMap(), 4);
	}

	private void unbindTerrain() {
		OpenglUtils.unbindVAO(0, 1, 2);
	}

	private void endRendering() {
		shader.stop();
	}

	@Override
	public void cleanUp() {
		shader.cleanUp();
	}
}
