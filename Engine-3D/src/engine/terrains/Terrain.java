package engine.terrains;

import engine.entities.Entity;
import engine.models.Model;
import engine.perlin.GameSeed;
import engine.perlin.PerlinNoiseGenerator;
import engine.physics.AABB;
import engine.space.ISpatialObject;
import engine.space.QuadTree;
import engine.toolbox.Maths;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Vector2f;
import engine.toolbox.vector.Vector3f;
import engine.world.World;

/**
 * Represents a terrain object/
 */
public class Terrain implements ISpatialObject {
	public static final MyFile TERRAINS_LOC = new MyFile(MyFile.RES_FOLDER, "terrain");

	private QuadTree<Entity> entityQuadtree;

	private Vector3f position;
	private Vector3f rotation;
	private float[][] heights;
	private Model model;
	private TerrainTexturePack texturePack;
	private PerlinNoiseGenerator noise;

	/**
	 * Creates a new game terrain that the player and engine.entities can interact on.
	 *
	 * @param gridX The position on the x grid.
	 * @param gridZ The position on the z grid.
	 * @param texturePack The set of textures used when rendering the engine.terrains blend maps.
	 */
	public Terrain(float gridX, float gridZ, TerrainTexturePack texturePack) {
		entityQuadtree = new QuadTree<>();

		this.texturePack = texturePack;
		position = new Vector3f(gridX * World.TERRAIN_SIZE, 0.0f, gridZ * World.TERRAIN_SIZE);
		rotation = new Vector3f(0, 0, 0); // TODO: Rotation!
		noise = new PerlinNoiseGenerator((int) GameSeed.getSeed());

		generateTerrain();
	}

	private void generateTerrain() {
		heights = new float[World.TERRAIN_VERTEX_COUNT][World.TERRAIN_VERTEX_COUNT];

		int count = World.TERRAIN_VERTEX_COUNT * World.TERRAIN_VERTEX_COUNT;
		float[] vertices = new float[count * 3];
		float[] normals = new float[count * 3];
		float[] textureCoords = new float[count * 2];
		int[] indices = new int[6 * (World.TERRAIN_VERTEX_COUNT - 1) * (World.TERRAIN_VERTEX_COUNT - 1)];
		int vertexPointer = 0;

		for (int i = 0; i < World.TERRAIN_VERTEX_COUNT; i++) {
			for (int j = 0; j < World.TERRAIN_VERTEX_COUNT; j++) {
				vertices[vertexPointer * 3] = j / ((float) World.TERRAIN_VERTEX_COUNT - 1) * World.TERRAIN_SIZE;
				float height = getHeightFromMap(j, i);
				heights[j][i] = height;
				vertices[vertexPointer * 3 + 1] = height;
				vertices[vertexPointer * 3 + 2] = i / ((float) World.TERRAIN_VERTEX_COUNT - 1) * World.TERRAIN_SIZE;
				Vector3f normal = calculateMapNormal(j, i);
				normals[vertexPointer * 3] = normal.getX();
				normals[vertexPointer * 3 + 1] = normal.getY();
				normals[vertexPointer * 3 + 2] = normal.getZ();
				textureCoords[vertexPointer * 2] = j / ((float) World.TERRAIN_VERTEX_COUNT - 1);
				textureCoords[vertexPointer * 2 + 1] = i / ((float) World.TERRAIN_VERTEX_COUNT - 1);
				vertexPointer++;
			}
		}

		int pointer = 0;

		for (int gz = 0; gz < World.TERRAIN_VERTEX_COUNT - 1; gz++) {
			for (int gx = 0; gx < World.TERRAIN_VERTEX_COUNT - 1; gx++) {
				int topLeft = gz * World.TERRAIN_VERTEX_COUNT + gx;
				int topRight = topLeft + 1;
				int bottomLeft = (gz + 1) * World.TERRAIN_VERTEX_COUNT + gx;
				int bottomRight = bottomLeft + 1;
				indices[pointer++] = topLeft;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = topRight;
				indices[pointer++] = topRight;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = bottomRight;
			}
		}

		model = new Model(vertices, textureCoords, normals, null, indices, 3);
	}

	private float getHeightFromMap(int x, int y) {
		float spread = 8.25f;
		float xTight = 0.75f;
		float yTight = 0.50f;
		float scale = 100.0f;
		return noise.tileableNoise2((position.x + x) / spread * xTight, (position.y + y) / spread * yTight, World.TERRAIN_VERTEX_COUNT, World.TERRAIN_VERTEX_COUNT) * scale;
	}

	private Vector3f calculateMapNormal(int x, int y) {
		float heightL = getHeightFromMap(x - 1, y);
		float heightR = getHeightFromMap(x + 1, y);
		float heightD = getHeightFromMap(x, y - 1);
		float heightU = getHeightFromMap(x, y + 1);
		Vector3f normal = new Vector3f(heightL - heightR, 2f, heightD - heightU);
		normal.normalize();
		return normal;
	}

	/**
	 * Gets the height of the terrain from a world coordinate.
	 *
	 * @param worldX World coordinate in the X.
	 * @param worldZ World coordinate in the Z.
	 *
	 * @return Returns the height at that spot.
	 */
	public float getHeightWorld(float worldX, float worldZ) {
		float terrainX = worldX - position.getX();
		float terrainZ = worldZ - position.getZ();
		float gridSquareSize = World.TERRAIN_SIZE / (heights.length - 1);
		int gridX = (int) Math.floor(terrainX / gridSquareSize);
		int gridZ = (int) Math.floor(terrainZ / gridSquareSize);

		if (gridX >= heights.length - 1 || gridZ >= heights.length - 1 || gridX < 0 || gridZ < 0) {
			return 0;
		}

		float xCoord = terrainX % gridSquareSize / gridSquareSize;
		float zCoord = terrainZ % gridSquareSize / gridSquareSize;
		float result;

		if (xCoord <= 1 - zCoord) {
			result = Maths.baryCentric(new Vector3f(0, heights[gridX][gridZ], 0), new Vector3f(1, heights[gridX + 1][gridZ], 0), new Vector3f(0, heights[gridX][gridZ + 1], 1), new Vector2f(xCoord, zCoord));
		} else {
			result = Maths.baryCentric(new Vector3f(1, heights[gridX + 1][gridZ], 0), new Vector3f(1, heights[gridX + 1][gridZ + 1], 1), new Vector3f(0, heights[gridX][gridZ + 1], 1), new Vector2f(xCoord, zCoord));
		}

		return result;
	}

	public QuadTree<Entity> getEntityQuadtree() {
		return entityQuadtree;
	}

	/**
	 * @return Gets the engine.terrains total rotation on the XYZ axes.
	 */
	public Vector3f getRotation() {
		return rotation;
	}

	/**
	 * @return Gets the actual model behind the terrain.
	 */
	public Model getModel() {
		return model;
	}

	/**
	 * @return Gets the terrain texture pack used on this terrain.
	 */
	public TerrainTexturePack getTexturePack() {
		return texturePack;
	}

	@Override
	public AABB getAABB() {
		return new AABB(new Vector3f(getPosition().getX(), getPosition().getY(), getPosition().getZ()), new Vector3f(getPosition().getX() + World.TERRAIN_SIZE, getPosition().getY() + World.TERRAIN_SIZE, getPosition().getZ() + World.TERRAIN_SIZE));
	}

	/**
	 * @return Gets the engine.terrains position in world space.
	 */
	public Vector3f getPosition() {
		return position;
	}
}
