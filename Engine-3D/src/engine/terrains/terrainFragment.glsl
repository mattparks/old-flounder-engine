#version 130

varying vec2 pass_textureCoords;
varying vec3 pass_surfaceNormal;
varying vec4 pass_shadowCoords;
varying vec3 pass_toCameraVector;
varying vec3 pass_toLightVector[4];
varying vec4 pass_positionRelativeToCam;
varying vec3 pass_tilePosition;

out vec4 out_colour;

layout(binding = 0) uniform sampler2D backgroundTexture;
layout(binding = 1) uniform sampler2D rTexture;
layout(binding = 2) uniform sampler2D gTexture;
layout(binding = 3) uniform sampler2D bTexture;
layout(binding = 4) uniform sampler2D shadowMap;

uniform vec3 lightColour[4];
uniform vec3 attenuation[4];
uniform float shineDamper;
uniform float reflectivity;
uniform float shadowMapSize;
uniform vec3 fogColour;
uniform float fogDensity;
uniform float fogGradient;

const float tileAmount = 50.0;
const float shadowDarkness = 0.6;

void main(void) {
	vec4 blendMapColour = vec4(pass_tilePosition, 1.0);
	blendMapColour = vec4(normalize(pass_surfaceNormal).x, 0, 0, 1); // TODO
	float backgroundTextureAmount = 1 - (blendMapColour.r + blendMapColour.g + blendMapColour.b);
	vec2 tiledCoords = pass_textureCoords * tileAmount;
	vec4 totalTextureColour = (texture(backgroundTexture, tiledCoords) * backgroundTextureAmount) + (texture(rTexture, tiledCoords) * blendMapColour.r) + (texture(gTexture, tiledCoords) * blendMapColour.g) + (texture(bTexture, tiledCoords) * blendMapColour.b);

	vec3 unitNormal = normalize(pass_surfaceNormal);
	vec3 unitVectorToCamera = normalize(pass_toCameraVector);
	vec3 totalDiffuse = vec3(0.0);
	vec3 totalSpecular = vec3(0.0);

	for (int i = 0; i < 4; i++) {
		float distance = length(pass_toLightVector[i]);
		float attinuationFactor = attenuation[i].x + (attenuation[i].y * distance) + (attenuation[i].z * distance * distance);
		vec3 unitLightVector = normalize(pass_toLightVector[i]);

		float brightness = max(dot(unitNormal, unitLightVector), 0.0);
		vec3 reflectedLightDirection = reflect(-unitLightVector, unitNormal);
		float specularFactor = max(dot(reflectedLightDirection, unitVectorToCamera), 0.0);
		float dampedFactor = pow(specularFactor, shineDamper);

		totalDiffuse = totalDiffuse + (brightness * lightColour[i]) / attinuationFactor;
		totalSpecular = totalSpecular + (dampedFactor * reflectivity * lightColour[i]) / attinuationFactor;
	}

	float visibility = clamp(exp(-pow((length(pass_positionRelativeToCam.xyz) * fogDensity), fogGradient)), 0.0, 1.0);
	totalDiffuse = max(totalDiffuse, 0.2);

	float shadowTexelSize = 1.0 / shadowMapSize;
	float shadowHalfw = shadowTexelSize * 0.5;
	float shadowTotal = 0.0;
	float shadowValue = 0.0;
	float shadowShadeFactor;
	shadowValue = texture(shadowMap, pass_shadowCoords.xy + vec2(0 + shadowHalfw, 0 + shadowHalfw)).r;

    if (pass_shadowCoords.x > 0.0 && pass_shadowCoords.x < 1.0 && pass_shadowCoords.y > 0.0 && pass_shadowCoords.y < 1.0 && pass_shadowCoords.z > 0.0 && pass_shadowCoords.z < 1.0) {
        if (shadowValue < pass_shadowCoords.z) {
            shadowTotal += shadowDarkness * pass_shadowCoords.w;
        }

        shadowValue = texture(shadowMap, pass_shadowCoords.xy + vec2(shadowTexelSize + shadowHalfw, 0 + shadowHalfw)).r;

        if (shadowValue < pass_shadowCoords.z) {
            shadowTotal += shadowDarkness * pass_shadowCoords.w;
        }

        shadowValue = texture(shadowMap, pass_shadowCoords.xy + vec2(0 + shadowHalfw, shadowTexelSize + shadowHalfw)).r;

        if (shadowValue < pass_shadowCoords.z) {
            shadowTotal += shadowDarkness * pass_shadowCoords.w;
        }

        shadowValue = texture(shadowMap, pass_shadowCoords.xy + vec2(shadowTexelSize + shadowHalfw, shadowTexelSize + shadowHalfw)).r;

        if (shadowValue < pass_shadowCoords.z) {
            shadowTotal += shadowDarkness * pass_shadowCoords.w;
        }

	    shadowShadeFactor = 1.0 - (shadowTotal / 4.0);
	} else {
	    shadowShadeFactor = 1.0;
	}

	out_colour = vec4(totalDiffuse, 1.0) * (totalTextureColour * shadowShadeFactor) + vec4(totalSpecular, 1.0);
	out_colour = mix(vec4(fogColour, 1.0), out_colour, visibility);
}
