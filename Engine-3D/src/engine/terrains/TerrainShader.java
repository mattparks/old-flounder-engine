package engine.terrains;

import engine.shaders.ShaderProgram;
import engine.shaders.UniformFloat;
import engine.shaders.UniformMat4;
import engine.shaders.UniformVec3;
import engine.shaders.UniformVec4;
import engine.toolbox.resources.MyFile;

public class TerrainShader extends ShaderProgram {
	public static final int MAX_LIGHTS = 4;

	private static final MyFile VERTEX_SHADER = new MyFile("engine/terrains", "terrainVertex.glsl");
	private static final MyFile FRAGMENT_SHADER = new MyFile("engine/terrains", "terrainFragment.glsl");

	protected UniformMat4 modelMatrix = new UniformMat4("modelMatrix");
	protected UniformMat4 projectionMatrix = new UniformMat4("projectionMatrix");
	protected UniformMat4 shadowSpaceMatrix = new UniformMat4("shadowSpaceMatrix");
	protected UniformFloat shadowDistance = new UniformFloat("shadowDistance");
	protected UniformMat4 viewMatrix = new UniformMat4("viewMatrix");
	protected UniformVec4 clipPlane = new UniformVec4("clipPlane");
	protected UniformVec3[] lightPosition = new UniformVec3[MAX_LIGHTS];
	protected UniformVec3[] lightColour = new UniformVec3[MAX_LIGHTS];
	protected UniformVec3[] attenuation = new UniformVec3[MAX_LIGHTS];
	protected UniformFloat shineDamper = new UniformFloat("shineDamper");
	protected UniformFloat reflectivity = new UniformFloat("reflectivity");
	protected UniformFloat shadowMapSize = new UniformFloat("shadowMapSize");
	protected UniformVec3 fogColour = new UniformVec3("fogColour");
	protected UniformFloat fogDensity = new UniformFloat("fogDensity");
	protected UniformFloat fogGradient = new UniformFloat("fogGradient");

	protected TerrainShader() {
		super(VERTEX_SHADER, FRAGMENT_SHADER);
		super.storeAllUniformLocations(modelMatrix, projectionMatrix, viewMatrix, clipPlane, shadowSpaceMatrix, shadowDistance, shineDamper, reflectivity, shadowMapSize, fogColour, fogDensity, fogGradient);

		for (int i = 0; i < MAX_LIGHTS; i++) {
			lightPosition[i] = new UniformVec3("lightPosition[" + i + "]");
			lightColour[i] = new UniformVec3("lightColour[" + i + "]");
			attenuation[i] = new UniformVec3("attenuation[" + i + "]");
			super.storeAllUniformLocations(lightPosition[i], lightColour[i], attenuation[i]);
		}
	}
}
