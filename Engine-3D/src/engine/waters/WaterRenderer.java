package engine.waters;

import engine.basics.EngineCore;
import engine.basics.ICamera;
import engine.basics.IRenderer;
import engine.basics.options.OptionsGraphics;
import engine.entities.components.LightComponent;
import engine.textures.Texture;
import engine.toolbox.Maths;
import engine.toolbox.OpenglUtils;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Matrix4f;
import engine.toolbox.vector.Vector4f;
import engine.world.Environment;
import engine.world.World;
import org.lwjgl.opengl.GL11;

public class WaterRenderer extends IRenderer {
	public static final MyFile WATERS_LOC = new MyFile(MyFile.RES_FOLDER, "water");
	public static final Texture dudvTexture = Texture.newTexture(new MyFile(WATERS_LOC, "dudvMap.png")).create();
	public static final Texture normalTexture = Texture.newTexture(new MyFile(WATERS_LOC, "normalMap.png")).create();

	private static final float WAVE_SPEED = 0.0125f;

	private WaterShader shader;
	private WaterFrameBuffers waterBufferObject;
	private float moveFactor;

	// public FBO refractionFBO;
	// public FBO reflectionFBO;

	public WaterRenderer() {
		shader = new WaterShader();
		waterBufferObject = new WaterFrameBuffers();
		moveFactor = 0;

		//	refractionFBO = FBO.newFBO(DeviceDisplay.getWidth(), DeviceDisplay.getHeight()).depthBuffer(FBOBuilder.DepthBufferType.TEXTURE).fitToScreen().create();
		//	reflectionFBO = FBO.newFBO(DeviceDisplay.getWidth(), DeviceDisplay.getHeight()).depthBuffer(FBOBuilder.DepthBufferType.RENDER_BUFFER).fitToScreen().create();
	}

	@Override
	public void renderObjects(Vector4f clipPlane, ICamera camera) {
		prepareRendering(clipPlane, camera);

		moveFactor += WAVE_SPEED * EngineCore.getDeltaSeconds();
		moveFactor %= 1;

		for (Water w : Environment.getWaterObjects(camera)) {
			prepareWater(w);
			GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, Water.VERTICES.length); // Render the water instance.
			unbindWater();
		}

		endRendering();
	}

	private void prepareRendering(Vector4f clipPlane, ICamera camera) {
		shader.start();
		shader.projectionMatrix.loadMat4(EngineCore.getModule().getRendererMaster().getProjectionMatrix());
		shader.viewMatrix.loadMat4(camera.getViewMatrix());
		shader.nearPlane.loadFloat(camera.getNearPlane());
		shader.farPlane.loadFloat(camera.getFarPlane());

		shader.fogColour.loadVec3(Environment.getFog().getFogColour().toVector());
		shader.fogDensity.loadFloat(Environment.getFog().getFogDensity());
		shader.fogGradient.loadFloat(Environment.getFog().getFogGradient());

		LightComponent sunComponent = (LightComponent) Environment.getSun().getEntity().getComponent(LightComponent.ID);
		shader.lightPosition.loadVec3(sunComponent.getLight().getPosition());
		shader.lightColour.loadVec3(sunComponent.getLight().getColour().toVector());
	}

	private void prepareWater(Water tile) {
		Matrix4f modelMatrix = Maths.createTransformationMatrix(tile.getPosition(), tile.getRotation(), World.WATER_SIZE);
		shader.modelMatrix.loadMat4(modelMatrix);

		shader.moveFactor.loadFloat(moveFactor);
		shader.colourAdditive.loadVec3(tile.colourAdditive);
		shader.colourMix.loadFloat(tile.colourMix);
		shader.textureTiling.loadFloat(tile.textureTiling);
		shader.waveStrength.loadFloat(tile.waveStrength);
		shader.normalDampener.loadFloat(tile.normalDampener);
		shader.dropOffDepth.loadFloat(tile.dropOffDepth);
		shader.reflectivity.loadFloat(tile.reflectivity);
		shader.shineDamper.loadFloat(tile.shineDamper);

		OpenglUtils.bindVAO(Water.VAO, 0);
		OpenglUtils.antialias(OptionsGraphics.ANTI_ALIASING);
		OpenglUtils.cullBackFaces(false);
		OpenglUtils.enableDepthTesting();
		OpenglUtils.disableBlending();

		OpenglUtils.bindTextureToBank(waterBufferObject.getReflectionTexture(), 0);
		OpenglUtils.bindTextureToBank(waterBufferObject.getRefractionTexture(), 1);
		OpenglUtils.bindTextureToBank(dudvTexture.getTextureID(), 2);
		OpenglUtils.bindTextureToBank(normalTexture.getTextureID(), 3);
		OpenglUtils.bindTextureToBank(waterBufferObject.getRefractionDepthTexture(), 4);
	}

	private void unbindWater() {
		OpenglUtils.unbindVAO(0);
	}

	private void endRendering() {
		shader.stop();
	}

	public void bindRefractionFrameBuffer() {
		waterBufferObject.bindRefractionFrameBuffer();
	}

	public void bindReflectionFrameBuffer() {
		waterBufferObject.bindReflectionFrameBuffer();
	}

	public void unbindCurrentFrameBuffer() {
		waterBufferObject.unbindCurrentFrameBuffer();
	}

	@Override
	public void cleanUp() {
		shader.cleanUp();
		waterBufferObject.cleanUp();
	}
}