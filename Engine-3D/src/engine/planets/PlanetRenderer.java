package engine.planets;

import engine.basics.EngineCore;
import engine.basics.ICamera;
import engine.basics.IRenderer;
import engine.toolbox.Maths;
import engine.toolbox.OpenglUtils;
import engine.toolbox.vector.Vector3f;
import engine.toolbox.vector.Vector4f;
import org.lwjgl.opengl.GL11;

public class PlanetRenderer extends IRenderer {
	private PlanetShader shader;
	private Planet planet;

	public PlanetRenderer() {
		shader = new PlanetShader();
		planet = new Planet();
	}

	@Override
	public void renderObjects(Vector4f clipPlane, ICamera camera) {
		prepareRendering(clipPlane, camera);

		planet.update(camera);

		for (PlanetChunk c : planet.getChunks()) {
			Vector3f position = Vector3f.add(planet.getPosition(), c.getPosition(), null);
			shader.modelMatrix.loadMat4(Maths.createTransformationMatrix(position, c.getRotation(), 1));
			shader.colour.loadVec3(c.getColour().toVector());
			OpenglUtils.bindVAO(c.getModel().getVaoID(), 0);
			OpenglUtils.antialias(true);
			OpenglUtils.cullBackFaces(false);
			GL11.glDrawElements(GL11.GL_TRIANGLES, c.getModel().getVAOLength(), GL11.GL_UNSIGNED_INT, 0); // Render the planet chunk instance.
			OpenglUtils.unbindVAO(0);
		}

		endRendering();
	}

	public void prepareRendering(Vector4f clipPlane, ICamera camera) {
		shader.start();
		shader.projectionMatrix.loadMat4(EngineCore.getModule().getRendererMaster().getProjectionMatrix());
		shader.viewMatrix.loadMat4(camera.getViewMatrix());
	}

	private void endRendering() {
		shader.stop();
	}

	@Override
	public void cleanUp() {
		shader.cleanUp();
	}
}
