package engine.planets;

import engine.shaders.ShaderProgram;
import engine.shaders.UniformMat4;
import engine.shaders.UniformVec3;
import engine.toolbox.resources.MyFile;

public class PlanetShader extends ShaderProgram {
	private static final MyFile VERTEX_SHADER = new MyFile("engine/planets", "planetVertex.glsl");
	private static final MyFile FRAGMENT_SHADER = new MyFile("engine/planets", "planetFragment.glsl");

	protected UniformMat4 modelMatrix = new UniformMat4("modelMatrix");
	protected UniformMat4 projectionMatrix = new UniformMat4("projectionMatrix");
	protected UniformMat4 viewMatrix = new UniformMat4("viewMatrix");
	protected UniformVec3 colour = new UniformVec3("colour");

	protected PlanetShader() {
		super(VERTEX_SHADER, FRAGMENT_SHADER);
		super.storeAllUniformLocations(modelMatrix, projectionMatrix, viewMatrix, colour);
	}
}
