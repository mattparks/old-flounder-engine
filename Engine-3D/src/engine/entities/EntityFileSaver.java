package engine.entities;

import engine.entities.components.ModelComponent;
import engine.models.ModelTextured;
import engine.toolbox.Logger;
import engine.toolbox.Maths;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Vector2f;
import engine.toolbox.vector.Vector3f;
import engine.toolbox.vector.Vector4f;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Class capable of saving engine.entities to a .entity file.
 */
public class EntityFileSaver {
	private static int lineLength;

	public static void save(Entity entity, String name) {
		try {
			// MyFile file = new MyFile(EntityFileTemplate.ENTITIES_FOLDER, name + ".entity");
			File saveFile = new File("resources/res/engine.entities/" + name + ".entity"); // TODO

			saveFile.createNewFile();
			FileWriter fileWriter = new FileWriter(saveFile);
			String savedDate = Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + "." + (Calendar.getInstance().get(Calendar.MONTH) + 1) + "." + Calendar.getInstance().get(Calendar.YEAR) + " - " + Calendar.getInstance().get(Calendar.HOUR) + ":" + Calendar.getInstance().get(Calendar.MINUTE);

			System.out.println("Entity " + name + " is being saved at: " + savedDate);
			EntityFileFunctions.reset();

			EntityFileFunctions.addComment(fileWriter, "Date Generated: " + savedDate, "Created By: " + System.getProperty("user.name"));

			// Entity General Data:
			EntityFileFunctions.beginNewSegment(fileWriter, "EntityData");
			{
				EntityFileFunctions.writeSegmentData(fileWriter, "Name: " + name, true);
			}
			EntityFileFunctions.endSegment(fileWriter, false);

			// Components:
			EntityFileFunctions.beginNewSegment(fileWriter, "Components");
			for (int i = 0; i < entity.getComponents().size(); i++) {
				EntityFileFunctions.beginNewSegment(fileWriter, entity.getComponents().get(i).getClass().getName());
				for (String s : entity.getComponents().get(i).getSavableValues()) {
					EntityFileFunctions.writeSegmentData(fileWriter, s, true);
				}
				EntityFileFunctions.endSegment(fileWriter, i == entity.getComponents().size() - 1);
			}
			EntityFileFunctions.endSegment(fileWriter, false);

			// Model Data:
			ModelComponent mc = ((ModelComponent) entity.getComponent(ModelComponent.ID));

			if (mc != null) {
				EntityFileFunctions.beginNewSegment(fileWriter, "ModelData");
				{
					ModelTextured model = mc.getModel();

					// Save Texture Into Multitexture:
					List<MyFile> textures = new ArrayList<>();
					String multiTexturePath = "res/entities/" + name + ".png";

					if (model.getModelTexture() != null) {
						textures.add(model.getModelTexture().getFile());
					}

					if (model.getNormalTexture() != null) {
						textures.add(model.getNormalTexture().getFile());
					}

					EntityFileImage.saveList("resources/" + multiTexturePath, textures);

					// General:
					EntityFileFunctions.writeSegmentData(fileWriter, "MultiTexture: " + multiTexturePath, true);
					EntityFileFunctions.writeSegmentData(fileWriter, "ModelTexture: " + model.getModelTexture().getFile().getPath(), true);
					EntityFileFunctions.writeSegmentData(fileWriter, "NormalTexture: " + ((model.getNormalTexture() != null) ? model.getNormalTexture().getFile() : null), true);
					EntityFileFunctions.writeSegmentData(fileWriter, "ColourMod: " + EntityFileFunctions.vec3String(model.getColourMod().toVector()), true);
					EntityFileFunctions.writeSegmentData(fileWriter, "UsesFakeLighting: " + model.isUseFakeLighting(), true);
					EntityFileFunctions.writeSegmentData(fileWriter, "ShineDamper: " + model.getShineDamper(), true);
					EntityFileFunctions.writeSegmentData(fileWriter, "Reflectivity: " + model.getReflectivity());
					EntityFileFunctions.enterBlankLine(fileWriter);
					EntityFileFunctions.enterBlankLine(fileWriter);

					// Materials:
					//	EntityFileFunctions.beginNewSegment(fileWriter, "Materials");
					//	for (Material m : model.getModel().getData().getModelMaterials()) {
					//		EntityFileFunctions.beginNewSegment(fileWriter, m.name);
					//		{
					//			EntityFileFunctions.writeSegmentData(fileWriter, "SpecularCoefficient: " + m.specularCoefficient, true);
					//			EntityFileFunctions.writeSegmentData(fileWriter, "AmbientColour: " + EntityFileFunctions.vec3String(m.ambientColour.toVector()), true);
					//			EntityFileFunctions.writeSegmentData(fileWriter, "DiffuseColour: " + EntityFileFunctions.vec3String(m.diffuseColour.toVector()), true);
					//			EntityFileFunctions.writeSegmentData(fileWriter, "SpecularColour: " + EntityFileFunctions.vec3String(m.specularColour.toVector()), true);
					//			EntityFileFunctions.writeSegmentData(fileWriter, "TextureMap: " + ((m.texture != null) ? m.texture.getFile() : null), true);
					//			EntityFileFunctions.writeSegmentData(fileWriter, "NormalMap: " + ((m.normalMap != null) ? m.normalMap.getFile() : null), true);
					//		}
					//		EntityFileFunctions.endSegment(fileWriter, true);
					//	}
					//	EntityFileFunctions.endSegment(fileWriter, false);

					// Vertices:
					EntityFileFunctions.beginNewSegment(fileWriter, "Vertices");
					for (Vector3f v : model.getModel().getVerticesList()) {
						String s = EntityFileFunctions.vec3String(v) + ",";
						EntityFileFunctions.writeSegmentData(fileWriter, s);
					}
					EntityFileFunctions.endSegment(fileWriter, false);

					// Material Usages:
					//	EntityFileFunctions.beginNewSegment(fileWriter, "MaterialUsages");
					//	for (String m : model.getModel().getData().getVerticeMaterials()) {
					//		EntityFileFunctions.writeSegmentData(fileWriter, m + ", ");
					//	}
					//	EntityFileFunctions.endSegment(fileWriter, false);

					// Colours:
					EntityFileFunctions.beginNewSegment(fileWriter, "Colours"); // TODO

					try {
						BufferedImage img = ImageIO.read(new File("resources" + model.getModelTexture().getFile().getPath()));

						for (Vector2f v : model.getModel().getTexturesList()) {
							// int numberOfRows = model.getModelTexture().getNumberOfRows(); // TODO

							int argb = img.getRGB((int) Math.abs(Maths.limit(img.getWidth() * (v.x), img.getWidth() - 1)), (int) Math.abs(Maths.limit(img.getHeight() * (v.y), img.getHeight() - 1))); // TODO Could be optimized!
							Color c = new Color(argb, true);
							Vector4f colour = new Vector4f(c.getRed() * (1 / 255.0f), c.getGreen() * (1 / 255.0f), c.getBlue() * (1 / 255.0f), c.getAlpha() * (1 / 255.0f));
							String s = EntityFileFunctions.vec4String(colour) + ",";
							EntityFileFunctions.writeSegmentData(fileWriter, s);
						}
						EntityFileFunctions.endSegment(fileWriter, false);
					} catch (IOException e) {
						e.printStackTrace();
					}

					// Texture Coordinates:
					EntityFileFunctions.beginNewSegment(fileWriter, "TextureCoords");
					for (Vector2f v : model.getModel().getTexturesList()) {
						String s = EntityFileFunctions.vec2String(v) + ",";
						EntityFileFunctions.writeSegmentData(fileWriter, s);
					}
					EntityFileFunctions.endSegment(fileWriter, false);

					// Normals:
					EntityFileFunctions.beginNewSegment(fileWriter, "Normals");
					for (Vector3f v : model.getModel().getNormalsList()) {
						String s = EntityFileFunctions.vec3String(v) + ",";
						EntityFileFunctions.writeSegmentData(fileWriter, s);
					}
					EntityFileFunctions.endSegment(fileWriter, false);

					// Tangents:
					EntityFileFunctions.beginNewSegment(fileWriter, "Tangents");
					for (Vector3f v : model.getModel().getTangentsList()) {
						String s = EntityFileFunctions.vec3String(v) + ",";
						EntityFileFunctions.writeSegmentData(fileWriter, s);
					}
					EntityFileFunctions.endSegment(fileWriter, false);

					// Indices:
					EntityFileFunctions.beginNewSegment(fileWriter, "Indices");
					for (int i : model.getModel().getIndicesList()) {
						String s = i + "/";
						EntityFileFunctions.writeSegmentData(fileWriter, s);
					}
					EntityFileFunctions.endSegment(fileWriter, true);
				}
				EntityFileFunctions.endSegment(fileWriter, true);
			}

			fileWriter.close();
		} catch (IOException e) {
			Logger.error("File saver for entity " + name + " did not execute successfully!");
			e.printStackTrace();
		}
	}

	public static class EntityFileFunctions {
		public static final int maxLineLength = 5000;
		public static int fileNestation;

		/**
		 * Resets the saver for a new file.
		 */
		public static void reset() {
			fileNestation = 0;
		}

		/**
		 * Turns a Vector 4 into a printable string.
		 *
		 * @param v The vector to read from.
		 *
		 * @return The printable string.
		 */
		public static String vec4String(Vector4f v) {
			return "[" + v.getX() + "/" + v.getY() + "/" + v.getZ() + "/" + v.getW() + "]";
		}

		/**
		 * Turns a Vector 3 into a printable string.
		 *
		 * @param v The vector to read from.
		 *
		 * @return The printable string.
		 */
		public static String vec3String(Vector3f v) {
			return "[" + v.getX() + "/" + v.getY() + "/" + v.getZ() + "]";
		}

		/**
		 * Turns a Vector 2 into a printable string.
		 *
		 * @param v The vector to read from.
		 *
		 * @return The printable string.
		 */
		public static String vec2String(Vector2f v) {
			return "[" + v.getX() + "/" + v.getY() + "]";
		}

		private static void beginNewSegment(FileWriter fw, String name) throws IOException {
			name = getIndentations() + name + " {";
			fw.write(name);
			enterBlankLine(fw);
			lineLength = 0;
			fileNestation++;
		}

		private static void endSegment(FileWriter fw, boolean enterTightSpace) throws IOException {
			enterBlankLine(fw);
			fileNestation--;
			fw.write(getIndentations() + "}");

			if (!enterTightSpace) {
				enterBlankLine(fw);
				enterBlankLine(fw);
			}
		}

		private static String getIndentations() {
			String data = "";

			for (int i = 0; i < fileNestation; i++) {
				data += "	";
			}

			return data;
		}

		private static void writeSegmentData(FileWriter fw, String data, boolean breakAfter) throws IOException {
			writeSegmentData(fw, data);

			if (breakAfter) {
				lineLength = maxLineLength;
			}
		}

		private static void writeSegmentData(FileWriter fw, String... data) throws IOException {
			if (lineLength >= maxLineLength) {
				lineLength = 0;
				enterBlankLine(fw);
			}

			if (lineLength == 0) {
				fw.write(getIndentations());
				lineLength = getIndentations().length();
			}

			for (String s : data) {
				lineLength += s.length();
				fw.write(s);
			}
		}

		private static void addComment(FileWriter fw, String... lines) throws IOException {
			fw.write(getIndentations() + "/**");
			enterBlankLine(fw);

			for (String line : lines) {
				fw.write(getIndentations() + " * " + line);
				enterBlankLine(fw);
			}

			fw.write(getIndentations() + " */");
			enterBlankLine(fw);
		}

		private static void startFileLine(FileWriter fw, String data) throws IOException {
			fw.write(data);
		}

		private static void writeSingleLine(FileWriter fw, String data) throws IOException {
			enterBlankLine(fw);
			fw.write(data);
		}

		private static void enterBlankLine(FileWriter fw) throws IOException {
			fw.write("\n");
		}
	}
}
