package engine.entities;

import engine.models.Model;
import engine.models.ModelTextured;
import engine.textures.Texture;
import engine.toolbox.Colour;
import engine.toolbox.Logger;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Vector2f;
import engine.toolbox.vector.Vector3f;
import engine.toolbox.vector.Vector4f;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class capable of loading engine.entities from a .entity file.
 */
public class EntityFileLoader {
	public static EntityFileTemplate load(String name) {
		MyFile file = new MyFile(EntityFileTemplate.ENTITIES_FOLDER, name + ".entity");
		BufferedReader reader;
		String line;

		try {
			reader = file.getReader();
		} catch (Exception e) {
			return null;
		}

		if (reader == null) {
			Logger.error("Error creating reader the OBJ: " + file);
		}

		try {
			String entityName = "unnamed";
			String modelTexture = null;
			String normalTexture = null;
			Colour colourMod = new Colour();
			boolean usesFakeLighting = false;
			float shineDamper = 1;
			float reflectivity = 0;

			float[] loadedVertices = null;
			float[] loadedColours = null;
			float[] loadedTextureCoords = null;
			float[] loadedNormals = null;
			float[] loadedTangents = null;
			int[] loadedIndices = null;

			Map<String, List<String>> componentsData = new HashMap<>();

			while ((line = reader.readLine()) != null) {
				if (line.contains("EntityData")) {
					while (!(line = reader.readLine()).contains("}")) {
						if (line.contains("Name")) {
							entityName = line.replaceAll("\\s+", "").substring("Name:".length());
						}
					}
				}

				if (line.contains("Components")) {
					int fileNestation = 1;

					String cKey = "null";
					List<String> cData = new ArrayList<>();

					while (fileNestation > 0) {
						if (line.contains("engine.entities.components")) {
							cKey = line.replaceAll("\\s+", "");
							cKey = cKey.substring(0, cKey.length() - 1);
						} else if (fileNestation == 2 && !line.replaceAll("\\s+", "").isEmpty()) {
							cData.add(line.replaceAll("\\s+", ""));
						}

						line = reader.readLine();

						if (line == null) {
							fileNestation = 0;
						} else {
							if (line.contains("{")) {
								fileNestation++;
							}

							if (line.contains("}")) {
								fileNestation--;

								if (cKey.equals("null")) {
									componentsData.put(cKey, cData);
								}

								cKey = "null";
								cData = new ArrayList<>();
							}
						}
					}
				}

				if (line.contains("ModelData")) {
					int fileNestation = 1;

					while (fileNestation > 0) {
						if (line.contains("ModelTexture")) {
							modelTexture = line.replaceAll("\\s+", "").substring("ModelTexture:".length());
						}

						if (line.contains("NormalTexture")) {
							normalTexture = line.replaceAll("\\s+", "").substring("NormalTexture:".length());
						}

						if (line.contains("ColourMod")) {
							String w = line.replaceAll("\\s+", "").substring("ColourMod: ".length());
							String[] valuesRead = w.substring(1, w.length() - 1).split("/");
							colourMod = new Colour(Float.parseFloat(valuesRead[0]), Float.parseFloat(valuesRead[1]), Float.parseFloat(valuesRead[2]));
						}

						if (line.contains("UsesFakeLighting")) {
							usesFakeLighting = Boolean.parseBoolean(line.replaceAll("\\s+", "").substring("UsesFakeLighting:".length()));
						}

						if (line.contains("ShineDamper")) {
							shineDamper = Float.parseFloat(line.replaceAll("\\s+", "").substring("ShineDamper:".length()));
						}

						if (line.contains("Reflectivity")) {
							reflectivity = Float.parseFloat(line.replaceAll("\\s+", "").substring("Reflectivity:".length()));
						}

						if (line.contains("Vertices")) {
							List<Vector3f> loadedVerticesList = new ArrayList<>();

							while (!(line = reader.readLine()).contains("}")) {
								for (String w : line.replaceAll("\\s+", "").split(",")) {
									String[] valuesRead = w.substring(1, w.length() - 1).split("/");
									loadedVerticesList.add(new Vector3f(Float.parseFloat(valuesRead[0]), Float.parseFloat(valuesRead[1]), Float.parseFloat(valuesRead[2])));
								}
							}

							loadedVertices = toFloatArrayV3(loadedVerticesList);
						}

						if (line.contains("Colours")) {
							List<Vector4f> loadedColoursList = new ArrayList<>();

							while (!(line = reader.readLine()).contains("}")) {
								for (String w : line.replaceAll("\\s+", "").split(",")) {
									String[] valuesRead = w.substring(1, w.length() - 1).split("/");
									loadedColoursList.add(new Vector4f(Float.parseFloat(valuesRead[0]), Float.parseFloat(valuesRead[1]), Float.parseFloat(valuesRead[2]), Float.parseFloat(valuesRead[3])));
								}
							}

							loadedColours = toFloatArrayV4(loadedColoursList);
						}

						if (line.contains("TextureCoords")) {
							List<Vector2f> loadedTextureCoordinatesList = new ArrayList<>();

							while (!(line = reader.readLine()).contains("}")) {
								for (String w : line.replaceAll("\\s+", "").split(",")) {
									String[] valuesRead = w.substring(1, w.length() - 1).split("/");
									loadedTextureCoordinatesList.add(new Vector2f(Float.parseFloat(valuesRead[0]), Float.parseFloat(valuesRead[1])));
								}
							}

							loadedTextureCoords = toFloatArrayV2(loadedTextureCoordinatesList);
						}

						if (line.contains("Normals")) {
							List<Vector3f> loadedNormalsList = new ArrayList<>();

							while (!(line = reader.readLine()).contains("}")) {
								for (String w : line.replaceAll("\\s+", "").split(",")) {
									String[] valuesRead = w.substring(1, w.length() - 1).split("/");
									loadedNormalsList.add(new Vector3f(Float.parseFloat(valuesRead[0]), Float.parseFloat(valuesRead[1]), Float.parseFloat(valuesRead[2])));
								}
							}

							loadedNormals = toFloatArrayV3(loadedNormalsList);
						}

						if (line.contains("Tangents")) {
							List<Vector3f> loadedTangentsList = new ArrayList<>();

							while (!(line = reader.readLine()).contains("}")) {
								for (String w : line.replaceAll("\\s+", "").split(",")) {
									String[] valuesRead = w.substring(1, w.length() - 1).split("/");
									loadedTangentsList.add(new Vector3f(Float.parseFloat(valuesRead[0]), Float.parseFloat(valuesRead[1]), Float.parseFloat(valuesRead[2])));
								}
							}

							loadedTangents = toFloatArrayV3(loadedTangentsList);
						}

						if (line.contains("Indices")) {
							List<Integer> loadedIndicesList = new ArrayList<>();

							while (!(line = reader.readLine()).contains("}")) {
								for (String w : line.replaceAll("\\s+", "").split(",")) {
									String[] valuesRead = w.split("/");

									for (String s : valuesRead) {
										loadedIndicesList.add(Integer.parseInt(s));
									}
								}
							}

							loadedIndices = toIntArrayV1(loadedIndicesList);
						}

						line = reader.readLine();

						if (line == null) {
							fileNestation = 0;
						} else {
							if (line.contains("{")) {
								fileNestation++;
							}

							if (line.contains("}")) {
								fileNestation--;
							}
						}
					}
				}
			}

			reader.close();
			ModelTextured model = null;

			if (normalTexture != null || loadedVertices != null || loadedTextureCoords != null || loadedNormals != null || loadedTangents != null || loadedIndices != null) {
				Texture loadedModelTexture = Texture.newTexture(new MyFile(modelTexture.substring(1, modelTexture.length()))).create();
				Texture loadedNormalTexture = (normalTexture != null && !normalTexture.contains("null")) ? Texture.newTexture(new MyFile(normalTexture.substring(1, normalTexture.length()))).create() : null;
				model = new ModelTextured(new Model(loadedVertices, loadedTextureCoords, loadedNormals, loadedTangents, loadedColours, loadedIndices, 3), loadedModelTexture);

				if (loadedNormalTexture != null) {
					model.setNormalTexture(loadedNormalTexture);
				}

				model.setColourMod(colourMod);
				model.setUseFakeLighting(usesFakeLighting);
				model.setShineDamper(shineDamper);
				model.setReflectivity(reflectivity);
			}

			System.out.println(entityName + " is being loaded into an entity template!");
			return new EntityFileTemplate(name, model, componentsData);
		} catch (IOException e) {
			Logger.error("File reader for entity " + file.getName() + " did not execute successfully!");
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Converts an list of Vector 4's into a array of floats.
	 *
	 * @param input The vectors to be stored.
	 *
	 * @return The new array of floats.
	 */
	private static float[] toFloatArrayV4(List<Vector4f> input) {
		float[] list = new float[input.size() * 4];
		int index = 0;

		for (Vector4f v : input) {
			list[index] = v.getX();
			list[index + 1] = v.getY();
			list[index + 2] = v.getZ();
			list[index + 3] = v.getW();
		}

		return list;
	}

	/**
	 * Converts an list of Vector 3's into a array of floats.
	 *
	 * @param input The vectors to be stored.
	 *
	 * @return The new array of floats.
	 */
	private static float[] toFloatArrayV3(List<Vector3f> input) {
		float[] list = new float[input.size() * 3];
		int index = 0;

		for (Vector3f v : input) {
			list[index] = v.getX();
			list[index + 1] = v.getY();
			list[index + 2] = v.getZ();
		}

		return list;
	}

	/**
	 * Converts an list of Vector 2's into a array of floats.
	 *
	 * @param input The vectors to be stored.
	 *
	 * @return The new array of floats.
	 */
	private static float[] toFloatArrayV2(List<Vector2f> input) {
		float[] list = new float[input.size() * 2];
		int index = 0;

		for (Vector2f v : input) {
			list[index] = v.getX();
			list[index + 1] = v.getY();
		}

		return list;
	}

	/**
	 * Converts an list of Integers into a array of ints.
	 *
	 * @param input The Integers to be stored.
	 *
	 * @return The new array of ints.
	 */
	private static int[] toIntArrayV1(List<Integer> input) {
		int[] list = new int[input.size()];
		int index = 0;

		for (int i : input) {
			list[index] = i;
		}

		return list;
	}
}
