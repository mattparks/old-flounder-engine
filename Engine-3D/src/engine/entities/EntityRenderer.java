package engine.entities;

import engine.basics.EngineCore;
import engine.basics.ICamera;
import engine.basics.IRenderer;
import engine.basics.MasterRenderer3D;
import engine.basics.options.OptionsGraphics;
import engine.entities.components.ModelComponent;
import engine.lights.Light;
import engine.models.ModelTextured;
import engine.shadows.ShadowRenderer;
import engine.toolbox.OpenglUtils;
import engine.toolbox.vector.Vector3f;
import engine.toolbox.vector.Vector4f;
import engine.world.Environment;
import org.lwjgl.opengl.GL11;

import java.util.List;
import java.util.Map;

public class EntityRenderer extends IRenderer {
	public EntityShader shader;

	public EntityRenderer() {
		shader = new EntityShader();
	}

	@Override
	public void renderObjects(Vector4f clipPlane, ICamera camera) {
		prepareRendering(clipPlane, camera);
		Map<ModelTextured, List<Entity>> entities = Environment.getEntityObjects(camera);

		for (ModelTextured model : entities.keySet()) {
			prepareTexturedModel(((MasterRenderer3D) (EngineCore.getModule().getRendererMaster())).getShadowMapRenderer(), model);

			for (Entity e : entities.get(model)) {
				prepareInstance(e);
				GL11.glDrawElements(GL11.GL_TRIANGLES, model.getModel().getVAOLength(), GL11.GL_UNSIGNED_INT, 0); // Render the entity instance.
			}

			unbindTexturedModel();
		}

		endRendering();
	}

	private void prepareRendering(Vector4f clipPlane, ICamera camera) {
		shader.start();
		shader.projectionMatrix.loadMat4(EngineCore.getModule().getRendererMaster().getProjectionMatrix());
		shader.viewMatrix.loadMat4(camera.getViewMatrix());
		shader.clipPlane.loadVec4(clipPlane);

		shader.fogColour.loadVec3(Environment.getFog().getFogColour().toVector());
		shader.fogDensity.loadFloat(Environment.getFog().getFogDensity());
		shader.fogGradient.loadFloat(Environment.getFog().getFogGradient());

		List<Light> lights = Environment.getLightObjects(camera);

		for (int i = 0; i < EntityShader.MAX_LIGHTS; i++) {
			if (i < lights.size()) {
				shader.lightPosition[i].loadVec3(lights.get(i).getPosition());
				shader.lightColour[i].loadVec3(lights.get(i).getColour().toVector());
				shader.attenuation[i].loadVec3(lights.get(i).getAttenuation().toVector());
			} else {
				shader.lightPosition[i].loadVec3(new Vector3f(0, 0, 0));
				shader.lightColour[i].loadVec3(new Vector3f(0, 0, 0));
				shader.attenuation[i].loadVec3(new Vector3f(1, 0, 0));
			}
		}
	}

	private void prepareTexturedModel(ShadowRenderer shadowMapMaster, ModelTextured texturedModel) {
		shader.useFakeLighting.loadBoolean(texturedModel.isUseFakeLighting());
		shader.atlasRows.loadFloat(texturedModel.getModelTexture().getNumberOfRows());

		shader.shineDamper.loadFloat(texturedModel.getShineDamper());
		shader.reflectivity.loadFloat(texturedModel.getReflectivity());
		shader.useNormalMap.loadBoolean(texturedModel.getNormalTexture() != null);

		OpenglUtils.bindVAO(texturedModel.getModel().getVaoID(), 0, 1, 2, 3, 4);
		OpenglUtils.antialias(OptionsGraphics.ANTI_ALIASING);
		OpenglUtils.enableDepthTesting();
		OpenglUtils.enableAlphaBlending();

		OpenglUtils.bindTextureToBank(texturedModel.getModelTexture().getTextureID(), 0);
		OpenglUtils.bindTextureToBank(texturedModel.getNormalTexture() == null ? 0 : texturedModel.getNormalTexture().getTextureID(), 1);
	}

	private void prepareInstance(Entity entity) {
		ModelComponent modelComponent = (ModelComponent) entity.getComponent(ModelComponent.ID);
		shader.modelMatrix.loadMat4(entity.getModelMatrix());
		shader.colourMod.loadVec3(modelComponent.getModel().getColourMod().toVector());
		shader.transparency.loadFloat(modelComponent.getTransparency());
		shader.atlasOffset.loadVec2(modelComponent.getTextureOffset());

		if (modelComponent.getTransparency() != 1.0 || modelComponent.getModel().getModelTexture().isHasTransparency()) {
			OpenglUtils.cullBackFaces(false); // Disable face culling if the object has transparency.
		} else {
			OpenglUtils.cullBackFaces(true); // Enable face culling if the object does not have transparency.
		}
	}

	private void unbindTexturedModel() {
		OpenglUtils.unbindVAO(0, 1, 2, 3, 4);
	}

	private void endRendering() {
		shader.stop();
	}

	@Override
	public void cleanUp() {
		shader.cleanUp();
	}
}
