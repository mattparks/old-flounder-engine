package engine.entities;

import engine.toolbox.Maths;
import engine.toolbox.resources.MyFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EntityFileImage {
	public static void saveList(String target, List<MyFile> textures) {
		try {
			// Creates a list of buffered images and find values for the output image.
			List<BufferedImage> images = new ArrayList<>();
			int maxWidth = 0;
			int maxHeight = 0;

			for (int t = 0; t < textures.size(); t++) {
				images.add(ImageIO.read(new File("resources" + textures.get(t).getPath())));

				if (images.get(t).getWidth() > maxWidth) {
					maxWidth = images.get(t).getWidth();
				}

				if (images.get(t).getHeight() > maxHeight) {
					maxHeight = images.get(t).getHeight();
				}
			}

			// Creates each materials output image.
			List<BufferedImage> outputs = new ArrayList<>();

			for (BufferedImage i : images) {
				BufferedImage textureOutput = new BufferedImage(maxWidth, maxHeight, BufferedImage.TYPE_INT_ARGB);
				double iScale = Maths.maxValue(maxWidth / i.getWidth(), maxHeight / i.getHeight());
				AffineTransform at = new AffineTransform();
				at.scale(iScale, iScale);
				AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
				textureOutput = scaleOp.filter(i, textureOutput);
				outputs.add(textureOutput);
			}

			// Puts each materials output image in a final buffered image.
			BufferedImage outputImage = joinBufferedImages(outputs);

			// Lastly, the images is saved and whiten to.
			File outputFile = new File(target);

			if (outputFile.exists()) {
				outputFile.delete();
			}

			outputFile.createNewFile();
			ImageIO.write(outputImage, "png", outputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static BufferedImage joinBufferedImages(List<BufferedImage> images) {
		// Finds values for the resulting image first.
		int width = 0;
		int height = 0;

		for (BufferedImage i : images) {
			width += i.getWidth();

			if (i.getHeight() > height) {
				height = i.getHeight();
			}
		}

		// Create a new buffer and draw two image into the new image.
		BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = newImage.createGraphics();
		Color oldColor = g2.getColor();

		// Draws the final image.
		g2.setColor(oldColor);

		for (int t = 0; t < images.size(); t++) {
			g2.drawImage(images.get(t), null, images.get(t).getWidth() * t, 0);
		}

		g2.dispose();
		return newImage;
	}
}
