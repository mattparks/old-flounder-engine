package engine.entities.components;

import engine.basics.EngineCore;
import engine.entities.Entity;
import engine.entities.EntityFileTemplate;
import engine.entities.EntityIDAssigner;

/**
 * A RemoveComponent that fades out until the entity disappears.
 */
public class FadeRemove extends RemoveComponent {
	public static final int ID = EntityIDAssigner.getId();

	public boolean removesAfterDuration;
	private double timer;
	private double duration;

	/**
	 * Creates a new FadeRemove
	 *
	 * @param entity The entity this component is attached to.
	 * @param duration How long the fade out takes.
	 */
	public FadeRemove(Entity entity, double duration) {
		super(entity);
		timer = 0.0;
		this.duration = duration;
		removesAfterDuration = true;
	}

	/**
	 * Creates a new FadeRemove. From strings loaded from entity files.
	 *
	 * @param entity The entity this component is attached to.
	 * @param template The entity template to load data from.
	 */
	public FadeRemove(Entity entity, EntityFileTemplate template) {
		this(entity, 0); //TODO
	}

	@Override
	public void deactivate() {
		timer = 0.0;
	}

	@Override
	public void onActivate() {
	}

	@Override
	public void removeUpdate() {
		timer += EngineCore.getDeltaSeconds();
		double fadeAmount = (duration - timer) / duration;

		if (timer >= duration && removesAfterDuration) {
			getEntity().removeComponent(CollisionComponent.ID);
			getEntity().forceRemove();
		}

		ModelComponent mc = (ModelComponent) super.getEntity().getComponent(ModelComponent.ID);

		if (mc != null) {
			mc.setTransparency((float) fadeAmount);
		}

		// LightComponent l = (LightComponent) getEntity().getComponent(LightComponent.ID);

		// if (l != null) {
		// l.setColourIntensity(new Colour(amount, amount, amount));
		// }
	}

	@Override
	public String[] getSavableValues() {
		return new String[]{"RemovesAfterDuration: " + removesAfterDuration, "Duration: " + duration};
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}
}