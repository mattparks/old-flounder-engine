/**
 * Provides various components that are useful in virtually any game situation.
 */
package engine.entities.components;