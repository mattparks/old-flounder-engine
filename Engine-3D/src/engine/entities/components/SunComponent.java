package engine.entities.components;

import engine.entities.Entity;
import engine.entities.EntityFileTemplate;
import engine.entities.EntityIDAssigner;
import engine.entities.IEntityComponent;
import engine.toolbox.Colour;

public class SunComponent extends IEntityComponent {
	public static final int ID = EntityIDAssigner.getId();

	public SunComponent(Entity entity) {
		super(entity, ID);
	}

	/**
	 * Creates a new SunComponent. From strings loaded from entity files.
	 *
	 * @param entity The entity this component is attached to.
	 * @param template The entity template to load data from.
	 */
	public SunComponent(Entity entity, EntityFileTemplate template) {
		this(entity);
	}


	// http://pastebin.com/v0Jt57s4

	// private void calculateSunPosition(float time) {
	// double radianTime = ((time * Math.PI) / 12000) - (Math.PI / 2);
	// float x = (float) (SUN_DISTANCE * Math.cos(radianTime));
	// float z = (float) ((1 - RELATIVE_HEIGHT_OF_ARC) * SUN_DISTANCE * Math.sin(radianTime));
	// float y = (float) (RELATIVE_HEIGHT_OF_ARC * SUN_DISTANCE * Math.sin(radianTime)) + HEIGHT_OFFSET;
	// sky.getSun().setPosition(x, y, z);
	// }
	// public void setPosition(float x, float y, float z) {
	// sunlight.setPosition(x, y, z);
	// Vector3f toSunFromCamera = new Vector3f(x - camera.getX(), y - camera.getY(), z - camera.getZ());
	// toSunFromCamera.normalise();
	// this.pitch = 360 - (float) Math.toDegrees(Math.asin(toSunFromCamera.y));
	// toSunFromCamera.scale(renderDistance);
	// this.x = camera.getX() + toSunFromCamera.x;
	// this.y = camera.getY() + toSunFromCamera.y;
	// this.z = camera.getZ() + toSunFromCamera.z;
	//
	// float dX = camera.getX() - x;
	// float dZ = camera.getZ() - z;
	// this.yaw = (float) (Math.toDegrees(Math.atan2(dX, dZ)));
	// }

	/**
	 * Changes the colour emitted by the sun.
	 *
	 * @param sunColour The new colour too be emitted.
	 */
	public void updateColour(Colour sunColour) {
		LightComponent lc = (LightComponent) super.getEntity().getComponent(LightComponent.ID);

		if (lc != null) {
			lc.getLight().setColour(sunColour);
		}
	}

	@Override
	public void update() {
	}

	@Override
	public String[] getSavableValues() {
		return new String[]{};
	}
}
