package engine.entities.components;

import engine.entities.Entity;
import engine.entities.EntityFileSaver;
import engine.entities.EntityFileTemplate;
import engine.entities.EntityIDAssigner;
import engine.entities.IEntityComponent;
import engine.lights.Light;
import engine.toolbox.Colour;
import engine.toolbox.vector.Vector3f;

/**
 * Creates a light that can be rendered into the world.
 */
public class LightComponent extends IEntityComponent {
	public static final int ID = EntityIDAssigner.getId();

	private Vector3f offset;
	private Light light;

	/**
	 * Creates a new LightComponent.
	 *
	 * @param entity The entity this component is attached to.
	 * @param offset How much the light position should be offset from the engine.entities position.
	 * @param light The light too be attached too the entity.
	 */
	public LightComponent(Entity entity, Vector3f offset, Light light) {
		super(entity, ID);
		this.offset = offset;
		this.light = light;
	}

	/**
	 * Creates a new LightComponent. From strings loaded from entity files.
	 *
	 * @param entity The entity this component is attached to.
	 * @param template The entity template to load data from.
	 */
	public LightComponent(Entity entity, EntityFileTemplate template) {
		this(entity, new Vector3f(), new Light(new Colour(1, 1, 1), new Vector3f())); // TODO
	}

	public Vector3f getOffset() {
		return offset;
	}

	public void setOffset(Vector3f offset) {
		this.offset = offset;
	}

	public Light getLight() {
		return new Light(light.getColour(), Vector3f.add(super.getEntity().getPosition(), offset, null), light.getAttenuation());
	}

	@Override
	public void update() {
	}

	@Override
	public String[] getSavableValues() {
		return new String[]{"Offset: " + EntityFileSaver.EntityFileFunctions.vec3String(offset), "LightAttenuation: " + EntityFileSaver.EntityFileFunctions.vec3String(light.getAttenuation().toVector()), "LightColour: " + EntityFileSaver.EntityFileFunctions.vec3String(light.getColour().toVector())};
	}
}
