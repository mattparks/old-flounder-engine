package engine.entities.components;

import engine.entities.Entity;
import engine.entities.EntityFileTemplate;
import engine.entities.EntityIDAssigner;
import engine.entities.IEntityComponent;
import engine.models.ModelTextured;
import engine.physics.AABB;
import engine.toolbox.vector.Vector2f;
import engine.toolbox.vector.Vector3f;

/**
 * Creates a model with a texture that can be rendered into the world.
 */
public class ModelComponent extends IEntityComponent {
	public static final int ID = EntityIDAssigner.getId();

	private ModelTextured model;
	private float transparency;
	private float scale;
	private int textureIndex;

	/**
	 * Creates a new ModelComponent.
	 *
	 * @param entity The entity this component is attached to.
	 * @param model The model that will be attached too this entity.
	 * @param scale The scale of the entity.
	 */
	public ModelComponent(Entity entity, ModelTextured model, float scale) {
		this(entity, model, scale, 0);
	}

	/**
	 * Creates a new ModelComponent.
	 *
	 * @param entity The entity this component is attached to.
	 * @param model The model that will be attached too this entity.
	 * @param scale The scale of the entity.
	 * @param textureIndex What texture index this entity should renderObjects from (0 default).
	 */
	public ModelComponent(Entity entity, ModelTextured model, float scale, int textureIndex) {
		super(entity, ID);
		this.model = model;
		transparency = 1.0f;
		this.scale = scale;
		this.textureIndex = textureIndex;

		//AABBMesh gottenAABBMesh = model.getModel().getAABBMesh();
		AABB gottenAABB = model.getModel().getAABB().scale(new Vector3f(scale, scale, scale));

		ColliderComponent c = (ColliderComponent) getEntity().getComponent(ColliderComponent.ID);

		if (c != null) {
			c.fitAABB(gottenAABB); // gottenAABBMesh,
		}
	}

	/**
	 * Creates a new ModelComponent. From strings loaded from entity files.
	 *
	 * @param entity The entity this component is attached to.
	 * @param template The entity template to load data from.
	 */
	public ModelComponent(Entity entity, EntityFileTemplate template) {
		this(entity, template.getModel(), 1, 0);
	}

	/**
	 * Gets the textures coordinate offset that is used in rendering the model.
	 *
	 * @return The coordinate offset used in rendering.
	 */
	public Vector2f getTextureOffset() {
		int column = textureIndex % model.getModelTexture().getNumberOfRows();
		int row = textureIndex / model.getModelTexture().getNumberOfRows();
		return new Vector2f((float) row / (float) model.getModelTexture().getNumberOfRows(), (float) column / (float) model.getModelTexture().getNumberOfRows());
	}

	public ModelTextured getModel() {
		return model;
	}

	public float getTransparency() {
		return transparency;
	}

	public void setTransparency(float transparency) {
		this.transparency = transparency;
	}

	public float getScale() {
		return scale;
	}

	public void setScale(float scale) {
		this.scale = scale;
	}

	public void setTextureIndex(int index) {
		this.textureIndex = index;
	}

	@Override
	public void update() {
		// model.getModel().getAabbMesh().setPosition(super.createEntity().getPosition());
	}

	@Override
	public String[] getSavableValues() {
		return new String[]{"Transparency: " + transparency, "Scale: " + scale, "TextureIndex: " + textureIndex};
	}
}
