package engine.entities.components;

import engine.basics.EngineCore;
import engine.devices.DeviceKeyboard;
import engine.entities.Entity;
import engine.entities.EntityFileTemplate;
import engine.entities.EntityIDAssigner;
import engine.entities.IEntityComponent;
import engine.inputs.ButtonAxis;
import engine.inputs.CompoundAxis;
import engine.inputs.CompoundButton;
import engine.inputs.IAxis;
import engine.inputs.IButton;
import engine.inputs.JoystickAxis;
import engine.inputs.JoystickButton;
import engine.inputs.KeyButton;
import engine.terrains.Terrain;
import engine.toolbox.Maths;
import engine.toolbox.vector.Vector3f;
import engine.world.World;

/**
 * Creates a new controllable player component for the game.
 */
public class PlayerComponent extends IEntityComponent {
	public static final int ID = EntityIDAssigner.getId();

	private static final float RUN_SPEED = 40;
	private static final float TURN_SPEED = 160;
	private static final float JUMP_POWER = 30;

	private boolean isInAir;
	private float currentSpeed;
	private float currentTurnSpeed;
	private float currentUpwardsSpeed;
	private String username;

	private IAxis inputForward;
	private IAxis inputTurn;
	private IButton inputJump;
	private IButton inputBoost;

	/**
	 * Creates a new PlayerComponent.
	 *
	 * @param entity The entity this component is attached to.
	 * @param username The username represented by this player component.
	 */
	public PlayerComponent(Entity entity, String username) {
		super(entity, ID);
		isInAir = false;
		currentSpeed = 0;
		currentTurnSpeed = 0;
		currentUpwardsSpeed = 0;
		this.username = username;

		IButton leftKeyButtons = new KeyButton(new int[]{DeviceKeyboard.KEY_A, DeviceKeyboard.KEY_LEFT});
		IButton rightKeyButtons = new KeyButton(new int[]{DeviceKeyboard.KEY_D, DeviceKeyboard.KEY_RIGHT});
		IButton upKeyButtons = new KeyButton(new int[]{DeviceKeyboard.KEY_W, DeviceKeyboard.KEY_UP});
		IButton downKeyButtons = new KeyButton(new int[]{DeviceKeyboard.KEY_S, DeviceKeyboard.KEY_DOWN});
		IButton jumpKeyButtons = new KeyButton(DeviceKeyboard.KEY_SPACE);
		IButton boostKeyButtons = new KeyButton(DeviceKeyboard.KEY_LEFT_SHIFT);

		IButton jumpJoyButtons = new JoystickButton(0, 0);
		IButton boostJoyButtons = new JoystickButton(0, 1);

		inputForward = new CompoundAxis(new ButtonAxis(upKeyButtons, downKeyButtons), new JoystickAxis(0, 1));
		inputTurn = new CompoundAxis(new ButtonAxis(leftKeyButtons, rightKeyButtons), new JoystickAxis(0, 3));
		inputJump = new CompoundButton(jumpKeyButtons, jumpJoyButtons);
		inputBoost = new CompoundButton(boostKeyButtons, boostJoyButtons);
	}

	/**
	 * Creates a new PlayerComponent. From strings loaded from entity files.
	 *
	 * @param entity The entity this component is attached to.
	 * @param template The entity template to load data from.
	 */
	public PlayerComponent(Entity entity, EntityFileTemplate template) {
		this(entity, "Player666"); // TODO
	}

	/**
	 * @return Returns the username of the player.
	 */
	public String getUsername() {
		return username;
	}

	private void checkInputs() {
		currentSpeed = (float) (-RUN_SPEED * Maths.deadband(0.05f, inputForward.getAmount()));
		currentTurnSpeed = (float) (-TURN_SPEED * Maths.deadband(0.05f, inputTurn.getAmount()));

		if (inputBoost.isDown()) {
			currentSpeed *= 7.0f;
			currentTurnSpeed *= 5.0f;
		}

		if (inputJump.isDown()) {
			jump();
		}
	}

	private void jump() {
		if (!isInAir) {
			currentUpwardsSpeed = JUMP_POWER;
			isInAir = true;
		}
	}

	@Override
	public void update() {
		checkInputs();

		float distance = currentSpeed * EngineCore.getDeltaSeconds();
		float dx = (float) (distance * Math.sin(Math.toRadians(super.getEntity().getRotation().getY())));
		float dz = (float) (distance * Math.cos(Math.toRadians(super.getEntity().getRotation().getY())));
		float dy = currentUpwardsSpeed * EngineCore.getDeltaSeconds();
		float ry = currentTurnSpeed * EngineCore.getDeltaSeconds();
		currentUpwardsSpeed += World.GRAVITY * EngineCore.getDeltaSeconds();

		float groundHeight = World.getTerrainHeight(super.getEntity().getPosition().getX() + dx, super.getEntity().getPosition().getZ() + dz);

		if (super.getEntity().getPosition().getY() + dy < groundHeight) {
			dy = groundHeight - super.getEntity().getPosition().getY(); // Move back too the surface.
			currentUpwardsSpeed = 0;
			isInAir = false;
		}

		super.getEntity().move(new Vector3f(dx, dy, dz), new Vector3f(0, ry, 0));

		Terrain terrain = World.getTerrainAtPoint(super.getEntity().getPosition().getX(), super.getEntity().getPosition().getZ());

		if (terrain != null) {
			super.getEntity().switchStructure(terrain.getEntityQuadtree());
		}
	}

	@Override
	public String[] getSavableValues() {
		return new String[]{"Username: " + username};
	}
}
