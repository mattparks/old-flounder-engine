package engine.entities;

import engine.models.ModelTextured;
import engine.space.ISpatialStructure;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Vector3f;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

/**
 * Class that represents loaded .entity data.
 */
public class EntityFileTemplate {
	public static final MyFile ENTITIES_FOLDER = new MyFile(MyFile.RES_FOLDER, "engine/entities");

	private String entityName;
	private ModelTextured model;
	private Map<String, List<String>> componentsData;

	/**
	 * Creates a new template from loaded data.
	 *
	 * @param entityName The name of the loaded entity.
	 * @param model The optional model that can be used when setting up some components.
	 * @param componentsData A HashMap of loaded component data to be parsed when attaching the component to the new entity.
	 */
	public EntityFileTemplate(String entityName, ModelTextured model, Map<String, List<String>> componentsData) {
		this.entityName = entityName;
		this.model = model;
		this.componentsData = componentsData;
	}

	/**
	 * Creates a new entity from the template.
	 *
	 * @param structure The structure to place the entity into.
	 * @param position Initial world position.
	 * @param rotation Initial rotation.
	 *
	 * @return Returns a new entity created from the template.
	 */
	public Entity createEntity(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation) {
		Entity instance = new Entity(structure, position, rotation);

		for (String k : componentsData.keySet()) {
			try {
				Class componentClass = Class.forName(k);
				Class[] componentTypes = new Class[]{instance.getClass(), this.getClass()};
				@SuppressWarnings("unchecked") Constructor componentConstructor = componentClass.getConstructor(componentTypes);
				Object[] componentParameters = new Object[]{instance, this};
				componentConstructor.newInstance(componentParameters);
			} catch (ClassNotFoundException | IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException e) {
				System.out.println("While loading " + entityName + "'s components " + k + " could not be found!");
				e.printStackTrace();
			}
		}

		return instance;
	}

	/**
	 * @return Gets the loaded model.
	 */
	public ModelTextured getModel() {
		return model;
	}

	/**
	 * Gets the parsable data from the requested key.
	 *
	 * @param key The key to get data for (component class package and name).
	 *
	 * @return Returns a list of parsable data.
	 */
	public String[] getParseData(String key) {
		String[] componentParseData = new String[componentsData.get(key).size()];

		for (int i = 0; i < componentsData.get(key).size(); i++) {
			componentParseData[i] = componentsData.get(key).get(i);
		}

		return componentParseData;
	}
}
