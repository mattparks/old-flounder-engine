package engine.basics;

import engine.basics.options.OptionsGraphics;
import engine.devices.DeviceDisplay;
import engine.entities.EntityRenderer;
import engine.fonts.FontRenderer;
import engine.guis.GuiRenderer;
import engine.particles.ParticleRenderer;
import engine.planets.PlanetRenderer;
import engine.post.piplines.PipelineBloom;
import engine.post.piplines.PipelineDemo;
import engine.post.piplines.PipelineDepthOfField;
import engine.post.piplines.PipelineGaussian;
import engine.shadows.ShadowRenderer;
import engine.skyboxes.SkyboxRenderer;
import engine.terrains.TerrainRenderer;
import engine.textures.fbos.FBO;
import engine.textures.fbos.FBOBuilder;
import engine.toolbox.Colour;
import engine.toolbox.OpenglUtils;
import engine.toolbox.vector.Matrix4f;
import engine.toolbox.vector.Vector4f;
import engine.waters.WaterRenderer;
import engine.world.Environment;
import engine.world.World;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;

public class MasterRenderer3D extends IRendererMaster {
	private static final float REFRACTION_BIAS = 1f;

	private FBO multisamplingFBO;
	private FBO postProcessingFBO;
	private PipelineDemo pipelineDemo;
	private PipelineBloom pipelineBloom;
	private PipelineGaussian pipelineGaussian1;
	private PipelineGaussian pipelineGaussian2;
	private PipelineDepthOfField pipelineDepthOfField;

	private Colour clearColour;

	private Matrix4f projectionMatrix;
	private EntityRenderer entityRenderer;
	private ParticleRenderer particleRenderer;
	private TerrainRenderer terrainRenderer;
	private PlanetRenderer planetRenderer;
	private ShadowRenderer shadowMapRenderer;
	private WaterRenderer waterRenderer;
	private SkyboxRenderer skyboxRenderer;
	private FontRenderer fontRenderer;
	private GuiRenderer guiRenderer;

	@Override
	public void init() {
		multisamplingFBO = FBO.newFBO(DeviceDisplay.getWidth(), DeviceDisplay.getHeight()).fitToScreen().antialias(OptionsGraphics.MSAA_SAMPLES).create();
		postProcessingFBO = FBO.newFBO(DeviceDisplay.getWidth(), DeviceDisplay.getHeight()).fitToScreen().depthBuffer(FBOBuilder.DepthBufferType.TEXTURE).create();
		pipelineDemo = new PipelineDemo();
		pipelineBloom = new PipelineBloom();
		pipelineGaussian1 = new PipelineGaussian(720, 430, false);
		pipelineGaussian2 = new PipelineGaussian(DeviceDisplay.getWidth(), DeviceDisplay.getHeight(), true);
		pipelineDepthOfField = new PipelineDepthOfField();

		clearColour = new Colour(1, 1, 1);

		projectionMatrix = new Matrix4f();
		entityRenderer = new EntityRenderer();
		particleRenderer = new ParticleRenderer();
		terrainRenderer = new TerrainRenderer();
		planetRenderer = new PlanetRenderer();
		shadowMapRenderer = new ShadowRenderer();
		waterRenderer = new WaterRenderer();
		skyboxRenderer = new SkyboxRenderer();
		fontRenderer = new FontRenderer();
		guiRenderer = new GuiRenderer();
	}

	@Override
	public void render() {
		/* Render water buffers. */
		if (!Environment.getWaterObjects(EngineCore.getModule().getCamera()).isEmpty()) {
			GL11.glEnable(GL30.GL_CLIP_DISTANCE0);
			waterRenderer.bindRefractionFrameBuffer();
			renderScene(true, true, true, true, false, new Vector4f(0, -1, 0, World.WATER_Y_POS + REFRACTION_BIAS));
			waterRenderer.unbindCurrentFrameBuffer();

			waterRenderer.bindReflectionFrameBuffer();
			float reflectionDistance = 2 * (EngineCore.getModule().getCamera().getPosition().y - World.WATER_Y_POS);
			EngineCore.getModule().getCamera().reflect(World.WATER_Y_POS);
			EngineCore.getModule().getCamera().getPosition().setY(EngineCore.getModule().getCamera().getPosition().getY() - reflectionDistance);
			renderScene(true, true, true, true, false, new Vector4f(0, 1, 0, -World.WATER_Y_POS));
			EngineCore.getModule().getCamera().getPosition().setY(EngineCore.getModule().getCamera().getPosition().getY() + reflectionDistance);
			EngineCore.getModule().getCamera().reflect(World.WATER_Y_POS);
			waterRenderer.unbindCurrentFrameBuffer();
			GL11.glDisable(GL30.GL_CLIP_DISTANCE0);
		}

		/* Shadow rendering. */
		shadowMapRenderer.renderObjects(new Vector4f(0, 1, 0, Float.POSITIVE_INFINITY), EngineCore.getModule().getCamera());

		/* Scene rendering. */
		bindRelevantFBO();
		renderScene(true, true, true, true, true, new Vector4f(0, 1, 0, Float.POSITIVE_INFINITY));

		/* Post rendering. */
		boolean wireframe = OpenglUtils.isInWireframe();
		OpenglUtils.goWireframe(false);
		renderPost(EngineCore.getModule().getGame().isGamePaused(), EngineCore.getModule().getGame().getScreenBlur(), wireframe);

		/* Scene independents. */
		guiRenderer.render(null, null);
		fontRenderer.render(null, null);
		OpenglUtils.goWireframe(wireframe);

		/* Logging of results. */
		// System.out.println("FPS: " + (1.0 / ((double) DeviceDisplay.getDeltaSeconds())));
	}

	private void bindRelevantFBO() {
		if (OptionsGraphics.ANTI_ALIASING) {
			multisamplingFBO.bindFrameBuffer();
		} else {
			postProcessingFBO.bindFrameBuffer();
		}
	}

	private void renderScene(boolean renderSkybox, boolean renderEntities, boolean renderParticles, boolean renderTerrain, boolean renderWater, Vector4f clipPlane) {
		OpenglUtils.prepareNewRenderParse(clearColour);
		ICamera camera = EngineCore.getModule().getCamera();
		updatePerspectiveMatrix(camera.getFOV(), DeviceDisplay.getAspectRatio(), camera.getNearPlane(), camera.getFarPlane());

		if (renderSkybox) {
			skyboxRenderer.render(clipPlane, camera);
		}

		if (renderTerrain) {
			// planetRenderer.renderObjects(clipPlane, camera);
			terrainRenderer.render(clipPlane, camera);
		}

		if (renderWater) {
			waterRenderer.render(clipPlane, camera);
		}

		if (renderEntities) {
			entityRenderer.render(clipPlane, camera);
		}

		if (renderParticles) {
			particleRenderer.render(clipPlane, camera);
		}
	}

	private void renderPost(boolean isPaused, float blurFactor, boolean wireframe) {
		if (OptionsGraphics.ANTI_ALIASING) {
			multisamplingFBO.unbindFrameBuffer();
			multisamplingFBO.resolveMultisampledFBO(postProcessingFBO);
		} else {
			postProcessingFBO.unbindFrameBuffer();
		}

		FBO output = postProcessingFBO;

		if (!wireframe) {
			switch (OptionsGraphics.POST_EFFECT) {
				case 0:
					break;
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
					pipelineDemo.renderPipeline(postProcessingFBO);
					output = pipelineDemo.getOutput();
					break;
				case 8:
					pipelineBloom.renderPipeline(postProcessingFBO);
					output = pipelineBloom.getOutput();
					break;
				case 9:
					pipelineGaussian1.renderPipeline(postProcessingFBO);
					output = pipelineGaussian1.getOutput();
					break;
				case 10:
					pipelineDepthOfField.renderPipeline(postProcessingFBO);
					output = pipelineDepthOfField.getOutput();
					break;
			}

			if (isPaused) {
				pipelineGaussian1.setBlendSpreadValue(new Vector4f(blurFactor, 1, 0, 1));
				pipelineGaussian1.setScale(6.75f);
				pipelineGaussian1.renderPipeline(output);

				pipelineGaussian2.setBlendSpreadValue(new Vector4f(blurFactor, 1, 0, 1));
				pipelineGaussian2.setScale(4.25f);
				pipelineGaussian2.renderPipeline(pipelineGaussian1.getOutput());
				output = pipelineGaussian2.getOutput();
			}
		}

		output.blitToScreen();
	}

	private void updatePerspectiveMatrix(float fov, float aspectRatio, float zNear, float zFar) {
		projectionMatrix.setIdentity();
		float yScale = (float) (1f / Math.tan(Math.toRadians(fov / 2f)) * aspectRatio);
		float xScale = yScale / aspectRatio;
		float depth = zFar - zNear;

		projectionMatrix.m00 = xScale;
		projectionMatrix.m11 = yScale;
		projectionMatrix.m22 = -((zFar + zNear) / depth);
		projectionMatrix.m23 = -1; // Can be used in view distortion.
		projectionMatrix.m32 = -(2 * zNear * zFar / depth);
		projectionMatrix.m33 = 0;
	}

	@Override
	public void cleanUp() {
		multisamplingFBO.delete();
		postProcessingFBO.delete();
		pipelineDemo.cleanUp();
		pipelineBloom.cleanUp();
		pipelineGaussian1.cleanUp();
		pipelineGaussian2.cleanUp();
		pipelineDepthOfField.cleanUp();

		entityRenderer.cleanUp();
		particleRenderer.cleanUp();
		terrainRenderer.cleanUp();
		planetRenderer.cleanUp();
		shadowMapRenderer.cleanUp();
		waterRenderer.cleanUp();
		skyboxRenderer.cleanUp();
		fontRenderer.cleanUp();
		guiRenderer.cleanUp();
	}

	@Override
	public Matrix4f getProjectionMatrix() {
		return projectionMatrix;
	}

	/**
	 * @return Returns the shadow map renderer.
	 */
	public ShadowRenderer getShadowMapRenderer() {
		return shadowMapRenderer;
	}
}