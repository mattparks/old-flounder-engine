package engine.world;

import engine.basics.ICamera;
import engine.entities.Entity;
import engine.entities.components.SpriteComponent;
import engine.physics2.AABB2D;
import engine.physics2.QuadTree;
import engine.textures.Texture;
import engine.toolbox.Colour;
import engine.toolbox.vector.Vector2f;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Holds information about the engines world.
 */
public class Environment {
	private static final int spatialStructureTileSize = 16;
	private static final int spatialStructureTileCapacity = 8;
	private static final int spatialStructureWidth = 256;
	private static final int spatialStructureHeight = 256;

	private static QuadTree<Entity> genericEntityTree;

	private static Colour fogColour;

	/**
	 * Initializes the start game environment.
	 */
	public static void init() {
		genericEntityTree = new QuadTree<Entity>(new AABB2D(new Vector2f(0, 0), new Vector2f((spatialStructureTileSize * spatialStructureWidth), (spatialStructureTileSize * spatialStructureHeight))), spatialStructureTileCapacity);
		fogColour = new Colour(0, 0, 0);
	}

	/**
	 * Updates all engine.entities in the viewable terrain range.
	 */
	public static void updateEntities() {
		Set<Entity> entities = new HashSet<>();
		genericEntityTree.getAll(entities).forEach(Entity::update);
	}

	/**
	 * Creates a list of renderable entities.
	 *
	 * @param camera The games camera to query entities from.
	 *
	 * @return Returns a HashMap of sorted Textured Models and entities.
	 */
	public static Map<Texture, List<Entity>> getEntityObjects(ICamera camera) {
		Map<Texture, List<Entity>> list = new HashMap<>();
		Set<Entity> entities = new HashSet<>();
		genericEntityTree.getAll(entities);

		for (Entity e : entities) {
			SpriteComponent sp = (SpriteComponent) e.getComponent(SpriteComponent.ID);

			if (sp != null) {
				Texture entityModel = sp.getTexture();
				List<Entity> batch = list.get(entityModel);

				if (batch != null) {
					batch.add(e);
				} else {
					List<Entity> newBatch = new ArrayList<>();
					newBatch.add(e);
					list.put(entityModel, newBatch);
				}
			}
		}

		return list;
	}

	/**
	 * Carries out environment updates.
	 */
	public static void update() {
		Environment.updateEntities();
	}

	public static QuadTree<Entity> setGenericEntityTree() {
		return genericEntityTree;
	}

	public static Colour getFogColour() {
		return fogColour;
	}
}
