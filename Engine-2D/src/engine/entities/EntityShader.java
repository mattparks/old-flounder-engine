package engine.entities;

import engine.shaders.ShaderProgram;
import engine.shaders.UniformMat4;
import engine.toolbox.resources.MyFile;

public class EntityShader extends ShaderProgram {
	private static final MyFile VERTEX_SHADER = new MyFile("engine/entities", "entityVertex.glsl");
	private static final MyFile FRAGMENT_SHADER = new MyFile("engine/entities", "entityFragment.glsl");

	protected UniformMat4 modelMatrix = new UniformMat4("modelMatrix");
	protected UniformMat4 orthographicMatrix = new UniformMat4("orthographicMatrix");
	protected UniformMat4 viewMatrix = new UniformMat4("viewMatrix");

	protected EntityShader() {
		super(VERTEX_SHADER, FRAGMENT_SHADER);
		super.storeAllUniformLocations(modelMatrix, orthographicMatrix, viewMatrix);
	}
}
