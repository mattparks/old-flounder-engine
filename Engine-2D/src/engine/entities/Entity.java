package engine.entities;

import engine.basics.ICamera;
import engine.entities.components.CollisionComponent;
import engine.entities.components.RemoveComponent;
import engine.physics2.AABB2D;
import engine.physics2.ISpatialObject;
import engine.physics2.ISpatialStructure;
import engine.toolbox.vector.Vector2f;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Entity implements ISpatialObject, Comparable<Entity> {
	private static int currentId = 0;

	private ISpatialStructure<Entity> structure;
	private List<IEntityComponent> components;
	private List<IEntityComponent> componentsToRemove;
	private Vector2f position;
	private boolean isRemoved;
	private AABB2D aabb2D;
	private int id;

	/**
	 * Creates a new Entity with minimum necessary construction.
	 *
	 * @param structure The spatial structure this entity will be contained in.
	 * @param position The location of the entity.
	 */
	public Entity(ISpatialStructure<Entity> structure, Vector2f position) {
		this.structure = structure;
		this.components = new ArrayList<>();
		this.componentsToRemove = new ArrayList<>();
		this.position = position;
		this.isRemoved = false;
		this.aabb2D = new AABB2D(new Vector2f(0, 0), new Vector2f(0, 0));
		this.id = getNextID();
		this.structure.add(this);
	}

	private static int getNextID() {
		return currentId++;
	}

	public void update() {
		components.removeAll(componentsToRemove);
		componentsToRemove.clear();
		components.forEach(IEntityComponent::update);
	}

	/**
	 * Moves this entity by a certain amount. If this entity is a colliding entity and it hits another colliding entity when it moves, then this will only move the entity as far as it can without intersecting a colliding entity. This function only works on one axis at a time; one of the parameters must be 0.
	 *
	 * @param move The amount to move.
	 *
	 * @return The amount actually moved on X or Y, depending on which was specified.
	 *
	 * @throws IllegalArgumentException If movement is specified on both axis; this function only works on one axis at a time.
	 */
	public float move(Vector2f move) {
		structure.remove(this);
		float amountX = move.getX();
		float amountY = move.getY();

		CollisionComponent collision = (CollisionComponent) getComponent(CollisionComponent.ID);

		if (collision != null) {
			Vector2f amts = collision.resolveCollisions(new Vector2f(amountX, amountY));
			amountX = amts.getX();
			amountY = amts.getY();
		}

		position.set(position.getX() + amountX, position.getY() + amountY);
		structure.add(this);

		if (amountX != 0) {
			return amountX;
		} else {
			return amountY;
		}
	}

	/**
	 * Removes this entity from the spatial structure, and triggers any remove actions specified for this entity.
	 */
	public void remove() {
		if (isRemoved) {
			return;
		}

		//	AudioComponent audioComponent = (AudioComponent) getComponent(AudioComponent.ID);

		//	if (audioComponent != null) {
		//		audioComponent.play("remove");
		//	}

		isRemoved = true;
		RemoveComponent removeComponent = (RemoveComponent) getComponent(RemoveComponent.ID);

		//if (removeComponent != null) {
		//	removeComponent.activate();
		//} else {
		forceRemove();
		//}
	}

	/**
	 * Forcibly removes this entity from the spatial structure without triggering remove actions. Use with caution; this function may fail or cause errors if used inappropriately.
	 */
	public void forceRemove() {
		isRemoved = true;
		structure.remove(this);
	}

	/**
	 * Ensures the bounds of this entity are at least big enough to contain {@code newAABB2D}.
	 *
	 * @param newAABB2D The AABB2D this entity must be able to contain.
	 */
	public void fitAABB(AABB2D newAABB2D) {
		structure.remove(this);

		if (aabb2D.getWidth() == 0.0 && aabb2D.getHeight() == 0.0) {
			aabb2D = newAABB2D;
		} else {
			aabb2D = aabb2D.combine(newAABB2D);
		}

		structure.add(this);
	}

	/**
	 * Moves an AABB2D into the position of this entity.
	 *
	 * @param aabb2D The AABB2D to be translated.
	 *
	 * @return An AABB2D translated into the position of this entity.
	 */
	public AABB2D translateAABB(AABB2D aabb2D) {
		return aabb2D.move(position);
	}

	/**
	 * Finds and returns a component attached to this entity by id. If more than one is found, the first component in the list is returned. If none are found, returns null.
	 *
	 * @param id The id of the component. This is typically found with ComponentClass.ID.
	 *
	 * @return The first component found with the given id, or null if none are found.
	 */
	public IEntityComponent getComponent(int id) {
		for (IEntityComponent component : components) {
			if (component.getId() == id) {
				return component;
			}
		}

		return null;
	}

	/**
	 * Visits every entity with a particular component within a certain range of space.
	 *
	 * @param id The id of the component. This is typically found with ComponentClass.ID. If no particular component is desired, specify -1.
	 * @param range The range of space to be visited.
	 * @param visitor The visitor that will be executed for every entity visited.
	 */
	public void visitInRange(int id, AABB2D range, IEntityVisitor visitor) {
		Set<Entity> entities = structure.queryRange(new HashSet<Entity>(), range);
		Iterator<Entity> it = entities.iterator();

		while (it.hasNext()) {
			Entity entity = it.next();

			if (entity.isRemoved) {
				continue;
			}

			IEntityComponent component = id == -1 ? null : entity.getComponent(id);

			if (component != null || id == -1) {
				visitor.visit(entity, component);
			}
		}
	}

	/**
	 * Adds a new component to the entity.
	 *
	 * @param component The component to add.
	 */
	public void addComponent(IEntityComponent component) {
		components.add(component);
	}

	/**
	 * Removes a component to the entity.
	 *
	 * @param component The component to remove.
	 */
	public void removeComponent(IEntityComponent component) {
		components.remove(component);
	}

	/**
	 * Removes a component from this entity by id. If more than one is found, the first component in the list is removed. If none are found, nothing is removed.
	 *
	 * @param id The id of the component. This is typically found with ComponentClass.ID.
	 */
	public void removeComponent(int id) {
		for (IEntityComponent c : components) {
			if (c.getId() == id) {
				components.remove(c);
				return;
			}
		}
	}

	public boolean isRenderable(ICamera camera) {
		return true; // TODO: Use 2D bounds to determine if renderable!
	}

	/**
	 * Gets whether or not this entity has been removed from the spatial structure.
	 *
	 * @return Whether or not this entity has been removed from the spatial structure.
	 */
	public boolean isRemoved() {
		return isRemoved;
	}

	public Vector2f getPosition() {
		return position;
	}

	public void setPosition(Vector2f position) {
		this.position = position;
	}

	@Override
	public AABB2D getAABB() {
		return translateAABB(aabb2D);
	}

	@Override
	public int compareTo(Entity other) {
		if (id > other.id) {
			return 1;
		}

		if (id < other.id) {
			return -1;
		}

		return 0;
	}
}
