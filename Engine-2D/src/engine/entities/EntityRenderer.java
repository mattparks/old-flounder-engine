package engine.entities;

import engine.basics.EngineCore;
import engine.basics.ICamera;
import engine.basics.IRenderer;
import engine.basics.options.OptionsGraphics;
import engine.loaders.Loader;
import engine.textures.Texture;
import engine.toolbox.Maths;
import engine.toolbox.OpenglUtils;
import engine.toolbox.vector.Vector4f;
import engine.world.Environment;
import org.lwjgl.opengl.GL11;

import java.util.List;
import java.util.Map;

public class EntityRenderer extends IRenderer {
	private static final float[] POSITIONS = {0, 0, 0, 1, 1, 0, 1, 1};
	private static final int VAO = Loader.createInterleavedVAO(POSITIONS, 2);

	private EntityShader shader;

	public EntityRenderer() {
		shader = new EntityShader();
	}

	public void renderObjects(Vector4f clipPlane, ICamera camera) {
		prepareRendering(camera);

		Map<Texture, List<Entity>> entities = Environment.getEntityObjects(camera);

		for (Texture texture : entities.keySet()) {
			prepareTextureSheet(texture);

			for (Entity e : entities.get(texture)) {
				prepareInstance(e);
				//	GL11.glDrawElements(GL11.GL_TRIANGLES, POSITIONS.length, GL11.GL_UNSIGNED_INT, 0); // Render the entity instance.
				GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, POSITIONS.length); // Render the entity instance.
			}

			unbindTextureSheet();
		}

		endRendering();
	}

	private void prepareRendering(ICamera camera) {
		shader.start();
		shader.orthographicMatrix.loadMat4(EngineCore.getModule().getRendererMaster().getProjectionMatrix());
		shader.viewMatrix.loadMat4(camera.getViewMatrix());
	}

	private void prepareTextureSheet(Texture texture) {
		OpenglUtils.bindVAO(VAO, 0);
		OpenglUtils.antialias(OptionsGraphics.ANTI_ALIASING);
		OpenglUtils.enableDepthTesting();
		OpenglUtils.enableAlphaBlending();

		OpenglUtils.bindTextureToBank(texture.getTextureID(), 0);
	}

	private void prepareInstance(Entity entity) {
		shader.modelMatrix.loadMat4(Maths.createTransformationMatrix(entity.getPosition(), 1.0f));
		OpenglUtils.cullBackFaces(false);
	}

	private void unbindTextureSheet() {
		OpenglUtils.unbindVAO(0);
	}

	private void endRendering() {
		shader.stop();
	}

	public void cleanUp() {
		shader.cleanUp();
	}
}
