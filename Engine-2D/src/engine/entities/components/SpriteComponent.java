package engine.entities.components;

import engine.entities.Entity;
import engine.entities.EntityAssignerID;
import engine.entities.IEntityComponent;
import engine.textures.Texture;
import engine.toolbox.Colour;
import engine.toolbox.vector.Vector2f;

public class SpriteComponent extends IEntityComponent {
	public static final int ID = EntityAssignerID.getId();

	private final Texture texture;

	private double transparency;
	private Vector2f spriteFlipOffset;
	private boolean flipX;
	private boolean flipY;
	private double scale;
	private Colour colour;

	public SpriteComponent(Entity entity, Texture texture) {
		super(entity, ID);
		this.texture = texture;
	}

	public Texture getTexture() {
		return texture;
	}

	@Override
	public void update() {
	}

	@Override
	public String[] getSavableValues() {
		return new String[]{};
	}
}
