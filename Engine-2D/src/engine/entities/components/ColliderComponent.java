package engine.entities.components;


import engine.entities.Entity;
import engine.entities.EntityAssignerID;
import engine.entities.IEntityComponent;
import engine.physics2.AABB2D;

/**
 * Gives an object a collider for spatial interaction. Note that a collider doesn't necessarily need to be used for collision. A collider component can be used for any spatial interaction.
 * <p>
 * For example, a checkpoint can use a ColliderComponent to detect when the player has reached it.
 */
public class ColliderComponent extends IEntityComponent {
	public static final int ID = EntityAssignerID.getId();
	private AABB2D aabb2D;

	/**
	 * Creates a new ColliderComponent.
	 *
	 * @param entity The entity this component is attached to.
	 */
	public ColliderComponent(Entity entity) {
		super(entity, ID);
		this.aabb2D = null;
	}

	/**
	 * Ensures the bounds of the collider are at least big enough to contain {@code other}.
	 *
	 * @param other The AABB2D this collider must be able to contain.
	 */
	public void fitAABB(AABB2D other) {
		if (aabb2D == null) {
			aabb2D = other;
		} else {
			aabb2D = aabb2D.combine(other);
		}

		getEntity().fitAABB(aabb2D);
	}

	/**
	 * Gets the AABB2D representing the collision range.
	 *
	 * @return An AABB2D representing the collision range.
	 */
	public AABB2D getAABB() {
		return getEntity().translateAABB(aabb2D);
	}

	@Override
	public void update() {
	}

	@Override
	public String[] getSavableValues() {
		return new String[]{};
	}
}
