package engine.entities.components;

import engine.entities.Entity;
import engine.entities.EntityAssignerID;
import engine.entities.IEntityComponent;
import engine.entities.IEntityVisitor;
import engine.physics2.AABB2D;
import engine.toolbox.vector.Vector2f;

/**
 * Component that detects collision between two entities.
 * <p>
 * Note: this component requires that both entities have a ColliderComponent. Should one entity not have a ColliderComponent, then no collisions will be detected, because there is no collider to detect collisions against.
 */
public class CollisionComponent extends IEntityComponent {
	public static final int ID = EntityAssignerID.getId();

	/**
	 * Creates a new CollisionComponent.
	 *
	 * @param entity The entity this component is attached to.
	 */
	public CollisionComponent(Entity entity) {
		super(entity, ID);
	}

	/**
	 * Resolves collisions with any other CollisionComponents encountered.
	 *
	 * @param amount The amount attempting to be moved.
	 *
	 * @return New X, Y and Z that will not cause collisions after movement.
	 */
	public Vector2f resolveCollisions(Vector2f amount) {
		final Vector2f result = new Vector2f(amount.getX(), amount.getY());
		ColliderComponent c = (ColliderComponent) getEntity().getComponent(ColliderComponent.ID);

		if (c == null) {
			return result;
		}

		final AABB2D collider = c.getAABB();
		final AABB2D collisionRange = collider.stretch(amount);

		getEntity().visitInRange(CollisionComponent.ID, collisionRange, new IEntityVisitor() {
			@Override
			public void visit(Entity entity, IEntityComponent component) {
				if (entity == getEntity()) {
					return;
				}

				ColliderComponent c2 = (ColliderComponent) entity.getComponent(ColliderComponent.ID);

				if (c2 == null) {
					return;
				}

				AABB2D collider2 = c2.getAABB();

				if (collider2.intersects(collisionRange)) {
					result.setX((float) collider.resolveCollisionX(collider2, result.getX()));
					result.setY((float) collider.resolveCollisionY(collider2, result.getY()));
				}
			}
		});

		return result;
	}

	@Override
	public void update() {
	}

	@Override
	public String[] getSavableValues() {
		return new String[]{};
	}
}
