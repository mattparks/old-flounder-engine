package engine.basics;

import engine.basics.options.OptionsGraphics;
import engine.devices.DeviceDisplay;
import engine.entities.EntityRenderer;
import engine.fonts.FontRenderer;
import engine.guis.GuiRenderer;
import engine.post.piplines.PipelineDemo;
import engine.post.piplines.PipelineGaussian;
import engine.textures.fbos.FBO;
import engine.textures.fbos.FBOBuilder;
import engine.toolbox.Colour;
import engine.toolbox.OpenglUtils;
import engine.toolbox.vector.Matrix4f;
import engine.toolbox.vector.Vector4f;

public class MasterRenderer2D extends IRendererMaster {
	private FBO multisamplingFBO;
	private FBO postProcessingFBO;
	private PipelineDemo pipelineDemo;
	private PipelineGaussian pipelineGaussian1;
	private PipelineGaussian pipelineGaussian2;

	private Colour clearColour;

	private Matrix4f projectionMatrix;
	private EntityRenderer entityRenderer;
	private FontRenderer fontRenderer;
	private GuiRenderer guiRenderer;

	@Override
	public void init() {
		multisamplingFBO = FBO.newFBO(DeviceDisplay.getWidth(), DeviceDisplay.getHeight()).fitToScreen().antialias(OptionsGraphics.MSAA_SAMPLES).create();
		postProcessingFBO = FBO.newFBO(DeviceDisplay.getWidth(), DeviceDisplay.getHeight()).fitToScreen().depthBuffer(FBOBuilder.DepthBufferType.TEXTURE).create();
		pipelineDemo = new PipelineDemo();
		pipelineGaussian1 = new PipelineGaussian(720, 430, false);
		pipelineGaussian2 = new PipelineGaussian(DeviceDisplay.getWidth(), DeviceDisplay.getHeight(), true);

		clearColour = new Colour(1, 1, 1);

		projectionMatrix = new Matrix4f();
		entityRenderer = new EntityRenderer();
		fontRenderer = new FontRenderer();
		guiRenderer = new GuiRenderer();
	}

	@Override
	public void render() {
		/* Scene rendering. */
		bindRelevantFBO();
		renderScene();

		/* Post rendering. */
		boolean wireframe = OpenglUtils.isInWireframe();
		OpenglUtils.goWireframe(false);
		renderPost(EngineCore.getModule().getGame().isGamePaused(), EngineCore.getModule().getGame().getScreenBlur(), wireframe);

		/* Scene independents. */
		guiRenderer.render(null, null);
		fontRenderer.render(null, null);
		OpenglUtils.goWireframe(wireframe);

		/* Logging of results. */
		// System.out.println("FPS: " + (1.0 / ((double) DeviceDisplay.getDeltaSeconds())));
	}

	private void bindRelevantFBO() {
		if (OptionsGraphics.ANTI_ALIASING) {
			multisamplingFBO.bindFrameBuffer();
		} else {
			postProcessingFBO.bindFrameBuffer();
		}
	}

	private void renderScene() {
		OpenglUtils.prepareNewRenderParse(clearColour);
		ICamera camera = EngineCore.getModule().getCamera();
		updateOrthographicMatrix(-10.0f, 10.0f, -10.0f * 9.0f / 16.0f, 10.0f * 9.0f / 16.0f, -10.0f, 10.0f); // 0, DeviceDisplay.getWidth(), 0, DeviceDisplay.getHeight(), -1, 1 // -400, 400, -300, 300, 0.1f, 1.1f

		entityRenderer.render(null, camera);
	}

	private void renderPost(boolean isPaused, float blurFactor, boolean wireframe) {
		if (OptionsGraphics.ANTI_ALIASING) {
			multisamplingFBO.unbindFrameBuffer();
			multisamplingFBO.resolveMultisampledFBO(postProcessingFBO);
		} else {
			postProcessingFBO.unbindFrameBuffer();
		}

		FBO output = postProcessingFBO;

		if (!wireframe) {
			switch (OptionsGraphics.POST_EFFECT) {
				case 0:
					break;
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
					pipelineDemo.renderPipeline(postProcessingFBO);
					output = pipelineDemo.getOutput();
					break;
				case 8:
				case 9:
				case 10:
					pipelineGaussian1.renderPipeline(postProcessingFBO);
					output = pipelineGaussian1.getOutput();
					break;
			}

			if (isPaused) {
				pipelineGaussian1.setBlendSpreadValue(new Vector4f(blurFactor, 1, 0, 1));
				pipelineGaussian1.setScale(6.75f);
				pipelineGaussian1.renderPipeline(output);

				pipelineGaussian2.setBlendSpreadValue(new Vector4f(blurFactor, 1, 0, 1));
				pipelineGaussian2.setScale(4.25f);
				pipelineGaussian2.renderPipeline(pipelineGaussian1.getOutput());
				output = pipelineGaussian2.getOutput();
			}
		}

		output.blitToScreen();
	}

	private void updateOrthographicMatrix(float left, float right, float bottom, float top, float near, float far) {
		projectionMatrix.setIdentity();
		float xOrtho = 2 / (right - left);
		float yOrtho = 2 / (top - bottom);
		float zOrtho = -2 / (far - near);

		float tx = -(right + left) / (right - left);
		float ty = -(top + bottom) / (top - bottom);
		float tz = -(far + near) / (far - near);

		projectionMatrix.m00 = xOrtho;
		projectionMatrix.m11 = yOrtho;
		projectionMatrix.m22 = zOrtho;
		projectionMatrix.m03 = tx;
		projectionMatrix.m13 = ty;
		projectionMatrix.m23 = tz;
		projectionMatrix.m33 = 1;
	}

	@Override
	public void cleanUp() {
		multisamplingFBO.delete();
		postProcessingFBO.delete();
		pipelineDemo.cleanUp();
		pipelineGaussian1.cleanUp();
		pipelineGaussian2.cleanUp();

		entityRenderer.cleanUp();
		fontRenderer.cleanUp();
		guiRenderer.cleanUp();
	}

	@Override
	public Matrix4f getProjectionMatrix() {
		return projectionMatrix;
	}
}