package engine.physics2;

import engine.toolbox.Maths;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Grid<T extends ISpatialObject> implements ISpatialStructure<T> {
	private final int tileSize;
	private final int width;
	private final int height;
	private final List<T>[] tiles;

	/**
	 * Creates a new grid.
	 *
	 * @param tileSize How much space each tile in the grid consumes
	 * @param width How many tiles there are on x.
	 * @param height How many tiles there are on y.
	 */
	@SuppressWarnings("unchecked")
	public Grid(int tileSize, int width, int height) {
		this.tileSize = tileSize;
		this.width = width;
		this.height = height;
		this.tiles = new List[width * height];

		for (int i = 0; i < tiles.length; i++) {
			tiles[i] = new ArrayList<T>();
		}
	}

	private int getGridPosMin(double pos) {
		return (int) Math.floor(pos / tileSize);
	}

	private int getGridPosMax(double pos) {
		return (int) Math.ceil(pos / tileSize);
	}

	private int getTileX(int x) {
		return Maths.floorMod(x, width);
	}

	private int getTileY(int y) {
		return Maths.floorMod(y, height);
	}

	private List<T> getTile(int x, int y) {
		return tiles[getTileX(x) + getTileY(y) * width];
	}

	private void visit(final AABB2D aabb, final IVisitor<T> visitor) {
		int minX, minY, maxX, maxY;

		if (aabb.getWidth() > width * tileSize) {
			minX = 0;
			maxX = width - 1;
		} else {
			minX = getGridPosMin(aabb.getMinPosition().getX());
			maxX = getGridPosMax(aabb.getMaxPosition().getX());
		}

		if (aabb.getHeight() > height * tileSize) {
			minY = 0;
			maxY = height - 1;
		} else {
			minY = getGridPosMin(aabb.getMinPosition().getY());
			maxY = getGridPosMax(aabb.getMaxPosition().getY());
		}

		for (int j = minY; j <= maxY; j++) {
			for (int i = minX; i <= maxX; i++) {
				visitor.onVisit(getTile(i, j));
			}
		}
	}

	@Override
	public void add(final T object) {
		visit(object.getAABB(), new IVisitor<T>() {
			@Override
			public void onVisit(List<T> tile) {
				tile.add(object);
			}
		});
	}

	@Override
	public void remove(final T object) {
		visit(object.getAABB(), new IVisitor<T>() {
			@Override
			public void onVisit(List<T> tile) {
				tile.remove(object);
			}
		});
	}

	@Override
	public Set<T> getAll(Set<T> result) {
		return result;
	}

	private Set<T> queryTile(List<T> tile, Set<T> result, AABB2D aabb) {
		Iterator<T> it = tile.iterator();

		while (it.hasNext()) {
			T t = it.next();

			if (t.getAABB().intersects(aabb)) {
				result.add(t);
			}
		}
		return result;
	}

	@Override
	public Set<T> queryRange(final Set<T> result, final AABB2D aabb) {
		visit(aabb, new IVisitor<T>() {
			@Override
			public void onVisit(List<T> tile) {
				queryTile(tile, result, aabb);
			}
		});

		return result;
	}

	@Override
	public void clear() {
		for (int i = 0; i < tiles.length; i++) {
			tiles[i].clear();
		}
	}

	private interface IVisitor<T> {
		public void onVisit(List<T> tile);
	}
}
