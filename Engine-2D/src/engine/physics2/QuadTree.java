package engine.physics2;

import engine.toolbox.vector.Vector2f;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Represents a 2D space that can be recursively divided into 4 equal subspaces.
 */
public class QuadTree<T extends ISpatialObject> implements ISpatialStructure<T> {
	private QuadTree<T> nodes[];
	private int capacity;
	private List<T> objects;
	private AABB2D aabb;

	/**
	 * Initializes a QuadTree from an AABB2D.
	 *
	 * @param aabb Represents the 2D space inside the QuadTree.
	 * @param capacity The number of objects that can be added to the QuadTree before it subdivides.
	 */
	public QuadTree(AABB2D aabb, int capacity) {
		this.aabb = aabb;
		this.capacity = capacity;
		objects = new ArrayList<>();
		nodes = null;
	}

	private QuadTree(QuadTree<T> other) {
		this.nodes = other.nodes;
		this.objects = other.objects;
		this.capacity = other.capacity;
		this.aabb = other.aabb;
	}

	@Override
	public void add(T object) {
		if (addInternal(object)) {
			return;
		}

		float dirX = (float) (object.getAABB().getCenterX() - aabb.getCenterX());
		float dirY = (float) (object.getAABB().getCenterY() - aabb.getCenterY());
		expand(dirX, dirY);
		add(object);
	}

	private void expand(float expandX, float expandY) {
		QuadTree<T> thisAsNode = new QuadTree<>(this);

		float minX = aabb.getMinPosition().getX();
		float minY = aabb.getMinPosition().getY();
		float maxX = aabb.getMaxPosition().getX();
		float maxY = aabb.getMaxPosition().getY();

		float expanseX = maxX - minX;
		float expanseY = maxY - minY;

		nodes = null;
		objects = new ArrayList<>();

		if (expandX <= 0 && expandY <= 0) {
			aabb = new AABB2D(new Vector2f(minX - expanseX, minY - expanseY), new Vector2f(maxX, maxY));
			subdivide();
			nodes[1] = thisAsNode;
		} else if (expandX <= 0 && expandY > 0) {
			aabb = new AABB2D(new Vector2f(minX - expanseX, minY), new Vector2f(maxX, maxY + expanseY));
			subdivide();
			nodes[3] = thisAsNode;
		} else if (expandX > 0 && expandY > 0) {
			aabb = new AABB2D(new Vector2f(minX, minY), new Vector2f(maxX + expanseX, maxY + expanseY));
			subdivide();
			nodes[2] = thisAsNode;
		} else if (expandX > 0 && expandY <= 0) {
			aabb = new AABB2D(new Vector2f(minX, minY - expanseY), new Vector2f(maxX + expanseX, maxY));
			subdivide();
			nodes[0] = thisAsNode;
		} else {
			throw new AssertionError("Error: QuadTree direction is invalid (?): " + expandX + " (dirX) " + expandY + " (dirY)");
		}
	}

	private boolean addInternal(T object) {
		if (!aabb.contains(object.getAABB())) {
			return false;
		}

		if (nodes == null) {
			if (objects.size() < capacity) {
				objects.add(object);
				return true;
			}

			subdivide();
		}

		if (!addToChildNode(object)) {
			objects.add(object);
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	private void subdivide() {
		float minX = aabb.getMinPosition().getX();
		float minY = aabb.getMinPosition().getY();
		float maxX = aabb.getMaxPosition().getX();
		float maxY = aabb.getMaxPosition().getY();

		float halfXLength = (maxX - minX) / 2.0f;
		float halfYLength = (maxY - minY) / 2.0f;

		nodes = (new QuadTree[4]);

		minY += halfYLength;
		maxX -= halfXLength;
		nodes[0] = new QuadTree<T>(new AABB2D(new Vector2f(minX, minY), new Vector2f(maxX, maxY)), capacity);

		minX += halfXLength;
		maxX += halfXLength;
		nodes[1] = new QuadTree<T>(new AABB2D(new Vector2f(minX, minY), new Vector2f(maxX, maxY)), capacity);

		minY -= halfYLength;
		maxY -= halfYLength;
		nodes[3] = new QuadTree<T>(new AABB2D(new Vector2f(minX, minY), new Vector2f(maxX, maxY)), capacity);

		minX -= halfXLength;
		maxX -= halfXLength;
		nodes[2] = new QuadTree<T>(new AABB2D(new Vector2f(minX, minY), new Vector2f(maxX, maxY)), capacity);

		reinsertObjects();
	}

	private void reinsertObjects() {
		Iterator<T> it = objects.iterator();

		while (it.hasNext()) {
			if (addToChildNode(it.next())) {
				it.remove();
			}
		}
	}

	private boolean addToChildNode(T object) {
		for (int i = 0; i < nodes.length; i++) {
			if (nodes[i].addInternal(object)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public void remove(T object) {
		if (!removeInternal(object)) {
			objects.remove(object);
		}
	}

	private boolean removeInternal(T object) {
		if (!aabb.contains(object.getAABB())) {
			return false;
		}

		if (objects.remove(object)) {
			return true;
		}

		if (nodes == null) {
			return false;
		}

		for (int i = 0; i < nodes.length; i++) {
			if (nodes[i].removeInternal(object)) {
				prune();
				return true;
			}
		}

		return false;
	}

	private void prune() {
		if (!isNodesEmpty()) {
			return;
		}

		nodes = null;
	}

	private boolean isNodesEmpty() {
		for (int i = 0; i < nodes.length; i++) {
			if (!nodes[i].isEmpty()) {
				return false;
			}
		}

		return true;
	}

	private boolean isEmpty() {
		if (!objects.isEmpty()) {
			return false;
		}

		if (nodes == null) {
			return true;
		}

		return isNodesEmpty();
	}

	@Override
	public void clear() {
		objects.clear();

		if (nodes != null) {
			for (int i = 0; i < nodes.length; i++) {
				nodes[i].clear();
			}
		}

		nodes = null;
	}

	@Override
	public Set<T> getAll(Set<T> result) {
		return addAll(result);
	}

	private Set<T> addAll(Set<T> result) {
		result.addAll(objects);

		if (nodes != null) {
			for (int i = 0; i < nodes.length; i++) {
				nodes[i].addAll(result);
			}
		}

		return result;
	}

	@Override
	public Set<T> queryRange(Set<T> result, AABB2D range) {
		if (!aabb.intersects(range)) {
			return result;
		}

		if (range.contains(aabb)) {
			return addAll(result);
		}

		if (nodes != null) {
			for (int i = 0; i < nodes.length; i++) {
				result = nodes[i].queryRange(result, range);
			}
		}

		Iterator<T> it = objects.iterator();

		while (it.hasNext()) {
			T current = it.next();

			if (current.getAABB().intersects(range)) {
				result.add(current);
			}
		}

		return result;
	}
}
