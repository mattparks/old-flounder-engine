package engine.physics2;

/**
 * Represents an object that has some notion of space, and can be stored in a {@link ISpatialStructure}.
 */
public interface ISpatialObject {
	public AABB2D getAABB();
}
