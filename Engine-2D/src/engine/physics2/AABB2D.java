package engine.physics2;

import engine.toolbox.vector.Vector2f;

public class AABB2D {
	private static final float DEFAULT_MIN_Z = Float.NEGATIVE_INFINITY;
	private static final float DEFAULT_MAX_Z = Float.POSITIVE_INFINITY;

	private final Vector2f minPosition;
	private final Vector2f maxPosition;

	/**
	 * Creates a new 2d AABB2D based on it's extents.
	 *
	 * @param minPos The minimum extent of the box.
	 * @param maxPos The minimum extent of the box.
	 */
	public AABB2D(Vector2f minPos, Vector2f maxPos) {
		this.minPosition = minPos;
		this.maxPosition = maxPos;
	}

	/**
	 * Tests whether another AABB2D is completely contained by this one.
	 *
	 * @param other The AABB2D being tested for containment
	 *
	 * @return True if {@code other} is contained by this AABB2D, false otherwise.
	 */
	public boolean contains(AABB2D other) {
		return minPosition.getX() <= other.minPosition.getX() && other.maxPosition.getX() <= maxPosition.getX() && minPosition.getY() <= other.minPosition.getY() && other.maxPosition.getY() <= maxPosition.getY();
	}

	/**
	 * Tests whether another AABB2D is intersecting this one.
	 *
	 * @param other The AABB2D being tested for intersection
	 *
	 * @return True if {@code other} is intersecting this AABB2D, false otherwise.
	 */
	public boolean intersects(AABB2D other) {
		return intersectCube(other.getMinPosition(), other.getMaxPosition());
	}

	/**
	 * Tests whether this AABB2D intersects a rectangle.
	 *
	 * @param minPosition The minimum extent of the rectangle.
	 * @param maxPosition The maximum extent of the rectangle.
	 *
	 * @return True if the rectangle intersects this AABB2D, false otherwise.
	 */
	public boolean intersectRectangle(Vector2f minPosition, Vector2f maxPosition) {
		return intersectCube(new Vector2f(minPosition.getX(), minPosition.getY()), new Vector2f(maxPosition.getX(), maxPosition.getY()));
	}

	/**
	 * Tests whether this AABB2D intersects a cube.
	 *
	 * @param minPosition The minimum extent of the cube.
	 * @param maxPosition The maximum extent of the cube.
	 *
	 * @return True if the cube intersects this AABB2D, false otherwise.
	 */
	public boolean intersectCube(Vector2f minPosition, Vector2f maxPosition) {
		return this.minPosition.getX() < maxPosition.getX() && this.maxPosition.getX() > minPosition.getX() && this.minPosition.getY() < maxPosition.getY() && this.maxPosition.getY() > minPosition.getY();
	}

	/**
	 * Adjusts a movement amount on X so that after the move is performed, this AABB2D will not intersect {@code other}.
	 * <p>
	 * This method assumes that this AABB2D can actually intersect {@code other} after some amount of movement on x, even if it won't necessarily intersect it after the movement specified by {@code moveAmtX}.
	 *
	 * @param other The AABB2D that this AABB2D is resolving against.
	 * @param moveAmountX The amount this AABB2D is trying to move.
	 *
	 * @return The new, adjusted move amount that guarantees no intersection.
	 */
	public double resolveCollisionX(AABB2D other, double moveAmountX) {
		double newAmtX = moveAmountX;

		if (moveAmountX == 0.0) {
			return moveAmountX;
		}

		if (moveAmountX > 0) { // This max == other min
			newAmtX = other.getMinPosition().getX() - maxPosition.getX();
		} else { // This min == other max
			newAmtX = other.getMaxPosition().getX() - minPosition.getX();
		}

		if (Math.abs(newAmtX) < Math.abs(moveAmountX)) {
			moveAmountX = newAmtX;
		}
		return moveAmountX;
	}

	/**
	 * Adjusts a movement amount on Y so that after the move is performed, this AABB2D will not intersect {@code other}.
	 * <p>
	 * This method assumes that this AABB2D can actually intersect {@code other} after some amount of movement on y, even if it won't necessarily intersect it after the movement specified by {@code moveAmtY}.
	 *
	 * @param other The AABB2D that this AABB2D is resolving against.
	 * @param moveAmounttY The amount this AABB2D is trying to move.
	 *
	 * @return The new, adjusted move amount that guarantees no intersection.
	 */
	public double resolveCollisionY(AABB2D other, double moveAmounttY) {
		double newAmtY = moveAmounttY;

		if (moveAmounttY == 0.0) {
			return moveAmounttY;
		}

		if (moveAmounttY > 0) { // This max == other min.
			newAmtY = other.getMinPosition().getY() - maxPosition.getY();
		} else { // This min == other max.
			newAmtY = other.getMaxPosition().getY() - minPosition.getY();
		}

		if (Math.abs(newAmtY) < Math.abs(moveAmounttY)) {
			moveAmounttY = newAmtY;
		}

		return moveAmounttY;
	}

	/**
	 * Calculates the center of this AABB2D on the X axis.
	 *
	 * @return The center location of this AABB2D on the X axis.
	 */
	public double getCenterX() {
		return (minPosition.getX() + maxPosition.getX()) / 2.0;
	}

	/**
	 * Calculates the center of this AABB2D on the Y axis.
	 *
	 * @return The center location of this AABB2D on the Y axis.
	 */
	public double getCenterY() {
		return (minPosition.getY() + maxPosition.getY()) / 2.0;
	}

	/**
	 * Gets the width of this AABB2D.
	 *
	 * @return The width of this AABB2D.
	 */
	public double getWidth() {
		return maxPosition.getX() - minPosition.getX();
	}

	/**
	 * Gets the height of this AABB2D.
	 *
	 * @return The height of this AABB2D.
	 */
	public double getHeight() {
		return maxPosition.getY() - minPosition.getY();
	}

	/**
	 * Gets the minimum extent of this AABB2D.
	 *
	 * @return The minimum extent of this AABB2D.
	 */
	public Vector2f getMinPosition() {
		return minPosition;
	}

	/**
	 * Gets the maximum extent of this AABB2D.
	 *
	 * @return The maximum extent of this AABB2D.
	 */
	public Vector2f getMaxPosition() {
		return maxPosition;
	}

	/**
	 * Creates a new AABB2D equivalent to this, but scaled away from the origin by a certain amount.
	 *
	 * @param expand Amount to scale up the AABB2D.
	 *
	 * @return A new AABB2D, scaled by the specified amounts.
	 */
	public AABB2D expand(Vector2f expand) {
		return new AABB2D(new Vector2f(minPosition.getX() - expand.getX(), minPosition.getY() - expand.getY()), new Vector2f(maxPosition.getX() + expand.getX(), maxPosition.getY() + expand.getY()));
	}

	/**
	 * Creates a new AABB2D equivalent to this, but scaled away from the upper left most origin.
	 *
	 * @param scale Amount to scale up the AABB2D.
	 *
	 * @return A new AABB2D, scaled by the specified amounts.
	 */
	public AABB2D scale(Vector2f scale) {
		return new AABB2D(minPosition, new Vector2f(maxPosition.getX() * scale.getX(), maxPosition.getY() * scale.getY()));
	}

	/**
	 * Creates an AABB2D equivalent to this, but in a new position.
	 *
	 * @param move The amount to move.
	 *
	 * @return An AABB2D equivalent to this, but in a new position.
	 */
	public AABB2D move(Vector2f move) {
		return new AABB2D(new Vector2f(minPosition.getX() + move.getX(), minPosition.getY() + move.getY()), new Vector2f(maxPosition.getX() + move.getX(), maxPosition.getY() + move.getY()));
	}

	/**
	 * Creates an AABB2D that bounds both this AABB2D and another AABB2D.
	 *
	 * @param other The other AABB2D being bounded.
	 *
	 * @return An AABB2D that bounds both this AABB2D and {@code other}.
	 */
	public AABB2D combine(AABB2D other) {
		float minX = Math.min(this.minPosition.getX(), other.getMinPosition().getX());
		float minY = Math.min(this.minPosition.getY(), other.getMinPosition().getY());
		float maxX = Math.max(this.maxPosition.getX(), other.getMaxPosition().getX());
		float maxY = Math.max(this.maxPosition.getY(), other.getMaxPosition().getY());
		return new AABB2D(new Vector2f(minX, minY), new Vector2f(maxX, maxY));
	}

	/**
	 * Creates a new AABB2D equivalent to this, but stretched by a certain amount.
	 *
	 * @param stretch The amount to stretch.
	 *
	 * @return A new AABB2D, stretched by the specified amounts.
	 */
	public AABB2D stretch(Vector2f stretch) {
		float minX, maxX, minY, maxY, minZ, maxZ;

		if (stretch.getX() < 0) {
			minX = this.minPosition.getX() + stretch.getX();
			maxX = this.maxPosition.getX();
		} else {
			minX = this.minPosition.getX();
			maxX = this.maxPosition.getX() + stretch.getX();
		}

		if (stretch.getY() < 0) {
			minY = this.minPosition.getY() + stretch.getY();
			maxY = this.maxPosition.getY();
		} else {
			minY = this.minPosition.getY();
			maxY = this.maxPosition.getY() + stretch.getY();
		}

		return new AABB2D(new Vector2f(minX, minY), new Vector2f(maxX, maxY));
	}
}
