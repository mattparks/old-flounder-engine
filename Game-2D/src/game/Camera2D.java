package game;

import engine.basics.ICamera;
import engine.space.Frustum;
import engine.toolbox.vector.Matrix4f;
import engine.toolbox.vector.Vector3f;

public class Camera2D implements ICamera {
	private Matrix4f viewMatrix;
	private Vector3f position;

	public Camera2D() {
		viewMatrix = new Matrix4f();
		position = new Vector3f(0, 0, 0);
	}

	@Override
	public float getNearPlane() {
		return -1;
	}

	@Override
	public float getFarPlane() {
		return 1;
	}

	@Override
	public float getFOV() {
		return 90;
	}

	@Override
	public void moveCamera(Vector3f focusPosition, Vector3f focusRotation, boolean gamePaused) {
		position = focusPosition;
		createViewMatrix(position);
	}

	private void createViewMatrix(Vector3f position) {
		viewMatrix.setIdentity();
		Matrix4f.translate(viewMatrix, position, viewMatrix);
	}

	@Override
	public Matrix4f getViewMatrix() {
		return viewMatrix;
	}

	@Override
	public Frustum getViewFrustum() {
		return null;
	}

	@Override
	public Matrix4f getReflectionViewMatrix(float planeHeight) {
		return null;
	}

	@Override
	public void reflect(float waterHeight) {
	}

	@Override
	public Vector3f getPosition() {
		return position;
	}

	@Override
	public float getPitch() {
		return 0;
	}

	@Override
	public float getYaw() {
		return 0;
	}

	@Override
	public float getAimDistance() {
		return 0;
	}
}
