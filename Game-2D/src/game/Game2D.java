package game;

import engine.basics.EngineCore;
import engine.basics.IGame;
import engine.basics.IModule;
import engine.basics.IRendererMaster;
import engine.basics.MasterRenderer2D;
import engine.basics.options.OptionsGraphics;
import engine.devices.DeviceDisplay;
import engine.devices.DeviceKeyboard;
import engine.entities.Entity;
import engine.entities.components.ColliderComponent;
import engine.entities.components.SpriteComponent;
import engine.inputs.ButtonAxis;
import engine.inputs.CompoundAxis;
import engine.inputs.ControllerKey;
import engine.inputs.IAxis;
import engine.inputs.IButton;
import engine.inputs.JoystickAxis;
import engine.inputs.KeyButton;
import engine.textures.Texture;
import engine.toolbox.OpenglUtils;
import engine.toolbox.resources.MyFile;
import engine.toolbox.vector.Vector2f;
import engine.toolbox.vector.Vector3f;
import engine.world.Environment;

public class Game2D extends IGame {
	private static Camera2D camera;
	private static IRendererMaster rendererMaster;
	private static IModule module;
	private TempMove tempMove;

	public Game2D() {
		super(null, module);
	}

	public static void main(String[] args) {
		camera = new Camera2D();
		rendererMaster = new MasterRenderer2D();
		module = new IModule(rendererMaster, camera);
		new Game2D(); // Creates the game!
	}

	@Override
	public void init() {
		GameKeyManager.init();
		GameGuis.init();

		/* GAME OBJECTS. */
		tempMove = new TempMove();

		Environment.init();

		/* GENERATE TESTS */
		final int mapSizeX = 128;
		final int mapSizeY = 128;

		for (int x = 0; x < mapSizeX; x++) {
			for (int y = 0; y < mapSizeY; y++) {
				Entity e1 = new Entity(Environment.setGenericEntityTree(), new Vector2f(x, y));
				new ColliderComponent(e1);
				new SpriteComponent(e1, Texture.newTexture(new MyFile(MyFile.RES_FOLDER, "e1.png")).create());
			}
		}
	}

	@Override
	public void update() {
		Environment.update();

		if (!GameGuis.isMenuOpen()) {
			tempMove.update();
		}

		GameKeyManager.update();
		GameGuis.update();

		super.updateGame(tempMove.position, tempMove.rotation, GameGuis.isMenuOpen(), GameGuis.getBlurFactor());
	}

	@Override
	public void cleanUp() {
	}

	private static class TempMove {
		public Vector3f position = new Vector3f();
		public Vector3f rotation = new Vector3f();
		private IAxis inputMoveX;
		private IAxis inputMoveY;

		public TempMove() {
			IButton leftKeyButtons = new KeyButton(new int[]{DeviceKeyboard.KEY_A, DeviceKeyboard.KEY_LEFT});
			IButton rightKeyButtons = new KeyButton(new int[]{DeviceKeyboard.KEY_D, DeviceKeyboard.KEY_RIGHT});
			IButton upKeyButtons = new KeyButton(new int[]{DeviceKeyboard.KEY_W, DeviceKeyboard.KEY_UP});
			IButton downKeyButtons = new KeyButton(new int[]{DeviceKeyboard.KEY_S, DeviceKeyboard.KEY_DOWN});

			inputMoveX = new CompoundAxis(new ButtonAxis(leftKeyButtons, rightKeyButtons), new JoystickAxis(0, 3));
			inputMoveY = new CompoundAxis(new ButtonAxis(upKeyButtons, downKeyButtons), new JoystickAxis(0, 1));
		}

		public void update() {
			position.set((float) (position.getX() + (inputMoveX.getAmount() * EngineCore.getDeltaSeconds())), (float) (position.getY() + (-inputMoveY.getAmount() * EngineCore.getDeltaSeconds())), 0.0f);
		}
	}

	private static class GameKeyManager {
		private static ControllerKey screenshotKey;
		private static ControllerKey fullscreenKey;
		private static ControllerKey polygonKey;

		private static boolean polygonMode;

		public static void init() {
			screenshotKey = new ControllerKey(new KeyButton(DeviceKeyboard.KEY_F2));
			fullscreenKey = new ControllerKey(new KeyButton(DeviceKeyboard.KEY_F11));
			polygonKey = new ControllerKey(new KeyButton(DeviceKeyboard.KEY_P));
			polygonMode = false;
		}

		public static void update() {
			if (screenshotKey.wasPressed()) {
				DeviceDisplay.screenshot();
			}

			if (fullscreenKey.wasPressed()) {
				OptionsGraphics.FULLSCREEN = !OptionsGraphics.FULLSCREEN;
			}

			if (polygonKey.wasPressed()) {
				polygonMode = !polygonMode;

				if (polygonMode) {
					OpenglUtils.goWireframe(true);
				} else {
					OpenglUtils.goWireframe(false);
				}
			}
		}
	}
}
