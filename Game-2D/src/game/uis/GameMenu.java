package game.uis;

import engine.basics.EngineCore;
import engine.fonts.Text;
import engine.guis.GuiComponent;
import engine.guis.GuiTexture;
import engine.toolbox.Colour;
import engine.visual.fxDrivers.ConstantDriver;
import engine.visual.fxDrivers.SlideDriver;
import engine.visual.fxDrivers.ValueDriver;

import java.util.List;

public class GameMenu extends GuiComponent {
	public static final Colour TEXT_COLOUR = new Colour(0.85f, 0.85f, 0.85f);
	private static final float TITLE_FONT_SIZE = 4;
	private static final float MAIN_MENU_Y_POS = 0.25f;
	private static final float MAIN_MENU_Y_SIZE = 0.6f;

	private static final float SLIDE_TIME = 0.7f;

	private ValueDriver secondaryDriver = new ConstantDriver(0);
	private ValueDriver mainDriver = new ConstantDriver(0);

	private boolean displayed = true;
	private boolean closeSecondary = false;

	private MainMenu mainMenu;
	private GuiComponent secondaryScreen;

	public GameMenu(GameMenuBackground superMenu) {
		mainMenu = new MainMenu(superMenu, this);
		Text text = Text.newText("DarkNote Alpha").center().setFontSize(TITLE_FONT_SIZE).create();
		text.setColour(TEXT_COLOUR);
		super.addText(text, 0, 0, 1);
		addComponent(mainMenu, 0, MAIN_MENU_Y_POS, 1, MAIN_MENU_Y_SIZE);
	}

	protected void display(boolean display) {
		if (display) {
			show(true);
			mainDriver = new SlideDriver(getRelativeX(), 0, SLIDE_TIME); // SinWaveDriver for cool effect :p
			displayed = true;
		} else {
			mainDriver = new SlideDriver(getRelativeX(), 1, SLIDE_TIME);
			displayed = false;
		}
	}

	@Override
	public void show(boolean visible) {
		super.show(visible);
		if (!visible) {
			removeSecondaryScreen();
			secondaryDriver = new ConstantDriver(0);
		}
	}

	@Override
	protected void updateSelf() {
		float mainValue = mainDriver.update(EngineCore.getDeltaSeconds());
		float value = secondaryDriver.update(EngineCore.getDeltaSeconds());
		mainMenu.setRelativeX(value);

		if (secondaryScreen != null) {
			secondaryScreen.setRelativeX(value - 1);
		}

		super.setRelativeX(mainValue);

		if (!displayed) {
			if (mainValue >= 1) {
				show(false);
			}
		}

		if (closeSecondary) {
			if (secondaryScreen.getRelativeX() <= -1) {
				removeSecondaryScreen();
				closeSecondary = false;
			}
		}
	}

	@Override
	protected void getGuiTextures(List<GuiTexture> guiTextures) {
	}

	private void removeSecondaryScreen() {
		if (secondaryScreen != null) {
			removeComponent(secondaryScreen);
			secondaryScreen = null;
		}
	}

	protected boolean isDisplayed() {
		return displayed;
	}

	protected void setNewSecondaryScreen(GuiComponent secondScreen) {
		removeSecondaryScreen();
		secondaryScreen = secondScreen;
		addComponent(secondScreen, mainMenu.getRelativeX() - 1, MAIN_MENU_Y_POS, 1, MAIN_MENU_Y_SIZE);
		secondaryDriver = new SlideDriver(mainMenu.getRelativeX(), 1, SLIDE_TIME);
	}

	protected void closeSecondaryScreen() {
		secondaryDriver = new SlideDriver(mainMenu.getRelativeX(), 0, SLIDE_TIME);
		closeSecondary = true;
	}
}
