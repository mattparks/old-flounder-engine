package game.uis;

import engine.devices.DeviceSound;
import engine.fonts.Text;
import engine.guis.GuiComponent;
import engine.guis.GuiTextButton;
import engine.guis.GuiTexture;
import engine.guis.Listener;
import engine.sounds.Sound;
import engine.toolbox.resources.MyFile;

import java.util.List;

public class OptionScreen extends GuiComponent {
	public static final float FONT_SIZE = 2;
	public static final float BUTTONS_Y_SIZE = 0.2f;
	public static final float BUTTONS_X_LEFT_POS = 0.049999997f;
	public static final float BUTTONS_X_CENTER_POS = 0.3f;
	public static final float BUTTONS_X_RIGHT_POS = 0.55f;
	public static final float BUTTONS_X_WIDTH = 0.4f;

	public static final Sound VALUE_UP_SOUND = Sound.loadSoundNow(new MyFile(DeviceSound.SOUND_FOLDER, "button3.wav"), 0.8f);
	public static final Sound VALUE_DOWN_SOUND = Sound.loadSoundNow(new MyFile(DeviceSound.SOUND_FOLDER, "button3.wav"), 0.8f);
	public static final Sound VALUE_INVALID_SOUND = Sound.loadSoundNow(new MyFile(DeviceSound.SOUND_FOLDER, "button3.wav"), 0.8f);

	private GameMenu gameMenu;

	protected OptionScreen(GameMenu menu) {
		gameMenu = menu;

		createGraphicsOption(BUTTONS_X_LEFT_POS, 0.0f);

		createSoundsOption(BUTTONS_X_RIGHT_POS, 0.0f);

		createBackOption(BUTTONS_X_CENTER_POS, 1.0f);
	}

	private void createGraphicsOption(float xPos, float yPos) {
		final String graphicsText = "Graphics Options";
		final Text text = Text.newText(graphicsText).center().setFontSize(FONT_SIZE).create();
		text.setColour(GameMenu.TEXT_COLOUR);
		GuiTextButton button = new GuiTextButton(text);

		Listener listener = () -> gameMenu.setNewSecondaryScreen(new OptionScreenGraphics(gameMenu));

		button.addLeftListener(listener);
		addComponent(button, xPos, yPos, BUTTONS_X_WIDTH, BUTTONS_Y_SIZE);
	}

	private void createSoundsOption(float xPos, float yPos) {
		final String soundsText = "Sounds Options";
		final Text text = Text.newText(soundsText).center().setFontSize(FONT_SIZE).create();
		text.setColour(GameMenu.TEXT_COLOUR);
		GuiTextButton button = new GuiTextButton(text);

		Listener listener = () -> gameMenu.setNewSecondaryScreen(new OptionScreenSounds(gameMenu));

		button.addLeftListener(listener);
		addComponent(button, xPos, yPos, BUTTONS_X_WIDTH, BUTTONS_Y_SIZE);
	}

	private void createBackOption(float xPos, float yPos) {
		final Text text = Text.newText("Back").center().setFontSize(FONT_SIZE).create();
		text.setColour(GameMenu.TEXT_COLOUR);
		GuiTextButton button = new GuiTextButton(text);

		Listener listener = () -> gameMenu.closeSecondaryScreen();

		button.addLeftListener(listener);
		addComponent(button, xPos, yPos, BUTTONS_X_WIDTH, BUTTONS_Y_SIZE);
	}

	@Override
	protected void updateSelf() {
	}

	@Override
	protected void getGuiTextures(List<GuiTexture> guiTextures) {
	}
}
