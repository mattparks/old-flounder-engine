package game.uis;

import engine.basics.EngineCore;
import engine.guis.GuiComponent;
import engine.guis.GuiTexture;
import engine.visual.fxDrivers.ConstantDriver;
import engine.visual.fxDrivers.SlideDriver;
import engine.visual.fxDrivers.ValueDriver;

import java.util.List;

public class GameMenuBackground extends GuiComponent {
	private static final float SLIDE_TIME = 0.7f;
	private ValueDriver slideDriver;
	private float backgroundAlpha;
	private GameMenu menu;
	private boolean displayed = true;

	public GameMenuBackground() {
		menu = new GameMenu(this);
		slideDriver = new ConstantDriver(1);
		super.addComponent(menu, 0, 0, 1, 1);
	}

	public void display(boolean display) {
		menu.display(display);
		displayed = display;

		if (display) {
			if (!isShown()) {
				show(true);
			}

			slideDriver = new SlideDriver(backgroundAlpha, 1, SLIDE_TIME);
		} else {
			slideDriver = new SlideDriver(backgroundAlpha, 0, SLIDE_TIME);
		}
	}

	public boolean isDisplayed() {
		return displayed;
	}

	public float getBlurFactor() {
		return backgroundAlpha;
	}

	@Override
	protected void setScreenSpacePosition(float x, float y, float width, float height) {
		super.setScreenSpacePosition(x, y, width, height);
	}

	@Override
	protected void updateSelf() {
		backgroundAlpha = slideDriver.update(EngineCore.getDeltaSeconds());

		if (!displayed && !menu.isShown()) {
			show(false);
		}
	}

	@Override
	protected void getGuiTextures(List<GuiTexture> guiTextures) {
	}
}
