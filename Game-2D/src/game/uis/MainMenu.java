package game.uis;

import engine.devices.DeviceDisplay;
import engine.fonts.Text;
import engine.guis.GuiComponent;
import engine.guis.GuiTextButton;
import engine.guis.GuiTexture;
import engine.guis.Listener;

import java.util.List;

public class MainMenu extends GuiComponent {
	private static final float FONT_SIZE = 2;
	private static final float BUTTONS_X_POS = 0.3f;
	private static final float BUTTONS_Y_SIZE = 0.2f;
	private static final float BUTTONS_X_WIDTH = 1f - BUTTONS_X_POS * 2f;

	private GameMenu gameMenu;
	private GameMenuBackground superMenu;

	protected MainMenu(GameMenuBackground superMenu, GameMenu menu) {
		gameMenu = menu;
		this.superMenu = superMenu;
		createPlayButton(0.1f);
		createOptionsButton(0.3f);
		createQuitButton(0.5f);
	}

	private void createPlayButton(float yPos) {
		Listener listener = () -> superMenu.display(false);
		createButton("Play", listener, yPos);
	}

	private void createOptionsButton(float yPos) {
		Listener listener = () -> gameMenu.setNewSecondaryScreen(new OptionScreen(gameMenu));
		createButton("Options", listener, yPos);
	}

	private void createQuitButton(float yPos) {
		Listener listener = DeviceDisplay::requestClosure;
		createButton("Quit", listener, yPos);
	}

	private void createButton(String textString, Listener listener, float yPos) {
		Text text = Text.newText(textString).center().setFontSize(FONT_SIZE).create();
		text.setColour(GameMenu.TEXT_COLOUR);
		GuiTextButton button = new GuiTextButton(text);
		button.addLeftListener(listener);
		addComponent(button, BUTTONS_X_POS, yPos, BUTTONS_X_WIDTH, BUTTONS_Y_SIZE);
	}

	@Override
	protected void updateSelf() {
	}

	@Override
	protected void getGuiTextures(List<GuiTexture> guiTextures) {
	}
}
