package game;

import engine.devices.DeviceKeyboard;
import engine.guis.GuiManager;
import engine.inputs.ControllerKey;
import engine.inputs.KeyButton;
import game.uis.GameMenuBackground;

/**
 * Class in charge of the main GUIs in the test game.
 */
public class GameGuis {
	private static GameMenuBackground gameMenu;
	private static ControllerKey openKey;
	private static boolean menuOpen;

	/**
	 * Carries out any necessary initialization of Guis.
	 */
	public static void init() {
		gameMenu = new GameMenuBackground();
		openKey = new ControllerKey(new KeyButton(DeviceKeyboard.KEY_ESCAPE));
		menuOpen = false;
		GuiManager.addComponent(gameMenu, 0, 0, 1, 1);
	}

	/**
	 * Checks inputs and updates Guis.
	 */
	public static void update() {
		if (openKey.wasPressed()) {
			gameMenu.display(!gameMenu.isDisplayed());
		}

		menuOpen = gameMenu.isShown();
	}

	public static float getBlurFactor() {
		return gameMenu.getBlurFactor();
	}

	public static boolean isMenuOpen() {
		return menuOpen;
	}
}
