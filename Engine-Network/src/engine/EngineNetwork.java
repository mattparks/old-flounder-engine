package engine;

import engine.client.Client;
import engine.network.Packet00Login;
import engine.network.Packet01Disconnect;
import engine.server.Server;

public class EngineNetwork {
	private Server socketServer;
	private Client socketClient;
	private String username = "USERNAME" + ((int) (Math.random() * 10000));

	public void startServer() {
		socketServer = new Server();
		socketServer.start();
	}

	public void startClient() {
		socketClient = new Client("localhost");
		socketClient.start();

		Packet00Login loginPacket = new Packet00Login(username);
		loginPacket.writeData(socketClient);
	}

	public void closeServer() {
		if (socketServer != null) {
			// new Packet01Disconnect("server").writeData(socketServer);
			socketServer.cleanUp();
			socketServer = null;
		}
	}

	public void closeClient() {
		if (socketClient != null) {
			new Packet01Disconnect(username).writeData(socketClient);
			socketClient.cleanUp();
			socketClient = null;
		}
	}

	public Server getSocketServer() {
		return socketServer;
	}

	public Client getSocketClient() {
		return socketClient;
	}

	public void cleanUp() {
		closeClient();
		closeServer();
	}
}
