package engine.client;

import engine.devices.DeviceDisplay;
import engine.network.Packet;
import engine.network.Packet00Login;
import engine.network.Packet01Disconnect;
import engine.toolbox.Logger;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class Client extends Thread {
	private InetAddress ipAddress;
	private DatagramSocket socket;

	public Client(String ipAddress) {
		try {
			this.socket = new DatagramSocket();
			this.ipAddress = InetAddress.getByName(ipAddress);
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		while (DeviceDisplay.isOpen() && socket != null) {
			byte[] data = new byte[1024];
			DatagramPacket packet = new DatagramPacket(data, data.length);

			try {
				socket.receive(packet);
			} catch (IOException e) {
				Logger.error("Client socket could not receive data!");
				e.printStackTrace();
			}

			parsePacket(packet.getData(), packet.getAddress(), packet.getPort());
		}
	}

	private void parsePacket(byte[] data, InetAddress address, int port) {
		String message = new String(data).trim();
		Packet.PacketType type = Packet.lookupPacket(message.substring(0, 2));

		Packet packet;

		switch (type) {
			default:
			case INVALID:
				packet = null;
				break;
			case LOGIN:
				packet = new Packet00Login(data);
				handleLogin((Packet00Login) packet, address, port);
				break;
			case DISCONNECT:
				packet = new Packet01Disconnect(data);
				handleDisconnect((Packet01Disconnect) packet, address, port);
				break;
		}
	}

	private void handleLogin(Packet00Login packet, InetAddress address, int port) {
		Logger.log("[" + address.getHostAddress() + ":" + port + "] " + (packet).getUsername() + " has joined the game.");
		// ((PlatformScene) DarkNote.getEngine().getCurrentScene()).getLevel().createMultiplayer(packet.getUsername(), ((PlatformScene) DarkNote.getEngine().getCurrentScene()).getLevel().getThisPlayer().getPosition().getX(), ((PlatformScene) DarkNote.getEngine().getCurrentScene()).getLevel().getThisPlayer().getPosition().getY() - 1);
	}

	private void handleDisconnect(Packet01Disconnect packet, InetAddress address, int port) {
		Logger.log("[" + address.getHostAddress() + ":" + port + "] " + packet.getUsername() + " has quit the game.");
		// ((PlatformScene) DarkNote.getEngine().getCurrentScene()).getLevel().removeMultiplayer(packet.getUsername());
	}

	public void sendData(byte[] data) {
		DatagramPacket packet = new DatagramPacket(data, data.length, ipAddress, 1331);

		try {
			socket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void cleanUp() {
		socket.close();
	}
}
