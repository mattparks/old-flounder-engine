package engine.server;

import engine.devices.DeviceDisplay;
import engine.network.Packet;
import engine.network.Packet00Login;
import engine.network.Packet01Disconnect;
import engine.toolbox.Logger;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

public class Server extends Thread {
	private DatagramSocket socket;
	private List<ClientData> connected;

	public Server() {
		try {
			this.socket = new DatagramSocket(1331);
			this.connected = new ArrayList<>();
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		while (DeviceDisplay.isOpen() && socket != null) {
			byte[] data = new byte[1024];
			DatagramPacket packet = new DatagramPacket(data, data.length);

			try {
				socket.receive(packet);
			} catch (IOException e) {
				Logger.error("Server socket could not receive data!");
				e.printStackTrace();
			}

			parsePacket(packet.getData(), packet.getAddress(), packet.getPort());
		}
	}

	private void parsePacket(byte[] data, InetAddress address, int port) {
		String message = new String(data).trim();
		Packet.PacketType type = Packet.lookupPacket(message.substring(0, 2));

		Packet packet;

		switch (type) {
			default:
			case INVALID:
				packet = null;
				break;
			case LOGIN:
				packet = new Packet00Login(data);
				// if (address.getHostAddress().equalsIgnoreCase("127.0.0.1")) // Server = Client
				System.out.println("[" + address.getHostAddress() + ":" + port + "] " + ((Packet00Login) (packet)).getUsername() + " has connected.");
				ClientData player = new ClientData(((Packet00Login) packet).getUsername(), address, port);
				addConnection(player, (Packet00Login) (packet));
				break;
			case DISCONNECT:
				packet = new Packet01Disconnect(data);
				System.out.println("[" + address.getHostAddress() + ":" + port + "] " + ((Packet01Disconnect) (packet)).getUsername() + " has disconnected.");
				removeConnection((Packet01Disconnect) (packet));
				break;
		}
	}

	private void addConnection(ClientData player, Packet00Login packet) {
		boolean alredyConnected = false;

		for (ClientData p : connected) {
			if (player.getUsername().equalsIgnoreCase(p.getUsername())) {
				if (p.ipAddress == null) {
					p.ipAddress = player.ipAddress;
				}

				if (p.port == -1) {
					p.port = player.port;
				}

				alredyConnected = true;
			} else {
				// Relay to the current connected player that there is a new player
				sendData(packet.getData(), p.ipAddress, p.port);

				// Relay to the new player that the currently connect player exists
				packet = new Packet00Login(p.getUsername());
				sendData(packet.getData(), player.ipAddress, player.port);
			}
		}

		if (!alredyConnected) {
			this.connected.add(player);
		}
	}

	private void removeConnection(Packet01Disconnect packet) {
		this.connected.remove(getPlayerMPIndex(packet.getUsername()));
		packet.writeData(this);
	}

	public ClientData getPlayerMP(String username) {
		for (ClientData player : connected) {
			if (player.getUsername().equals(username)) {
				return player;
			}
		}

		return null;
	}

	public int getPlayerMPIndex(String username) {
		int index = 0;

		for (ClientData player : connected) {
			if (player.getUsername().equals(username)) {
				break;
			}

			index++;
		}

		return index;
	}

	public void sendData(byte[] data, InetAddress ipAddress, int port) {
		DatagramPacket packet = new DatagramPacket(data, data.length, ipAddress, port);

		try {
			socket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendDataToAllClients(byte[] data) {
		for (ClientData p : connected) {
			sendData(data, p.ipAddress, p.port);
		}
	}

	public void cleanUp() {
		socket.close();
	}
}
